unit CustomersDocumentExcelAndPdf;

interface

uses
  UCustomFlexCelReport,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass, frxExportPDF, Data.DB,
  Data.Win.ADODB, frxDBSet, UFlexCelReport, UExcelAdapter,
  XLSAdapter;

type
  TCustomerDocumentsExcelAndPdf = class(TForm)

    ADOConnection_imp: TADOConnection;
    frxReport1: TfrxReport;
    frxDBDataset1: TfrxDBDataset;
    frxDBDataset2: TfrxDBDataset;
    DataSource1: TDataSource;
    ADODataSet1: TADODataSet;
    frxPDFExport1: TfrxPDFExport;
    XLSAdapter1: TXLSAdapter;
    FlexCelReport_Docs: TFlexCelReport;
    adodts_consumo: TADODataSet;
    FlexCelReport_consumo: TFlexCelReport;
    ado_pedrep: TADODataSet;
    flexcelreport_pedidos: TFlexCelReport;
    ADOdts_repres: TADODataSet;

    procedure BuildInvoices();
    procedure BuildDeliveryNote();
    procedure BuildCustomerConsumption(cliente_id:string);
    procedure BuildSalesManOrder(repre:string);
    procedure ManagerSalesManOrder();
    procedure OutstandingPayments();
    procedure SetParams();
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    Procedure Genera_tabla_documentos(cliente, pathToSave:string;tipo_doc:integer);
    procedure Generar_tabla_consumo_html(cliente_id, fichero_excel, fecha_consulta, ruta_fichero_local:string);
    procedure Generar_Tabla_html_local(repre:string);
    Procedure gestiona_fichero_tabla_documentos(xcliente, pathToSave:string; tipo_doc:integer; html_tabla:widestring);
    procedure tratar_clientes_con_relacion_efectos();
    function marcar_efecto_borrado(cliente, documento: string; fecha_vto: string): boolean;
    procedure Genera_pagos_ptes();
   //rocedure crear_directorios(cliente_id:String);
    function crea_relacion_efectos_cedidos(cliente: string): boolean;


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CustomerDocumentsExcelAndPdf: TCustomerDocumentsExcelAndPdf;

type
  Select = (sFacturas, sAlbaranes, sPedidos, s_existe_doc_ftp,
  s_efectos_cab, s_existe_doc_ppte_ftp, web_ftp_Efectos_sBorrar, s_efectos_modificar, Web_s_rel_efectos_actualizar);

type
  insert = (i_existe_doc_ftp, i_doc_ppte_ftp);

type
  update = (upd_doc_ftp, upd_efecto_borrar);

type
  arr_slt = array [Select] of widestring;

type
  arr_ins = array [insert] of widestring;

type
  arr_upd = array [update] of widestring;

implementation

{$R *.dfm}

uses base, TDocPdf_ftp, TDocExcel_ftp, bdalfa, encripter, general, Hashes, Timp_Pago_ptes_ftp;

var
    startDocumentId,
    endDoumentId,
    startCustomerId,
    endCustomerId,
    startDate,
    endDate,
    reprint,
    startCycle,
    endCycle,
    counted,
    typeDocument,
    pathToSave: string;

    array_select: arr_slt;
    array_insert: arr_ins;
    array_update: arr_upd;

    dir_alb_ftp, dir_fac_ftp, dir_consumo_ftp, dir_consumo_local, ruta_ppal_ftp, dir_rptPedidosCli_local,
    dir_rptPedidosCli_ftp, fichero_plantilla, fichero_rptPedidos_local: ansiString;

procedure carga_consultas_array;
begin
  array_select[sFacturas] := retorna_consulta_ini(ejecutable_ini, 'sql_select', 's_Facturas');
  array_select[sAlbaranes] := retorna_consulta_ini(ejecutable_ini, 'sql_select', 's_Albaranes');
  array_select[sPedidos] := retorna_consulta_ini(ejecutable_ini, 'sql_select', 's_Pedidos');

  array_select[s_existe_doc_ftp] := retorna_consulta_ini(ejecutable_ini,'sql_select', 's_existe_doc_ftp');
  array_insert[i_existe_doc_ftp] := retorna_consulta_ini(ejecutable_ini,'sql_insert', 'i_existe_doc_ftp');
  array_update[upd_doc_ftp] := retorna_consulta_ini(ejecutable_ini,'sql_update', 'upd_doc_ftp');

  array_select[s_efectos_cab] := retorna_consulta_ini(ejecutable_ini,'sql_select', 's_efectos_cab');
  array_select[Web_s_rel_efectos_actualizar] := retorna_consulta_ini(ejecutable_ini, 'sql_select', 's_efectos_actualizar');
  array_insert[i_doc_ppte_ftp] := retorna_consulta_ini(ejecutable_ini, 'sql_insert', 'i_pago_pte_ftp');
  array_update[upd_efecto_borrar] := retorna_consulta_ini(ejecutable_ini, 'sql_update', 'upd_efecto_borrar');

end;


{*
/td: Tipo de documento 21-Pedido 22_Albaran 23-Factura
/di: Documento Inicial
/df: Documento final
/ci: Cliente Inicial
/cf: Cliente Final
/fi: Fecha Inicial
/ff: Fecha Final
/re: Reimpresi�n    0-Sin reimpresi�n   1-Reimpresi�n    2-Todos
/ic: Ciclo inicio
/fc: Ciclo fin
/ct: Contabilizado  0-No contabilizado  1-Contabilizado  2-Todos
/ps: Path to save documents
*}
procedure TCustomerDocumentsExcelAndPdf.SetParams();
var
  idx: integer;
  str_parameter_type: string;
  str_parameter: string;
  size_parameter: integer;
begin

  for idx := 0 to ParamCount do
  begin
    size_parameter := Length(ParamStr(idx));
    str_parameter := copy(ParamStr(idx), 5, size_parameter - 3);
    str_parameter := trim(str_parameter);
    str_parameter_type := copy(ParamStr(idx), 0, 4);

    if str_parameter_type = '/di:' then

      if str_parameter <> '' then startDocumentId := str_parameter else startDocumentId := ''

    else if str_parameter_type = '/df:' then

      if str_parameter <> '' then endDoumentId := str_parameter else endDoumentId := ''

    else if str_parameter_type = '/ci:' then

      if str_parameter <> '' then startCustomerId := str_parameter else startCustomerId := ''

    else if str_parameter_type = '/cf:' then

      if str_parameter <> '' then endCustomerId := str_parameter else endCustomerId := ''

    else if str_parameter_type = '/fi:' then

      if str_parameter <> '' then startDate := str_parameter else startDate := ''

    else if str_parameter_type = '/ff:' then

      if str_parameter <> '' then endDate := str_parameter else endDate := ''

    else  if str_parameter_type = '/td:' then

      if str_parameter <> '' then typeDocument := str_parameter else typeDocument := ''

    else  if str_parameter_type = '/re:' then

      if str_parameter <> '' then reprint := str_parameter else reprint := ''

    else  if str_parameter_type = '/ct:' then

      if str_parameter <> '' then counted := str_parameter else counted := ''

    else  if str_parameter_type = '/ic:' then

      if str_parameter <> '' then startCycle := str_parameter else startCycle := ''

    else  if str_parameter_type = '/fc:' then

      if str_parameter <> '' then endCycle := str_parameter else endCycle := ''

    else  if str_parameter_type = '/ps:' then

      if str_parameter <> '' then pathToSave := str_parameter else pathToSave := ''

    else  if str_parameter_type = '/td:' then

      if str_parameter <> '' then typeDocument := str_parameter else typeDocument := ''

  end;

end;



function guarda_doc_bd(cli, doc, fecha, borrado, tipo_doc, ximporte: String; conexion: TADOConnection; formato:string): boolean;
var
  adocommand1: Tadocommand;
  ADODataSet1: TADODataSet;
begin
  ADODataSet1 := TADODataSet.Create(nil);
  adocommand1 := Tadocommand.Create(nil);
  ADODataSet1.Connection := conexion;
  adocommand1.Connection := conexion;

  ADODataSet1.CommandText := '';
  ADODataSet1.ParamCheck := true;
  ADODataSet1.CommandText := array_select[s_existe_doc_ftp];
  ADODataSet1.Parameters[0].Value := doc;
  ADODataSet1.Parameters[1].Value := cli;
  adodataset1.parameters[2].value := tipo_doc;
  adodataset1.parameters[3].value := formato;
  ADODataSet1.Active := true;

  adocommand1.CommandText := '';
  adocommand1.ParamCheck := true;

  if ADODataSet1.RecordCount > 0 then
  begin
    adocommand1.CommandText := array_update[upd_doc_ftp];
    with adocommand1 do
     begin
      Parameters[0].Value := fecha;
      Parameters[1].Value := borrado;
      parameters[2].Value := ximporte;
      Parameters[3].Value := cli;
      Parameters[4].Value := doc;
      Parameters[5].Value := tipo_doc;
      parameters[6].Value := formato;

     end;
  end
  else
  begin
    adocommand1.CommandText := array_insert[i_existe_doc_ftp];
    with adocommand1 do
     begin
      parameters[0].Value := formato;
      Parameters[1].Value := fecha;
      Parameters[2].Value := borrado;
      Parameters[3].Value := cli;
      Parameters[4].Value := doc;
      Parameters[5].Value := tipo_doc;
      parameters[6].Value := ximporte;
     end;
  end;

  Try
    adocommand1.Execute;
    guarda_doc_bd := true;
  except
    on E: Exception do
    begin
      guarda_doc_bd := false;
      //WriteLn(myFile, '-- ERROR: Guarda_Doc_BD_FTP ');
      //raise Exception.Create('Error al insertar documentos en la BD !!!!!');
    end;
  end;
end;



Procedure obtener_cabecera_TablaDoc(var HTML_str:widestring; tipo_doc:integer);
var   html_cab:ansistring;
begin
   case tipo_doc of
      2, 22: // albaran
      begin
         lee_dato_ini(ejecutable_ini,'ficheros_html','TablaDocs_cab_alb_fact',html_cab);
         HTML_str:=carga_ficherowide(html_cab, false);
         HTML_str:=StringReplace(HTML_str, '@xtipo_doc_descripcion','Albaranes',[rfReplaceAll]);
      end;
      3, 23: // factura
      begin
         lee_dato_ini(ejecutable_ini,'ficheros_html','TablaDocs_cab_alb_fact',html_cab);
         HTML_str:=carga_ficherowide(html_cab, false);
         HTML_str:=StringReplace(HTML_str, '@xtipo_doc_descripcion','Facturas',[rfReplaceAll]);
      end;
   end;
end;



procedure Obtener_ruta_fichero_pdf_TablaDocs_ftp(xcliente, xnumdoc_id, xfecha_doc:string; var ruta_fichero_pdf, ruta_fichero_excel:ansistring ; tipo_doc:integer);
var webalfa:ansistring;
begin

  xfecha_doc:= stringreplace(xfecha_doc, '/','',[rfReplaceAll]);
  xfecha_doc:= stringreplace(xfecha_doc, '-','',[rfReplaceAll]);
  lee_dato_ini(ejecutable_ini,'Rutas','web_alfa', webalfa);

  case tipo_doc of
      2, 22: // albaranes
        begin
          ruta_fichero_pdf:= webalfa + xcliente + dir_alb_ftp + '/Alb_' + xcliente + '_' + xnumdoc_id +'_'+ xfecha_doc+ '.pdf';
          ruta_fichero_excel:= webalfa + xcliente + dir_alb_ftp + '/Alb_' + xcliente + '_' + xnumdoc_id +'_'+ xfecha_doc+ '.xls';
        end;
      3, 23: // facturas
        begin
           ruta_fichero_pdf:= webalfa + xcliente + dir_fac_ftp + '/Fac_' + xcliente + '_' + xnumdoc_id +'_'+ xfecha_doc+ '.pdf';
           ruta_fichero_excel:= webalfa + xcliente + dir_fac_ftp + '/Fac_' + xcliente + '_' + xnumdoc_id +'_'+ xfecha_doc+ '.xls';
        end;
  end;

end;


Procedure TCustomerDocumentsExcelAndPdf.gestiona_fichero_tabla_documentos(xcliente, pathToSave:string; tipo_doc:integer; html_tabla:widestring);
var fichero_local_tablaDocs, fichero_ftp_tablaDocs, ruta_ftp_cli: ansiString;
tabla_pedidos_local, tabla_albaranes_local, tabla_facturas_local:ansistring;
 htmlfile:textfile;
begin
  lee_dato_ini(ejecutable_ini,'ficheros_html','tabla_albaranes_local',tabla_albaranes_local);
  lee_dato_ini(ejecutable_ini,'ficheros_html','tabla_facturas_local',tabla_facturas_local);

  case tipo_doc of
    2, 22: // albaranes
      begin
        fichero_local_tablaDocs := pathToSave + xcliente + tabla_albaranes_local;
      end;
    3, 23: // facturas
      begin
        fichero_local_tablaDocs := pathToSave + xcliente + tabla_facturas_local;
      end;
  end;

  try
    AssignFile(htmlfile, fichero_local_tablaDocs);
    rewrite(htmlfile);
    Write(htmlfile, html_tabla);
  finally
     CloseFile(htmlfile);
  end;
end;


/// Generar la tabla con los documentos
Procedure TCustomerDocumentsExcelAndPdf.Genera_tabla_documentos(cliente, pathToSave:string;tipo_doc:integer);
var Adodataset_aux:Tadodataset;
  HTML_str: widestring;
  html_pie, html_lin_ini, html_lin_fin,strql :ansistring;
  ruta_fichero_pdf, ruta_fichero_excel:ansistring;
  i:integer;
 // xcliente_id:string;
begin
  lee_dato_ini(ejecutable_ini,'ficheros_html','TablaDocs_pie',html_pie);

   case tipo_doc of
      2, 22: // albaranes
        begin
            lee_dato_ini(ejecutable_ini,'ficheros_html','TablaDocs_lin_alb_fact',html_lin_ini);
        end;
      3, 23: // facturas
        begin
            lee_dato_ini(ejecutable_ini,'ficheros_html','TablaDocs_lin_alb_fact',html_lin_ini);
        end;
   end;

   ///Genera la tabla.
     Adodataset_aux:= TADODataSet.Create(nil);
     Adodataset_aux.Connection:=ADOConnection_imp;
     //consulta sobre la tabla web_docs_ftp  para recuperar todos los documentos que han de incluirse en la tabla.
     lee_dato_ini(ejecutable_ini, 'sql_select', 's_doctabla', strql);

    with Adodataset_aux do
    begin
       ParamCheck:=true;
       CommandText:=strql;
       Parameters[0].value:=cliente;
       Parameters[1].value:=tipo_doc;
       Active:=true;
    end;

    obtener_cabecera_TablaDoc(HTML_str, tipo_doc);

    if Adodataset_aux.RecordCount>0 then
    begin //se debe generar el fichero con la tabla
      //obtener_cabecera_TablaDoc(HTML_str, tipo_doc);
      Adodataset_aux.First;
      while not Adodataset_aux.Eof do
      begin
         html_lin_fin:='';
         html_lin_fin:=carga_ficherowide(html_lin_ini,false);
         if i mod 2 = 0 then
          begin
              html_lin_fin:= StringReplace(html_lin_fin, '@xcolor_lin', '<tr align="center" bgcolor="#ffffff" >',[rfReplaceAll]);
          end
          else
          begin
              html_lin_fin:= StringReplace(html_lin_fin, '@xcolor_lin', '<tr align="center" bgcolor="#eeeeee" >',[rfReplaceAll]);
          end;
          html_lin_fin:= StringReplace(html_lin_fin, '@xnumdoc_id', adodataset_aux.FieldByName('xnumdoc_id').asstring,[rfReplaceAll]);
          html_lin_fin:= StringReplace(html_lin_fin, '@xfecha_doc', adodataset_aux.FieldByName('xfecha_doc').asstring,[rfReplaceAll]);
          html_lin_fin:= StringReplace(html_lin_fin, '@ximporte', adodataset_aux.FieldByName('ximporte').asstring,[rfReplaceAll]);
          Obtener_ruta_fichero_pdf_TablaDocs_ftp(
            Adodataset_aux.FieldByName('xcliente_id').asstring,
            Adodataset_aux.FieldByName('xnumdoc_id').asstring,
            Adodataset_aux.FieldByName('xfecha_doc').asstring,
            ruta_fichero_pdf,
            ruta_fichero_excel,
            tipo_doc
          );
          html_lin_fin:= StringReplace(html_lin_fin, '@ruta_fichero_pdf', ruta_fichero_pdf,[rfReplaceAll]);
          html_lin_fin:= StringReplace(html_lin_fin, '@ruta_fichero_excel', ruta_fichero_excel,[rfReplaceAll]);
          HTML_str:=HTML_str +html_lin_fin;
          Adodataset_aux.next;
          i:=i+1;
      end;
      HTML_str:=HTML_str+carga_ficherowide(html_pie, False);
      gestiona_fichero_tabla_documentos(cliente, pathToSave, tipo_doc, HTML_str);
    end;
    Adodataset_aux.Free;
end;




procedure TCustomerDocumentsExcelAndPdf.Generar_tabla_consumo_html(cliente_id, fichero_excel, fecha_consulta, ruta_fichero_local:string);
var
  htmlfile:textfile;
  HTML_str: widestring;
  html_cab, html_pie, html_lin_ori, html_lin_final,nombre_fichero_consumo :ansistring;
  nombre_fichero_html, carpeta_consumo_ftp,fichero_excel_consumo_ftp, web_alfa:ansistring;
  i: integer;
begin

  lee_dato_ini(ejecutable_ini,'consumo','TablaConsumo_cab',html_cab);
  lee_dato_ini(ejecutable_ini,'consumo','TablaConsumo_pie',html_pie);
  lee_dato_ini(ejecutable_ini,'consumo','tablaConsumo_lin',html_lin_ori);
  lee_dato_ini(ejecutable_ini,'consumo','nombre_fichero_consumo_html_ftp', nombre_fichero_html);
  lee_dato_ini(ejecutable_ini,'consumo','nombre_fichero_consumo_local', nombre_fichero_consumo);
  lee_dato_ini(ejecutable_ini, 'rutas', 'dir_consumo_ftp', carpeta_consumo_ftp);

  HTML_str:=carga_ficherowide(html_cab, false);
  lee_dato_ini(ejecutable_ini, 'rutas','web_alfa', web_alfa);
  fichero_excel_consumo_ftp:=web_alfa+cliente_id+carpeta_consumo_ftp+nombre_fichero_html+copy(StrCheckSum(cliente_id),1,6)+'.xls';

  try
  begin
      html_lin_final:='';
      html_lin_final:=carga_ficherowide(html_lin_ori,false);
      if i mod 2 = 0 then
        begin
            html_lin_final:= StringReplace(html_lin_final, '@xcolor_lin', '<tr align="center" bgcolor="#ffffff" >',[rfReplaceAll]);
        end
        else
        begin
            html_lin_final:= StringReplace(html_lin_final, '@xcolor_lin', '<tr align="center" bgcolor="#eeeeee" >',[rfReplaceAll]);
        end;
        html_lin_final:= StringReplace(html_lin_final, '@ruta_fichero_consumo', fichero_excel_consumo_ftp,[rfReplaceAll]);
        html_lin_final:= StringReplace(html_lin_final, '@xfecha',fecha_consulta,[rfReplaceAll]);
        HTML_str:=HTML_str +html_lin_final;

        HTML_str:=HTML_str +carga_ficherowide(html_pie, False);
  end;
  finally
    try

      if FileExists(ruta_fichero_local+nombre_fichero_consumo+'.html') then
            DeleteFile(pChar(ruta_fichero_local+nombre_fichero_consumo+'.html'));
      AssignFile(htmlfile, ruta_fichero_local+nombre_fichero_consumo+'.html');
      rewrite(htmlfile);
      Write(htmlfile, HTML_str);
    finally
       CloseFile(htmlfile);
    end;
  end;
end;


procedure TCustomerDocumentsExcelAndPdf.BuildInvoices();
var
  adodataset_factura: TADODataSet;
  facturaPdf: timp_factura_pdf;
  facturaExcel: Timp_Factura_Excel;
  fecha_doc:string;
begin
  adodataset_factura := TADODataSet.Create(nil);

  with adodataset_factura do
  begin
    ParamCheck := true;
    Connection := ADOConnection_imp;
    CommandText := (array_select[sFacturas]);

      {*
    startDocumentId := '9012060';
    endDoumentId := '9012063';
    startCycle := '2019';
    endCycle := '2019';
    counted := '-1';
    reprint := '1';
    pathToSave := 'C:\ADWebCustomerDocuments\';
                        *}

    /// /PARAMETROS DE LA CONSULTA
    Parameters[0].Value := startCustomerId;
    Parameters[1].Value := endCustomerId;
    if startDocumentId = '' then Parameters[2].Value := null else Parameters[2].Value := startDocumentId  ;
    if endDoumentId = '' then Parameters[3].Value := null else Parameters[3].Value := endDoumentId ;
    if startDate = '' then Parameters[4].Value := null else Parameters[4].Value := startDate ;
    if endDate = '' then Parameters[5].Value := null else Parameters[5].Value := endDate ;
    if startCycle = '' then Parameters[6].Value := null else Parameters[6].Value := startCycle ;
    if endCycle = '' then Parameters[7].Value := null else Parameters[7].Value := endCycle ;
    if counted = '' then Parameters[8].Value := null else Parameters[8].Value := counted ;
    if reprint = '' then Parameters[9].Value := null else Parameters[9].Value := reprint ;

    try
      begin
        Active := true;
        First;
      end;
    except
      on E: Exception do
      begin
        //Guarda_registro_en_FicheroLog('Genera_Facturas -- ERROR: ');
      end;
    end;
  end;
  if adodataset_factura.RecordCount > 0 then
  begin
      while not adodataset_factura.eof do
      begin
        if adodataset_factura.FieldByName('xaparicion_doc').AsString='1' then
         begin

            facturaPdf := Timp_Factura_Pdf.Create;
            facturaPdf.conexion := ADOConnection_imp;
            facturaPdf.CliIni := adodataset_factura.FieldByName('xcliente_id').AsString;
            facturaPdf.CliFin := adodataset_factura.FieldByName('xcliente_id').AsString;
            facturaPdf.DocIni := adodataset_factura.FieldByName('xnumdoc_id').AsString;
            facturaPdf.DocFin := adodataset_factura.FieldByName('xnumdoc_id').AsString;
            facturaPdf.CicloIni := adodataset_factura.FieldByName('anyo').AsString;
            facturaPdf.CicloFin := adodataset_factura.FieldByName('anyo').AsString;
            facturaPdf.Contabilizado := '-1';
            facturaPdf.Reimp := '-1';
            facturaPdf.PathToSave := pathToSave;

            if facturaPdf.crea_factura(frxDBDataset1, frxDBDataset2, 'Cli') then
            begin
                guarda_doc_bd(
                  adodataset_factura.FieldByName('xcliente_id').AsString,
                  adodataset_factura.FieldByName('xnumdoc_id').AsString,
                  adodataset_factura.FieldByName('xfecha_doc').AsString,
                  '0',
                  '23',
                  adodataset_factura.FieldByName('ximporte').AsString,
                  ADOConnection_imp,
                  'PDF'
                );
            end
            else
            begin
              //Guarda_registro_en_FicheroLog('Genera_Facturas -- Error: ' + adodataset_factura.FieldByName('xnumdoc_id').AsString);
            end;
            facturaPdf.Free;


            facturaExcel := timp_factura_excel.Create;
            facturaExcel.conexion := ADOConnection_imp;
            facturaExcel.CliIni := adodataset_factura.FieldByName('xcliente_id').AsString;
            facturaExcel.CliFin := adodataset_factura.FieldByName('xcliente_id').AsString;
            facturaExcel.DocIni := adodataset_factura.FieldByName('xnumdoc_id').AsString;
            facturaExcel.DocFin := adodataset_factura.FieldByName('xnumdoc_id').AsString;
            facturaExcel.CicloIni := adodataset_factura.FieldByName('anyo').AsString;
            facturaExcel.CicloFin := adodataset_factura.FieldByName('anyo').AsString;
            facturaExcel.Contabilizado := '-1';
            facturaExcel.Reimp := '-1';
            facturaExcel.PathToSave := pathToSave;
            facturaExcel.completo:= true;
            if facturaExcel.crea_factura('Cli', FlexCelReport_Docs, XLSAdapter1, adodts_consumo) then
            begin
                guarda_doc_bd(
                  adodataset_factura.FieldByName('xcliente_id').AsString,
                  adodataset_factura.FieldByName('xnumdoc_id').AsString,
                  adodataset_factura.FieldByName('xfecha_doc').AsString,
                  '0',
                  '23',
                  adodataset_factura.FieldByName('ximporte').AsString,
                  ADOConnection_imp,
                  'EXCEL'
                );
            end
            else
            begin
              //Guarda_registro_en_FicheroLog('Genera_Facturas -- Error: ' + adodataset_factura.FieldByName('xnumdoc_id').AsString);
            end;
            facturaExcel.Free;


            Genera_tabla_documentos(adodataset_factura.FieldByName('xcliente_id').AsString, pathToSave, 23);
            BuildCustomerConsumption(adodataset_factura.FieldByName('xcliente_id').AsString);
         end;
            adodataset_factura.Next;
       end;
      //Guarda_registro_en_FicheroLog( 'Genera_Facturas -- N�Facturas: ' + inttostr(i));
  end;

end;



procedure TCustomerDocumentsExcelAndPdf.BuildDeliveryNote();
var
  albaranPdf: Timp_Albaran_PDF;
  albaranExcel: Timp_Albaran_Excel;
  adodataset_albaran: TADODataSet;
  fecha_doc:string;
begin
  adodataset_albaran := TADODataSet.Create(nil);

  with adodataset_albaran do
  begin
    ParamCheck := true;
    Connection := ADOConnection_imp;
    CommandText := (array_select[sAlbaranes]);


    {*
    startDocumentId := '9014177';
    endDoumentId := '9014192';
    reprint := '0';
    pathToSave := 'C:\ADWebCustomerDocuments\';
    *}


    /// /PARAMETROS DE LA CONSULTA
    Parameters[0].Value := startCustomerId;
    Parameters[1].Value := endCustomerId;
    if startDocumentId = '' then Parameters[2].Value := null else Parameters[2].Value := startDocumentId  ;
    if endDoumentId = '' then Parameters[3].Value := null else Parameters[3].Value := endDoumentId ;
    if startDate = '' then Parameters[4].Value := null else Parameters[4].Value := startDate ;
    if endDate = '' then Parameters[5].Value := null else Parameters[5].Value := endDate ;
    if reprint = '' then Parameters[6].Value := null else Parameters[6].Value := reprint ;

    try
      begin
        Active := true;
        First;
      end;
    except
      on E: Exception do
      begin
        //Guarda_registro_en_FicheroLog('Genera_Albaran -- ERROR: ');
      end;
    end;
  end;

  if adodataset_albaran.RecordCount > 0 then
  begin
    while not adodataset_albaran.eof do
    begin
       if adodataset_albaran.FieldByName('xaparicion_doc').AsString='1' then
         begin

            albaranPdf := Timp_Albaran_PDF.Create;
            albaranPdf.conexion := ADOConnection_imp;
            albaranPdf.DocIni := adodataset_albaran.FieldByName('xnumdoc_id').AsString;
            albaranPdf.DocFin := adodataset_albaran.FieldByName('xnumdoc_id').AsString;
            albaranPdf.CliIni := adodataset_albaran.FieldByName('xcliente_id').AsString;
            albaranPdf.CliFin := adodataset_albaran.FieldByName('xcliente_id').AsString;
            albaranPdf.Reimp := reprint;
            albaranPdf.PathToSave := pathToSave;

            if albaranPdf.crea_albaran(frxDBDataset1, frxDBDataset2, 'Cli') then
            begin
              fecha_doc:=stringreplace(adodataset_albaran.FieldByName('xfecha_doc').asstring, '/','',[rfReplaceAll]);
              fecha_doc:=stringreplace(fecha_doc,'-','', [rfReplaceAll]);

              guarda_doc_bd(adodataset_albaran.FieldByName('xcliente_id').AsString, adodataset_albaran.FieldByName('xnumdoc_id').AsString,adodataset_albaran.FieldByName('xfecha_doc').AsString, '0', '22', adodataset_albaran.FieldByName('ximporte').AsString, ADOConnection_imp, 'PDF');

            end
            else
            begin
              //Guarda_registro_en_FicheroLog( 'Genera_Albaranes -- Error: ' + adodataset_albaran.FieldByName('xnumdoc_id').AsString);
            end;
            albaranPdf.Free;


            albaranExcel := Timp_Albaran_Excel.Create;
            albaranExcel.conexion := ADOConnection_imp;
            albaranExcel.DocIni := adodataset_albaran.FieldByName('xnumdoc_id').AsString;
            albaranExcel.DocFin := adodataset_albaran.FieldByName('xnumdoc_id').AsString;
            albaranExcel.CliIni := adodataset_albaran.FieldByName('xcliente_id').AsString;
            albaranExcel.CliFin := adodataset_albaran.FieldByName('xcliente_id').AsString;
            albaranExcel.Reimp := reprint;
            albaranExcel.PathToSave := pathToSave;

            if albaranExcel.crea_albaran('Cli', FlexCelReport_Docs, XLSAdapter1, adodts_consumo) then
            begin
              fecha_doc:=stringreplace(adodataset_albaran.FieldByName('xfecha_doc').asstring, '/','',[rfReplaceAll]);
              fecha_doc:=stringreplace(fecha_doc,'-','', [rfReplaceAll]);

              guarda_doc_bd(adodataset_albaran.FieldByName('xcliente_id').AsString, adodataset_albaran.FieldByName('xnumdoc_id').AsString,adodataset_albaran.FieldByName('xfecha_doc').AsString, '0', '22', adodataset_albaran.FieldByName('ximporte').AsString, ADOConnection_imp, 'PDF');

            end
            else
            begin
              //Guarda_registro_en_FicheroLog( 'Genera_Albaranes -- Error: ' + adodataset_albaran.FieldByName('xnumdoc_id').AsString);
            end;
            albaranExcel.Free;

            Genera_tabla_documentos(adodataset_albaran.FieldByName('xcliente_id').asstring, pathToSave, 22);
         end;
      adodataset_albaran.Next;
    end;
  end;
end;





procedure TCustomerDocumentsExcelAndPdf.BuildCustomerConsumption(cliente_id:string);
var s_consumo, anyo :ansistring;
dd,mm,yy:word;
anyo_consumo:integer;
fichero_plantilla,  nombre_Fichero_consumo_local:ansistring;
ruta_fichero_consumo_local, ruta_fichero_consumo_ftp,nombre_fichero_consumo_html_ftp:ansistring;
fichero_excel_local:string;
 //fichero_excel_ftp,
fichero_html_local, fichero_html_ftp:string;
//carpeta_consumo_ftp, ruta_excel_ftp: ansistring;
begin
     lee_dato_ini(ejecutable_ini, 'Consumo', 'anyo', anyo);
     lee_dato_ini(ejecutable_ini, 'rutas', 'dir_consumo_local', dir_consumo_local);
     lee_dato_ini(ejecutable_ini, 'rutas', 'dir_consumo_ftp', dir_consumo_ftp);
     lee_dato_ini(ejecutable_ini, 'rutas', 'dir_ppal_ftp', ruta_ppal_ftp);

     DecodeDate(Now,yy,mm,dd);
     anyo_consumo:=yy-strtoint(anyo);
     if adodts_consumo.Active  then adodts_consumo.Active:=false;
     adodts_consumo.Connection:=ADOConnection_imp;

     try
        begin
         lee_dato_ini(ejecutable_ini, 'sql_select', 's_consumo', s_consumo);
         adodts_consumo.ParamCheck:=true;
         adodts_consumo.CommandText:=s_consumo;
         adodts_consumo.Parameters[0].Value:= cliente_id;
         adodts_consumo.Parameters[1].Value:= anyo_consumo;
         adodts_consumo.Active:=True;
        end;

        ////////////////////

        ruta_fichero_consumo_local:=pathToSave+cliente_id+dir_consumo_local;

        if not DirectoryExists(PathToSave) then
            CreateDir(PathToSave);

        if not DirectoryExists(PathToSave + cliente_id) then
            CreateDir(PathToSave + cliente_id);

        if  not DirectoryExists(PathToSave + cliente_id + dir_consumo_ftp) then
            CreateDir(PathToSave + cliente_id + dir_consumo_ftp);

        lee_dato_ini(ejecutable_ini, 'consumo', 'fichero_plantilla',fichero_plantilla);
        lee_dato_ini(ejecutable_ini, 'consumo', 'nombre_Fichero_consumo_local', nombre_Fichero_consumo_local);
        flexcelreport_consumo.Template:=fichero_plantilla;

        flexcelreport_consumo.AutoClose:=true;
        flexcelreport_consumo.FileName:=ruta_fichero_consumo_local+nombre_Fichero_consumo_local+copy(StrCheckSum(cliente_id),1,6)+'.xls';

        if FileExists(flexcelreport_consumo.FileName) then DeleteFile(flexcelreport_consumo.FileName);

        flexcelreport_consumo.Values['articulo']:= adodts_consumo.fieldbyname('articulo').AsString;
        flexcelreport_consumo.Values['descripcion']:= adodts_consumo.fieldbyname('descripcion').AsString;
        flexcelreport_consumo.Values['caja']:= adodts_consumo.fieldbyname('caja').AsString;
        flexcelreport_consumo.Values['blister']:= adodts_consumo.fieldbyname('blister').AsString;
        flexcelreport_consumo.Values['medida']:= adodts_consumo.fieldbyname('medida').AsString;
        flexcelreport_consumo.Values['c_barras']:= adodts_consumo.fieldbyname('c_barras').AsString;
        flexcelreport_consumo.Values['pvp_actual']:= adodts_consumo.fieldbyname('pvp_actual').AsString;
        flexcelreport_consumo.Values['xnombre']:= adodts_consumo.fieldbyname('xnombre').AsString;
        flexcelreport_consumo.Values['fecha_consulta']:= 'PRECIO COMPRA ACTUAL '+datetostr(date());

        flexcelreport_consumo.Run;
        ////////////////////////////////////////////////////////////

        //// Generar la tabla_consumo_html
        Generar_tabla_consumo_html(cliente_id, nombre_Fichero_consumo_local, datetostr(date()), ruta_fichero_consumo_local );

      finally

     end;

end;



procedure TCustomerDocumentsExcelAndPdf.BuildSalesManOrder(repre:string);
var ruta_fichero_rptPedidos_local, ruta_fichero_rptPedidos_consumo_ftp:ansistring;
begin
  lee_dato_ini(ejecutable_ini, 'rutas', 'dir_rptPedidosCli_local', dir_rptPedidosCli_local);
  lee_dato_ini(ejecutable_ini, 'rutas', 'dir_ppal_ftp', ruta_ppal_ftp);
  lee_dato_ini(ejecutable_ini,'rutas', 'fichero_plantilla',fichero_plantilla);
  lee_dato_ini(ejecutable_ini,'rutas', 'fichero_rptPedidos_local', fichero_rptPedidos_local);

  ruta_fichero_rptPedidos_local:=pathToSave+'660'+repre+dir_rptPedidosCli_local;
  ruta_fichero_rptPedidos_consumo_ftp:=ruta_ppal_ftp+'660'+repre+dir_rptPedidosCli_ftp;

  if not DirectoryExists(PathToSave) then
            CreateDir(PathToSave);

  if not DirectoryExists(PathToSave + '660'+repre) then
      CreateDir(PathToSave + '660'+repre);

  if  not DirectoryExists(PathToSave + '660'+repre + dir_rptPedidosCli_local) then
      CreateDir(PathToSave + '660'+repre + dir_rptPedidosCli_local);

  flexcelreport_pedidos.Template:=fichero_plantilla;

  flexcelreport_pedidos.AutoClose:=true;
  flexcelreport_pedidos.FileName:=ruta_fichero_rptPedidos_local+fichero_rptPedidos_local+REPRE+'.xls';
  if FileExists(ruta_fichero_rptPedidos_local+fichero_rptPedidos_local+REPRE+'.xls') then
    DeleteFile(ruta_fichero_rptPedidos_local+fichero_rptPedidos_local+REPRE+'.xls');


  flexcelreport_pedidos.Values['xnom_repre']:= repre +' - '+ ado_pedrep.fieldbyname('xnom_repre').AsString;
  flexcelreport_pedidos.Values['xcliente_id']:= ado_pedrep.fieldbyname('xcliente_id').AsString;
  flexcelreport_pedidos.Values['xnombre']:= ado_pedrep.fieldbyname('xnombre').AsString;
  flexcelreport_pedidos.Values['xfecha_ped']:= ado_pedrep.fieldbyname('xfecha_ped').AsString;
  flexcelreport_pedidos.Values['ximporte_ped']:= ado_pedrep.fieldbyname('ximporte_ped').AsString;
  flexcelreport_pedidos.Values['tipo_ped']:= ado_pedrep.fieldbyname('tipo_ped').AsString;


  flexcelreport_pedidos.Run;

end;



procedure TCustomerDocumentsExcelAndPdf.Generar_Tabla_html_local(repre:string);
var htmlfile:textfile;
  HTML_str: widestring;
  html_lin_final, ruta_y_fichero_ftp, web_alfa, fichero_rptPedidos_ftp, html_cab, html_lin_ori, html_pie  :ansistring;
  i: integer;
begin
  lee_dato_ini(ejecutable_ini, 'rutas','web_alfa', web_alfa);
  lee_dato_ini(ejecutable_ini,'rutas', 'fichero_rptPedidos_ftp', fichero_rptPedidos_ftp);
  ruta_y_fichero_ftp:=web_alfa+'660'+repre+ dir_rptPedidosCli_ftp+ fichero_rptPedidos_ftp+repre+'.xls';

  lee_dato_ini(ejecutable_ini,'ficheros_html','TablaClienes_cab',html_cab);
  lee_dato_ini(ejecutable_ini,'ficheros_html','TablaClientes_lin',html_lin_ori);
  lee_dato_ini(ejecutable_ini,'ficheros_html','TablaClientes_pie',html_pie);

  HTML_str:=carga_ficherowide(html_cab, false);
  try
  begin
      ado_pedrep.First;
      for i:=0 to ado_pedrep.RecordCount-1 do
      begin

          html_lin_final:='';
          html_lin_final:=carga_ficherowide(html_lin_ori,false);
          if i mod 2 = 0 then
            begin
                html_lin_final:= StringReplace(html_lin_final, '@xcolor_lin', '<tr align="center" bgcolor="#ffffff" >',[rfReplaceAll]);
            end
            else
            begin
                html_lin_final:= StringReplace(html_lin_final, '@xcolor_lin', '<tr align="center" bgcolor="#eeeeee" >',[rfReplaceAll]);
            end;
            html_lin_final:= StringReplace(html_lin_final, '@xfecha_ped',ado_pedrep.FieldByName('xfecha_ped').asstring,[rfReplaceAll]);
            html_lin_final:= StringReplace(html_lin_final, '@xcliente_id',ado_pedrep.FieldByName('xcliente_id').asstring,[rfReplaceAll]);
            html_lin_final:= StringReplace(html_lin_final, '@xnombre',ado_pedrep.FieldByName('xnombre').asstring,[rfReplaceAll]);
            html_lin_final:= StringReplace(html_lin_final, '@ximporte_ped',ado_pedrep.FieldByName('ximporte_ped').asstring,[rfReplaceAll]);
            html_lin_final:= StringReplace(html_lin_final, '@ximporte_ped',ado_pedrep.FieldByName('ximporte_ped').asstring,[rfReplaceAll]);
            html_lin_final:= StringReplace(html_lin_final, '@tipo_ped',ado_pedrep.FieldByName('tipo_ped').asstring,[rfReplaceAll]);

            HTML_str:=HTML_str +html_lin_final;
            ado_pedrep.Next;
      end;
        HTML_str:= StringReplace(HTML_str, '@ruta_fichero_pedidos',ruta_y_fichero_ftp,[rfReplaceAll]);
        HTML_str:=HTML_str +carga_ficherowide(html_pie, False);
  end;
  finally
    try

      if FileExists(pathToSave+'660'+repre+dir_rptPedidosCli_local+fichero_rptPedidos_local+repre+'.html') then
            DeleteFile(pChar(pathToSave+'660'+repre+dir_rptPedidosCli_local+fichero_rptPedidos_local+repre+'.html'));
      AssignFile(htmlfile, pathToSave+'660'+repre+dir_rptPedidosCli_local+fichero_rptPedidos_local+repre+'.html');
      rewrite(htmlfile);
      Write(htmlfile, HTML_str);
    finally
       CloseFile(htmlfile);
    end;
  end;

end;



procedure TCustomerDocumentsExcelAndPdf.ManagerSalesManOrder();
var  fichero_excel_local, fichero_excel_ftp, fichero_html_local, fichero_html_ftp:string;
ruta_fichero_rptPedidos_local, ruta_fichero_rptPedidos_consumo_ftp, ruta_html_ftp,   pedidos_repre, repres_con_pedidos:ansistring;
repre:string;
a:integer;
begin
  lee_dato_ini(ejecutable_ini,'SQL','pedidos_repre', pedidos_repre );

  bda_conectabd(ADOConnection_imp);
  lee_dato_ini(ejecutable_ini, 'sql','repres_con_pedidos', repres_con_pedidos);
  //adodts_repres:= TADODataSet.Create(nil);
  ADOdts_repres.Connection:=ADOConnection_imp;
  ADOdts_repres.CommandText:=repres_con_pedidos;
  try
    begin
      adodts_repres.Active:=true;
    end;
  except
    on E: exception do
    begin
        //Showmessage('Error en la consulta de Lista Representantes');
    end;
  end;


  if adodts_repres.RecordCount>0 then
  begin
   adodts_repres.First;

    for a:=0 to adodts_repres.RecordCount-1 do
     begin
        ado_pedrep.Active:=false;
        repre:=adodts_repres.FieldByName('xrepresentante_id').asstring;
        ado_pedrep.Connection:=ADOConnection_imp;
        ado_pedrep.ParamCheck:=true;
        ado_pedrep.CommandText:=pedidos_repre;
        ado_pedrep.Parameters[0].Value:=repre;
        ado_pedrep.Active:=true;

        if ado_pedrep.RecordCount>0 then
        begin
           ///Fichero Excel
           BuildSalesManOrder(repre);

           //// Generar la tabla_consumo_html
           Generar_Tabla_html_local(repre);
        end;
        adodts_repres.Next;
     end
  end;

end;



function TCustomerDocumentsExcelAndPdf.marcar_efecto_borrado(cliente, documento: string; fecha_vto: string): boolean;
var
  adts_borra_efecto: Tadocommand;
begin
  /// // marcar efecto vencido
  adts_borra_efecto := Tadocommand.Create(nil);
  with adts_borra_efecto do
  begin
    ParamCheck := true;
    Connection := ADOConnection_imp;
    CommandText := (array_update[upd_efecto_borrar]);
    Parameters[0].Value := cliente;
    Parameters[1].Value := trim(documento);
    if fecha_vto = '' then
      Parameters[2].Value := null
    else
      Parameters[2].Value := fecha_vto;

    try
      begin
        execute;
        Result := true;
      end;
    except
      on e: exception do
      begin
        Result := false;
        //Guarda_reg_FicheroLog('Error -- marcar_efecto_borrado -- ' + datetostr(now))
      end;
    end;
  end;
  adts_borra_efecto.Free;
end;


procedure TCustomerDocumentsExcelAndPdf.tratar_clientes_con_relacion_efectos();
var
  cliente: string;
  adodts_efecto_marcar_borrar: TADODataSet;
  efecto: Timp_efecto_pte;
  i: integer;
begin
  /// / Obtenemos los efectos a marcar como borrados
  adodts_efecto_marcar_borrar := TADODataSet.Create(nil);
  adodts_efecto_marcar_borrar.CommandTimeout:=90;
  with adodts_efecto_marcar_borrar do
  begin
    ParamCheck := true;
    Connection := ADOConnection_imp;
    CommandText := (array_select[Web_s_rel_efectos_actualizar]);
    /// parametro con valor 1 para obtener CLIENTES Y DOCUMENTOS tenemos que generar realcion efectos
    Parameters[0].Value := '1';
    try
      Active := true;
      First;
    except
      on e: exception do
      begin
          //Guarda_reg_FicheroLog('Error -- Imposible Obtener Efectos para borrar [Web_s_rel_efectos_actualizar] -- ' + datetostr(now))
      end;
    end;
  end;

  /// / marcar efecto vencido como borrado   --- MARCAR EFECTO BORRADO  ---
  if adodts_efecto_marcar_borrar.RecordCount > 0 then
  begin
    while not adodts_efecto_marcar_borrar.Eof do
    begin

      if not marcar_efecto_borrado(adodts_efecto_marcar_borrar.FieldByName
        ('cuenta').AsString, adodts_efecto_marcar_borrar.FieldByName
        ('xdocumento_id').AsString, adodts_efecto_marcar_borrar.FieldByName
        ('fecha_vto').AsString) then
      begin
          //Guarda_reg_FicheroLog('Error -marcar efecto vencido como borrado -Borrar- ' + adodts_efecto_marcar_borrar.FieldByName('xcliente_id').AsString + ' -- ' + trim(adodts_efecto_marcar_borrar.FieldByName ('xdocumento_id').AsString) + ' -- ' + datetostr(now))
      end;

      adodts_efecto_marcar_borrar.Next;
    end;
  end;

  adodts_efecto_marcar_borrar.Free;

end;



function TCustomerDocumentsExcelAndPdf.crea_relacion_efectos_cedidos(cliente: string): boolean;
var
  efecto: Timp_efecto_pte;
begin
  Result := true;
  efecto := Timp_efecto_pte.Create;
  efecto.conexion := ADOConnection_imp;
  efecto.CliIni := cliente;
  efecto.CliFin := cliente;
  efecto.PathToSave := pathToSave;
  try
    begin
      if efecto.crea_efecto(frxDBDataset1, frxDBDataset2) then
      begin
        Result := true;
      end
      else
        Result := false;
    end
  finally
    efecto.Free;
  end;
end;


procedure TCustomerDocumentsExcelAndPdf.Genera_pagos_ptes();
var
  adodts_relefecto_cab_aux: TADODataSet;
begin

  adodts_relefecto_cab_aux := TADODataSet.Create(nil);
  with adodts_relefecto_cab_aux do
  begin
    ParamCheck := true;
    Connection := ADOConnection_imp;
    CommandText := (array_select[s_efectos_cab]);
    Parameters[0].Value := '';
    Parameters[1].Value := '';
    try
      Active := true;
      First;
    except
      on e: exception do
      begin
        //Guarda_reg_FicheroLog('Error - Genera_pagos_ptes --- Imposible Seleccionar Cabeceras [s_efectos_cab] -- ' + datetostr(now));
      end;
    end;
  end;

  if adodts_relefecto_cab_aux.RecordCount > 0 then
  begin
    while not adodts_relefecto_cab_aux.Eof do
    begin
      //crear_directorios(adodts_relefecto_cab_aux.FieldByName('xcliente_id').AsString);

      if crea_relacion_efectos_cedidos(adodts_relefecto_cab_aux.FieldByName('xcliente_id').AsString) then
      begin
        //Guarda_reg_FicheroLog('OK -- Genera_pagos_ptes -- ' + adodts_relefecto_cab_aux.FieldByName('xcliente_id').AsString + ' -- ' + datetostr(now));
      end
      else
      begin
        //arda_reg_FicheroLog('Error -- Genera_pagos_ptes-- ' + adodts_relefecto_cab_aux.FieldByName('xcliente_id').AsString + ' -- ' + datetostr(now));
      end;
      adodts_relefecto_cab_aux.Next;
    end;
  end;

  adodts_relefecto_cab_aux.Free;
end;

procedure TCustomerDocumentsExcelAndPdf.OutstandingPayments();
begin
    tratar_clientes_con_relacion_efectos();
    Genera_pagos_ptes();
end;


procedure TCustomerDocumentsExcelAndPdf.FormActivate(Sender: TObject);
begin
  if ParamCount > 0 then
    begin
      SetParams;

      if(typeDocument = '2') or (typeDocument = '22') then
        BuildDeliveryNote
      else if(typeDocument = '3') or (typeDocument = '23') then
        BuildInvoices
      else if typeDocument = '4' then
        BuildCustomerConsumption(startCustomerId)
      else if typeDocument = '5' then
        ManagerSalesManOrder
      else if typeDocument = '6' then
         OutstandingPayments
      else
        begin
          BuildDeliveryNote;
          BuildInvoices;
          ManagerSalesManOrder;
          OutstandingPayments;
        end;
    end
  else
    begin
      SetParams;

      typeDocument := '4';
      pathToSave := 'C:\ADWebCustomerDocuments\';

      if(typeDocument = '2') or (typeDocument = '22') then
        BuildDeliveryNote
      else if(typeDocument = '3') or (typeDocument = '23') then
        BuildInvoices
      else if typeDocument = '4' then
        BuildCustomerConsumption(startCustomerId)
      else if typeDocument = '5' then
        ManagerSalesManOrder
      else if typeDocument = '6' then
         OutstandingPayments
      else
        begin
          BuildDeliveryNote;
          BuildInvoices;
          ManagerSalesManOrder;
          OutstandingPayments;
        end;

    end;

    application.Terminate;
end;

procedure TCustomerDocumentsExcelAndPdf.FormCreate(Sender: TObject);
begin
  bda_conectabd(ADOConnection_imp);
  ADODataSet1.Connection := ADOConnection_imp;
  DataSource1.DataSet := ADODataSet1;
  carga_consultas_array;
end;

end.
