object CustomerDocumentsExcelAndPdf: TCustomerDocumentsExcelAndPdf
  Left = 0
  Top = 0
  Caption = 'Export Customer Documents Excel and Pdf'
  ClientHeight = 318
  ClientWidth = 297
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object frxReport1: TfrxReport
    Version = '4.12.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 43510.480327731480000000
    ReportOptions.LastChange = 43510.480327731480000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 24
    Top = 16
    Datasets = <>
    Variables = <>
    Style = <>
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    BCDToCurrency = False
    Left = 24
    Top = 80
  end
  object frxDBDataset2: TfrxDBDataset
    UserName = 'frxDBDataset2'
    CloseDataSource = False
    BCDToCurrency = False
    Left = 24
    Top = 136
  end
  object ADOConnection_imp: TADOConnection
    CommandTimeout = 300
    ConnectionTimeout = 300
    Left = 104
    Top = 16
  end
  object DataSource1: TDataSource
    Left = 104
    Top = 80
  end
  object ADODataSet1: TADODataSet
    Parameters = <>
    Left = 96
    Top = 136
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    Left = 24
    Top = 200
  end
  object XLSAdapter1: TXLSAdapter
    AllowOverwritingFiles = False
    Left = 96
    Top = 200
  end
  object FlexCelReport_Docs: TFlexCelReport
    AutoClose = False
    Adapter = XLSAdapter1
    DateTimeFormat = 'mm/dd/yyyy hh:mm'
    KeepEmptyPictures = False
    DataModule = Owner
    Left = 96
    Top = 264
  end
  object adodts_consumo: TADODataSet
    Connection = ADOConnection_imp
    Parameters = <>
    Left = 24
    Top = 264
  end
  object FlexCelReport_consumo: TFlexCelReport
    AutoClose = False
    Adapter = XLSAdapter1
    DateTimeFormat = 'mm/dd/yyyy hh:mm'
    KeepEmptyPictures = False
    DataModule = Owner
    Left = 216
    Top = 16
  end
  object ado_pedrep: TADODataSet
    Parameters = <>
    Left = 184
    Top = 80
  end
  object flexcelreport_pedidos: TFlexCelReport
    AutoClose = False
    Adapter = XLSAdapter1
    DateTimeFormat = 'mm/dd/yyyy hh:mm'
    KeepEmptyPictures = False
    DataModule = Owner
    Left = 192
    Top = 136
  end
  object ADOdts_repres: TADODataSet
    Parameters = <>
    Left = 192
    Top = 200
  end
end
