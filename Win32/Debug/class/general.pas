
unit general;

interface

  uses  windows, system.classes,  sysutils,  shellapi,  messages,  inifiles,  forms,  filectrl,
  stdctrls,  SHLOBJ,    Vcl.Controls, Vcl.DBCtrls ,VCl.Buttons, cxcurrencyedit, cxdbedit, vcl.comctrls, ioutils, system.types, dialogs, cxContainer, adodb , typinfo;



type
 Tpermisos = class(Tcomponent)
  lista_admins:TStrings;
public
 usuario:string;
 es_usuario_all:boolean;
 es_usuario_admin:boolean;
 function permiso_para_grabar_articulo_ini( usuario:string):boolean;
 function carga_permisos_usuario(formulario:tform;usuario:string):boolean;overload;

 function carga_permisos_usuario:boolean;overload;
 procedure set_todos_los_permisos(form:tform);
 procedure set_ningun_permiso(form:tform);
 function es_admin(usuariostr:string):boolean;overload;
 function es_admin:boolean;overload;
 constructor create;
 end;


function GetFileTimes(const FileName: ansistring; var Created: TDateTime;
var Accessed: TDateTime; var Modified: TDateTime): Boolean;
function get_fecha_fichero_modificado(const filename):tdatetime;
procedure ficherodropped(var Msg: TMessage);
function FindAll (const Path: String; Attr: Integer):  TStrings ;
function carga_ficherowide(nombre_fichero:ansistring;desencriptar:boolean):widestring;
function carga_fichero(nombre_fichero:ansistring;desencriptar:boolean):tstrings;
function cambia_a_wide(testrings:Tstrings;separador:ansistring):widestring;
function cambia_a_tstrings(widestr:widestring;separador:ansistring):Tstrings;
function esta_en_la_lista(elemento:ansistring;lista:tstrings):boolean;
function getloginname:string;
function apartirdeligual(cadena:ansistring):ansistring;
 function carga_permisos_usuario_bd(conexion:tadoconnection; aplicacion:string; usuario:string):boolean;
//function estan_en_la_lista(elementos:tstrings;lista:tstrings):tstrings;


function carga_anios:tstrings;
function carga_empresas:tstrings;
function listado_ficheros(ruta:ansistring;mascara:ansistring):tstrings;
procedure carga_permisos_ini(formulario:tform;sender:tobject; usuario:string);




{function reemplaza(char_in: char; char_out: char; cadena:ansistring):ansistring;}

function lee_dato_ini (ficheroini:ansistring; seccion:ansistring; clave:ansistring; var vardestino:ANSIstring):boolean;overload;
function lee_dato_ini (ficheroini:ansistring; seccion:ansistring; clave:ansistring; var vardestino:integer):boolean;overload;
function lee_dato_ini (ficheroini:ansistring; seccion:ansistring; clave:ansistring; var vardestino:tdatetime):boolean;overload;
function lee_secciones_ini (ficheroini:ansistring):tstrings;

function lee_claves_ini (ficheroini:ansistring; seccion:ansistring):tstrings;
function lee_claves_valor(ficheroini:ansistring; seccion:ansistring):tstrings;
function modifica_dato_ini (ficheroini:ansistring; seccion:ansistring; clave:ansistring; dato:ansistring):boolean;overload;
function modifica_dato_ini (ficheroini:ansistring; seccion:ansistring; clave:ansistring; dato:integer):boolean;overload;
function modifica_dato_ini (ficheroini:ansistring; seccion:ansistring; clave:ansistring; dato:tdatetime):boolean;overload;

function crea_dato_ini(ficheroini:ansistring; seccion:ansistring; clave:ansistring; dato:ansistring):boolean;overload;
function crea_dato_ini(ficheroini:ansistring; seccion:ansistring; clave:ansistring; dato:tdatetime):boolean;overload;
function crea_dato_ini(ficheroini:ansistring; seccion:ansistring; clave:ansistring; dato:integer):boolean;overload;

function eliminar_clave_ini(ficheroini:ansistring; seccion:ansistring; clave:ansistring):boolean;
function eliminar_seccion_ini(ficheroini:ansistring; seccion:ansistring):boolean;


function Get_File_Size(sFileToExamine: ansistring): integer;
function ejecutable_ini:ansistring;
function ejecutable_nombre:ansistring;
function GetAppVersion:string;
function GetDirectorioSistema(Folder: Integer; CanCreate: Boolean): string;
function hexarandom:string;

function ejecutayespera  (aplicacion:string; parametros:string):boolean;

function deshabilitacontroles(formulario:tform):boolean;
function GetFileModDate(filename : string) : TDateTime;


var ficheros_droppeados:tstrings;
implementation
{$D+}
uses encripter, bdalfa;


function GetFileModDate(filename : string) : TDateTime;
var
   F : TSearchRec;
begin
   FindFirst(filename,faAnyFile,F);
   Result := F.TimeStamp;
   //if you really wanted an Int, change the return type and use this line:
   //Result := F.Time;
   FindClose(F);
end;



function deshabilitacontroles(formulario:tform):boolean;
var a,i:integer;
begin
   a:=0;
   for I := 1 to formulario.ComponentCount-1 do
   begin
     if (formulario.Components[i] is tcxcontainer)  then
       try
       tcontrol(formulario.Components[i]).enabled:=false;
       except
       showmessage(inttostr(i));
       end;
   end;
end;


 function carga_permisos_usuario_bd(conexion:tadoconnection; aplicacion:string; usuario:string):boolean;
var ado1:tadodataset;
 formulario,componente,propiedad:string;
 valor:variant;
 begin
   ado1:=tadodataset.Create(nil);
   ado1.Connection:=conexion;
   ado1.CommandText:='select * from imp.g733_permisos where aplicacion_id='+quotedstr(aplicacion)+' and usuario_id='+quotedstr('envase1');
   ado1.Active:=true;

   while  not ado1.eof do
   begin
     formulario:=ado1.FieldByName('formulario_id').AsString;
     componente:=ado1.FieldByName('componente_nombre').AsString;
     propiedad:=ado1.FieldByName('componente_propiedad').AsString;
     valor:=ado1.FieldByName('componente_prop_valor').Asvariant;
     try
       setpropvalue(tform(application.FindComponent(formulario)).findcomponent(componente),propiedad,valor);
     except

     end;
     ado1.Next;

   end;
   ado1.Active:=false;
   ado1.Free;

   {TODO -oOwner -cGeneral : ActionItem}
 end;











function ejecutayespera(aplicacion:string; parametros:string):boolean;
var

   SEInfo: TShellExecuteInfo;
   ExitCode: DWORD;
   ExecuteFile: string;
begin
     ExecuteFile:=aplicacion;

     FillChar(SEInfo, SizeOf(SEInfo), 0) ;
     SEInfo.cbSize := SizeOf(TShellExecuteInfo) ;
     with SEInfo do begin
      fMask := SEE_MASK_NOCLOSEPROCESS;
      Wnd := application.Handle;
      lpFile := PChar(ExecuteFile) ;
     {
ParamString can contain the
application parameters.
}
      lpParameters := PChar(parametros) ;
{
StartInString specifies the
name of the working directory.
If ommited, the current directory is used.
}
// lpDirectory := PChar(StartInString) ;
     nShow := SW_SHOWNORMAL;
   end;
   try
        if ShellExecuteEx(@SEInfo) then
        begin
             repeat
               application.ProcessMessages;
               GetExitCodeProcess(SEInfo.hProcess, ExitCode) ;
             until (ExitCode <> STILL_ACTIVE) or
             Application.Terminated;
             result:=true;
        end
   except
         result:=false;
   end
end;


function apartirdeligual(cadena:ansistring):ansistring;
begin
  result:=copy(cadena,pos('=',cadena)+1,length(cadena)-pos('=',cadena));
end;




function ejecutable_ini:ansistring;
begin
     result:=changefileext(application.exeName,'.ini');

end;


function hexarandom:string;
begin
     result := IntToHex(Random(65535),4);
end;

function GetLoginName: string;
var
  buffer: array[0..255] of char;
  size: dword;
begin
  size := 256;
  if GetUserName(buffer, size) then
    Result := buffer
  else
    Result := ''
end;

constructor tpermisos.create;
var i:integer;
    lista_usuarios:Tstringlist;
    usuariostr:ansistring;
begin
  self.usuario:=uppercase(getloginname);
  lista_admins:=Tstringlist.Create;

  lista_usuarios:=tstringlist.Create;
  lista_usuarios.AddStrings(lee_claves_ini(ejecutable_ini,'ADMIN'));
  for i := 0 to lista_usuarios.Count-1 do
  begin
    if lee_dato_ini(ejecutable_ini,'ADMIN',lista_usuarios[i],usuariostr) then
    lista_admins.Add(uppercase(desencripta(usuariostr,1499)));
  end;
end;

function tpermisos.es_admin(usuariostr: string):boolean;
begin

  result:=true;

  if (lista_admins.IndexOf(uppercase(usuariostr))=-1) then
     es_usuario_admin:=false
  else
     es_usuario_admin:=true;

  if (lista_admins.IndexOf('ALL')<>-1) then
     es_usuario_all:=true
  else
     es_usuario_all:=false;
  result:=es_usuario_all or es_usuario_admin;
end;

function tpermisos.es_admin:boolean;
begin
  result:=true;
  if (lista_admins.IndexOf(uppercase(self.usuario))=-1) then
  result:=false;
end;

function tpermisos.permiso_para_grabar_articulo_ini(usuario:string):boolean;
begin
  result:=es_admin(usuario);
end;

function tpermisos.carga_permisos_usuario(Formulario:Tform;usuario: string):boolean;
var listacomps_GRANT:Tstrings;
    listacomps_DENY:Tstrings;
    i:integer;
begin
   if not es_admin(usuario) then
   begin
     deshabilitacontroles(formulario);


    // tcontrol(formulario.findcomponent(listacomps_GRANT[i])).enabled:=true;


     if usuario='' then
       raise Exception.Create('Error. Debe informar el usuario para obtener sus permisos.')
     else
     begin
       listacomps_GRANT:=tstringlist.Create;
       listacomps_GRANT:=lee_claves_ini(ejecutable_ini,'GRANT_'+usuario);
       listacomps_GRANT.AddStrings(lee_claves_ini(ejecutable_ini,'GRANT_ALL'));
       for i:=0 to listacomps_GRANT.Count-1 do
         if formulario.FindComponent(listacomps_GRANT[i]) is TCustomControl then
         begin
           tcontrol(formulario.findcomponent(listacomps_GRANT[i])).enabled:=true;
           tcontrol(formulario.FindComponent(listacomps_GRANT[i])).Visible:=true;
         end;

     end;
   end
   ELSE
   BEGIN
     listacomps_DENY:=tstringlist.Create;

     listacomps_DENY:=lee_claves_ini(ejecutable_ini,'DENY_'+usuario);
     listacomps_DENY.AddStrings(lee_claves_ini(ejecutable_ini,'DENY_ALL'));
     for i:=0 to listacomps_DENY.Count-1 do
     if formulario.FindComponent(listacomps_DENY[i]) is TCustomControl then
     begin
       tcontrol(formulario.findcomponent(listacomps_DENY[i])).enabled:=FALSE;
    //   tcontrol(formulario.FindComponent(listacomps_GRANT[i])).Visible:=true;
     end;
   END;
end;
procedure carga_permisos_ini(formulario:tform;sender:tobject; usuario:string);
var
    claves:tstrings;
    a:integer;
    variable:ansistring;
begin

     claves:=tstringlist.create;
     claves:=lee_claves_ini(ejecutable_ini,usuario);
     for a:=0 to claves.Count-1 do
     begin
          lee_dato_ini(ejecutable_ini,usuario,claves[a],variable);
          variable:=uppercase(variable);
          if variable='TEDIT' then
             TEDIT(formulario.FindComponent(claves[a])).enabled:=true
          else
          if variable='TDBEDIT' then
             Tdbedit(formulario.FindComponent(claves[a])).enabled:=true
          ELSE
          if variable='TBUTTON' then
             Tbutton(formulario.FindComponent(claves[a])).enabled:=true
          else
          if variable='TBITBTN' then
             Tbitbtn(formulario.FindComponent(claves[a])).enabled:=true
          else
          if variable='TCOMBOBOX' then
             Tcombobox(formulario.FindComponent(claves[a])).enabled:=true
          ELSE
          if variable='TCXCURRENCYEDIT' then
             tcxcurrencyedit(formulario.FindComponent(claves[a])).enabled:=true
          ELSE
          if variable='TDBDATEEDIT' then
             Tcxdbdateedit(formulario.FindComponent(claves[a])).enabled:=true
          ELSE
          if variable='TDBLOOKUPCOMBOBOX' then
             Tdblookupcombobox(formulario.FindComponent(claves[a])).enabled:=true
          ELSE
          if variable='TSPEEDBUTTON' then
             TSpeedbutton(formulario.FindComponent(claves[a])).enabled:=true
          else
          if variable='TDBCHECKBOX' then
             tdbcheckbox (formulario.FindComponent(claves[a])).enabled:=true
          else
          if variable='Ttabsheet' then
             ttabsheet (formulario.FindComponent(claves[a])).TabVisible:=true

       {   if variable='TTABSHEET' THEN
             ttabsheet(form1.Findcomponent(claves[a])).visible:=true;}
     end;
end;


function tpermisos.carga_permisos_usuario:boolean;
begin
  carga_permisos_usuario(application.MainForm,self.usuario);
end;


procedure tpermisos.set_todos_los_permisos;
var i:integer;
begin
  for i:=0 to application.ComponentCount-1 do
  begin
    if (application.Components[i]) is tcontrol then
    begin
      tcontrol(application.Components[i]).enabled:=true;
      tcontrol(application.Components[i]).Visible:=true;

    end;
  end;
end;

procedure tpermisos.set_ningun_permiso(form:tform);
var i:integer;
begin
  for i:=0 to form.ComponentCount-1 do

    if ((form.Components[i].tostring='tcxcontrol') or (form.Components[i] is tcontrol)) then
    if not ((form.components[i].ToString ='TcxPageControl') or
            (form.Components[i].ToString = 'TcxtabSheet') or
            (form.components[i].ToString='TForm')                                           {
                                            or
                                                        (form.Components[i] is TcxGridDBTableView) or
                                                        (form.Components[i] is TcxGridLevel) or
                                                        (form.components[i] is TdxSkinController)
                                          }
            ) then
    begin
      if tcontrol(form.components[i]).Tag<>-1 then

      tcontrol(form.Components[i]).enabled:=false;
     // tcontrol(form.Components[i]).Visible:=true;
    end;

end;

function GetDirectorioSistema(Folder: Integer; CanCreate: Boolean): string;

// Gets path of special system folders
//
// Call this routine as follows:
// GetSpecialFolderPath (CSIDL_PERSONAL, false)
//        returns folder as result
//
 { CSIDL_DESKTOP                       = $0000; { <desktop> }
 { CSIDL_INTERNET                      = $0001; { Internet Explorer (icon on desktop) }
 { CSIDL_PROGRAMS                      = $0002; { Start Menu\Programs }
 { CSIDL_CONTROLS                      = $0003; { My Computer\Control Panel }
  {CSIDL_PRINTERS                      = $0004; { My Computer\Printers }
  {CSIDL_PERSONAL                      = $0005; { My Documents.  This is equivalent to CSIDL_MYDOCUMENTS in XP and above }
  {CSIDL_FAVORITES                     = $0006; { <user name>\Favorites }
  {CSIDL_STARTUP                       = $0007; { Start Menu\Programs\Startup }
  {CSIDL_RECENT                        = $0008; { <user name>\Recent }
  {CSIDL_SENDTO                        = $0009; { <user name>\SendTo }
  {CSIDL_BITBUCKET                     = $000a; { <desktop>\Recycle Bin }
  {CSIDL_STARTMENU                     = $000b; { <user name>\Start Menu }
  {CSIDL_MYDOCUMENTS                   = $000c; { logical "My Documents" desktop icon }
  {CSIDL_MYMUSIC                       = $000d; { "My Music" folder }
  {CSIDL_MYVIDEO                       = $000e; { "My Video" folder }
  {CSIDL_DESKTOPDIRECTORY              = $0010; { <user name>\Desktop }
  {CSIDL_DRIVES                        = $0011; { My Computer }
  {CSIDL_NETWORK                       = $0012; { Network Neighborhood (My Network Places) }
  {CSIDL_NETHOOD                       = $0013; { <user name>\nethood }
  {CSIDL_FONTS                         = $0014; { windows\fonts }
  {CSIDL_TEMPLATES                     = $0015;
  {CSIDL_COMMON_STARTMENU              = $0016; { All Users\Start Menu }
  {CSIDL_COMMON_PROGRAMS               = $0017; { All Users\Start Menu\Programs }
  {CSIDL_COMMON_STARTUP                = $0018; { All Users\Startup }
  {CSIDL_COMMON_DESKTOPDIRECTORY       = $0019; { All Users\Desktop }
  {CSIDL_APPDATA                       = $001a; { <user name>\Application Data }
  {CSIDL_PRINTHOOD                     = $001b; { <user name>\PrintHood }
  {CSIDL_LOCAL_APPDATA                 = $001c; { <user name>\Local Settings\Application Data (non roaming) }
  {CSIDL_ALTSTARTUP                    = $001d; { non localized startup }
  {CSIDL_COMMON_ALTSTARTUP             = $001e; { non localized common startup }
  {CSIDL_COMMON_FAVORITES              = $001f;
  {CSIDL_INTERNET_CACHE                = $0020;
  {CSIDL_COOKIES                       = $0021;
  {CSIDL_HISTORY                       = $0022;
  {CSIDL_COMMON_APPDATA                = $0023; { All Users\Application Data }
  {CSIDL_WINDOWS                       = $0024; { GetWindowsDirectory() }
  {CSIDL_SYSTEM                        = $0025; { GetSystemDirectory() }
  {CSIDL_PROGRAM_FILES                 = $0026; { C:\Program Files }
  {CSIDL_MYPICTURES                    = $0027; { C:\Program Files\My Pictures }
  {CSIDL_PROFILE                       = $0028; { USERPROFILE }
  {CSIDL_SYSTEMX86                     = $0029; { x86 system directory on RISC }
  {CSIDL_PROGRAM_FILESX86              = $002a; { x86 C:\Program Files on RISC }
  {CSIDL_PROGRAM_FILES_COMMON          = $002b; { C:\Program Files\Common }
  {CSIDL_PROGRAM_FILES_COMMONX86       = $002c; { x86 C:\Program Files\Common on RISC }
  {CSIDL_COMMON_TEMPLATES              = $002d; { All Users\Templates }
  {CSIDL_COMMON_DOCUMENTS              = $002e; { All Users\Documents }
  {CSIDL_COMMON_ADMINTOOLS             = $002f; { All Users\Start Menu\Programs\Administrative Tools }
  {CSIDL_ADMINTOOLS                    = $0030; { <user name>\Start Menu\Programs\Administrative Tools }
  {CSIDL_CONNECTIONS                   = $0031; { Network and Dial-up Connections }
  {CSIDL_COMMON_MUSIC                  = $0035; { All Users\My Music }
  {CSIDL_COMMON_PICTURES               = $0036; { All Users\My Pictures }
  {CSIDL_COMMON_VIDEO                  = $0037; { All Users\My Video }
  {CSIDL_RESOURCES                     = $0038; { Resource Directory }
  {CSIDL_RESOURCES_LOCALIZED           = $0039; { Localized Resource Directory }
  {CSIDL_COMMON_OEM_LINKS              = $003a; { Links to All Users OEM specific apps }
  {CSIDL_CDBURN_AREA                   = $003b; { USERPROFILE\Local Settings\Application Data\Microsoft\CD Burning }
  {CSIDL_COMPUTERSNEARME               = $003d; { Computers Near Me (computered from Workgroup membership) }
  {CSIDL_PROFILES                      = $003e; �}

var
   FilePath: array [0..255] of char;

begin
 SHGetSpecialFolderPath(0, @FilePath[0], FOLDER, CanCreate);
 Result := FilePath;
end;


function  GetAppVersion:string;
   var
    Size, Size2: DWord;
    Pt, Pt2: Pointer;
   begin
     Size := GetFileVersionInfoSize(PChar (ParamStr (0)), Size2);
     if Size > 0 then
     begin
       GetMem (Pt, Size);
       try
          GetFileVersionInfo (PChar (ParamStr (0)), 0, Size, Pt);
          VerQueryValue (Pt, '\', Pt2, Size2);
          with TVSFixedFileInfo (Pt2^) do
          begin
            Result:= IntToStr (HiWord (dwFileVersionMS)) + '.' +
                     IntToStr (LoWord (dwFileVersionMS)) + '.' +
                     IntToStr (HiWord (dwFileVersionLS)) + '.' +
                     IntToStr (LoWord (dwFileVersionLS));
         end;
       finally
         FreeMem (Pt);
       end;
     end;
end;


function ejecutable_nombre:ansistring;
begin
     result:=changefileext(application.exeName,'')

end;   

function Get_File_Size(sFileToExamine: ansistring): integer;
{
 for some reason both methods of finding file size return
 a filesize that is slightly larger than what Windows File
 Explorer reports
}
var
  FileHandle: THandle;
  FileSize: LongWord;
begin
  //a- Get file size
  FileHandle := CreateFile(PChar(sFileToExamine),
    GENERIC_READ,
    0, {exclusive}
    nil, {security}
    OPEN_EXISTING,
    FILE_ATTRIBUTE_NORMAL,
    0);
    result:=0;
  FileSize   := GetFileSize(FileHandle, nil);
  Result     := round((FileSize/1024));
  CloseHandle(FileHandle);

end;



function lee_claves_ini (ficheroini:ansistring; seccion:ansistring):tstrings;
var
    inifile:tinifile;
    res:tstrings;
begin
     res:=tstringlist.create;
     res.Clear;
     inifile:=tinifile.Create(ficheroini);
     if inifile.SectionExists(seccion) then
      inifile.ReadSection(seccion, res);
     inifile.Free;
     lee_claves_ini:=res;
end;

function lee_claves_valor(ficheroini:ansistring; seccion:ansistring):tstrings;
var
    inifile:tinifile;
    res:tstrings;
begin
  res:=tstringlist.create;
  res.Clear;
  inifile:=tinifile.Create(ficheroini);
  if inifile.SectionExists(seccion) then
    inifile.ReadSectionValues(seccion, res);
  inifile.Free;
  lee_claves_valor:=res;
end;

function lee_secciones_ini (ficheroini:ansistring):tstrings;
var
    inifile:tinifile;
    res:tstrings;
begin
  res:=tstringlist.create;
  res.Clear;
  inifile:=tinifile.Create(ficheroini);
  inifile.readSections(res);
  lee_secciones_ini:=res;
end;

function FindAll (const Path: String; Attr: Integer):  TStrings ;
var
   Res: TSearchRec;
   EOFound: Boolean;
   lista:Tstrings;
begin
   lista:=tstringlist.create;
   lista.Clear;
   EOFound:= False;
   if FindFirst(Path, Attr, Res) < 0 then
     exit
   else
     while not EOFound do begin
       if res.Name<>'' then
       lista.Add(Res.Name) ;
       EOFound:= FindNext(Res) <> 0;
     end;
   FindClose(Res) ;
   findall:=lista;
end;

function listado_ficheros(ruta:ansistring;mascara:ansistring):tstrings;
var res:Tstrings;
begin
  res:=tstringlist.Create;
  res.AddStrings( TArray<string>(TDirectory.GetFiles(ruta, mascara) ) );
  result:=res
end;

function Carga_Empresas:tstrings;
var f:textfile;
    linea:string;
    empres:tstrings;
begin
     empres:=tstringlist.create;
     empres.clear;
     AssignFile(F,'c:\archivos de programa\ccs\g733\datos_comunes\empresas.txt');
     reset(F);
     while not eof(f) do
      begin
           readln(f,linea);
           empres.Add(linea);
      end;
     closefile(f);
     result:=empres;
end;


function carga_anios:tstrings;
var a:integer;
    resultado:tstrings;
begin
     resultado:=tstringlist.create;
     resultado.Clear;
     a:=CurrentYear;
     resultado.add(inttostr(a-2));
     resultado.add(inttostr(a-1));
     resultado.add(inttostr(a));
     resultado.add(inttostr(a+1));
     result:=resultado;

end;

function eliminar_seccion_ini(ficheroini:ansistring; seccion:ansistring):boolean;
var inifile:tinifile;
begin

     result:=true;
     inifile:=tinifile.Create(ficheroini);
     if inifile.SectionExists(seccion) then
      try
          IniFile.EraseSection(seccion);
      except
          result:=false;
      end
     else
       result:=false;
       inifile.Free;
end;

function eliminar_clave_ini(ficheroini:ansistring; seccion:ansistring; clave:ansistring):boolean;
var inifile:tinifile;
begin
     result:=true;
     inifile:=tinifile.Create(ficheroini);
     if inifile.ValueExists(seccion, clave) then
      try
          IniFile.DeleteKey(seccion,clave);
      except
          result:=false;
      end
     else
       result:=false;
       inifile.Free;
end;

function crea_dato_ini(ficheroini:ansistring; seccion:ansistring; clave:ansistring; dato:ansistring):boolean;
var inifile:tinifile;
begin
     result:=true;
     inifile:=tinifile.Create(ficheroini);
     if not inifile.ValueExists(seccion, clave) then
      try
          IniFile.writeString(seccion,clave,dato);
      except
          result:=false;
      end
     else
       result:=false;
     inifile.Free;
end;


function crea_dato_ini(ficheroini:ansistring; seccion:ansistring; clave:ansistring; dato:tdatetime):boolean;
var inifile:tinifile;
begin
     result:=true;
     inifile:=tinifile.Create(ficheroini);
     if not inifile.ValueExists(seccion, clave) then
      try
          IniFile.writedatetime(seccion,clave,dato);
      except
          result:=false;
      end
     else
       result:=false;
     inifile.Free;
end;

function crea_dato_ini(ficheroini:ansistring; seccion:ansistring; clave:ansistring; dato:integer):boolean;overload;
var inifile:tinifile;
begin
     result:=true;
     inifile:=tinifile.Create(ficheroini);
     if not inifile.ValueExists(seccion, clave) then
      try
          IniFile.writeinteger(seccion,clave,dato);
      except
          result:=false;
      end
     else
       result:=false;
     inifile.Free;
end;


function modifica_dato_ini (ficheroini:ansistring; seccion:ansistring; clave:ansistring; dato:ansistring):boolean;
var inifile:tinifile;
begin
     result:=true;
     inifile:=tinifile.Create(ficheroini);
     if inifile.ValueExists(seccion, clave) then
      try
          IniFile.writeString(seccion,clave,dato);
      except
          result:=false;
      end
     else
       result:=false;
     inifile.Free;
end;

function modifica_dato_ini (ficheroini:ansistring; seccion:ansistring; clave:ansistring; dato:integer):boolean;
var inifile:tinifile;
begin
     result:=true;
     inifile:=tinifile.Create(ficheroini);
     if inifile.ValueExists(seccion, clave) then
      try
          IniFile.writeinteger(seccion,clave,dato);
      except
          result:=false;
      end
     else
       result:=false;
     inifile.Free;
end;

function modifica_dato_ini (ficheroini:ansistring; seccion:ansistring; clave:ansistring; dato:tdatetime):boolean;
var inifile:tinifile;
begin
     result:=true;
     inifile:=tinifile.Create(ficheroini);
     if inifile.ValueExists(seccion, clave) then
      try
          IniFile.writedatetime(seccion,clave,dato);
      except
          result:=false;
      end
     else
       result:=false;
     inifile.Free;
end;

function esta_en_la_lista(elemento:ansistring;lista:tstrings):boolean;
var i:integer;
begin
     i:=0;
     result:=false;
     repeat
      if elemento=lista[i] then result:=true;
      i:=i+1;
     until (result=true) or (i=lista.count);
end;
//function estan_en_la_lista(elementos:tstrings;lista:tstrings):tstrings;



function cambia_a_tstrings(widestr:widestring;separador:ansistring):Tstrings;
var a:integer;
    indice:integer;
    peo:tstrings;
    cadena:string;
begin
     indice:=1;
     peo:=tstringlist.Create;
     peo.clear;
     cadena:=widestr;

     while pos(separador,cadena)<>0 do
     begin
     cadena:=TrimLeft(cadena);
     indice:=pos(separador, cadena);
     peo.Add(copy(cadena,1,indice-1));
     delete(cadena,1,indice);
     indice:=indice+1;
     end;
     cadena:=trimleft(cadena);
     peo.add(cadena);
     result:=peo;
end;


function cambia_a_wide(testrings:Tstrings;separador:ansistring):widestring;
var a:integer;
    resultado:widestring;
begin
     resultado:='';
     if testrings.Count>0 then
     begin
          resultado:=testrings[0];
          for a:=1 to testrings.Count-1 do
          resultado:=resultado+separador+testrings[a];
          result:=resultado;
     end
     else
     result:='';

end;

{function reemplaza(char_in: char; char_out: char; cadena:ansistring):ansistring;
var a:integer;
begin
     if length(cadena)>0 then
     for a:=0 to length(cadena)-1 do
       if cadena[a]=pwidechar(char_in) then cadena[a]:=pwidechar(char_out);
     result:=cadena;
end;     }

function lee_dato_ini (ficheroini:ansistring; seccion:ansistring; clave:ansistring; var vardestino:ANSIstring):boolean;
var inifile:tinifile;
begin
     result:=true;
     inifile:=tinifile.Create(ficheroini);
     if inifile.ValueExists(seccion, clave) then
     begin
          vardestino:=IniFile.ReadString(seccion,clave,'_NULL');
          if vardestino='_NULL' then result:=false;
     end
     else
       result:=false;
     inifile.Free;
end;

function lee_dato_ini (ficheroini:ansistring; seccion:ansistring; clave:ansistring; var vardestino:integer):boolean;
var inifile:tinifile;
begin
     result:=true;
     inifile:=tinifile.Create(ficheroini);
     if inifile.ValueExists(seccion, clave) then
     begin
          vardestino:= IniFile.readinteger(seccion,clave,-9999);
          if vardestino=-9999 then result:=false;
     end
     else
        result:=false;
     inifile.Free;
end;

function lee_dato_ini (ficheroini:ansistring; seccion:ansistring; clave:ansistring; var vardestino:tdatetime):boolean;
var inifile:tinifile;
fechastr:ansistring;
begin
     result:=true;
     inifile:=tinifile.Create(ficheroini);
     if inifile.ValueExists(seccion, clave) then
     begin
          fechastr:=IniFile.readstring(seccion,clave,'');
          if dateseparator='/' then
          fechastr:=StringReplace(fechastr,'-',dateseparator,[rfreplaceall])
          else
          fechastr:=StringReplace(fechastr,'/',dateseparator,[rfreplaceall]);
          try
             if fechastr<>datetimetostr(strtodatetime(fechastr)) then result:=false;
          except
             result:=false;
          end;
          if fechastr='' then result:=false;
     end
     else
        result:=false;
     if result then vardestino:=strtodatetime(fechastr);
     inifile.Free;
end;

function carga_ficherowide(nombre_fichero:ansistring;desencriptar:boolean):widestring;
var  f:textfile;
     linea:widestring;
     linea_resultado:widestring;
begin
     try
        assignfile(f,nombre_fichero);
        reset(f);
        while not eof(f) do
        begin
             readln(f,linea);
             linea_resultado:=linea_resultado +#13#10+ linea;
        end;
        closefile(f);
        result:=linea_resultado;
     except;
        linea_resultado:='_NULL';
     end;
     result:=linea_resultado;
end;


function carga_fichero(nombre_fichero:ansistring;desencriptar:boolean):tstrings;
var  f:textfile;
     linea:widestring;
     contenido:tstrings;
begin
     contenido:=tstringlist.create;
     contenido.Clear;
     try
        assignfile(f,nombre_fichero);
        reset(f);
        while not eof(f) do
        begin
             readln(f,linea);
             if desencriptar then
                linea:=desencripta(linea,1499);
             contenido.add(linea);
        end;
        closefile(f);
        result:=contenido;
     except
        contenido.add('_NULL');
        result:=contenido;
     end;
end;


procedure ficherodropped(var Msg: TMessage);
{
The DragQueryFile function retrieves the filenames of dropped files.

UINT DragQueryFile(
    HDROP hDrop,	// handle to structure for dropped files
    UINT iFile,	// index of file to query
    LPTSTR lpszFile,	// buffer for returned filename
    UINT cch 	// size of buffer for filename
   );

Parameters

hDrop
Identifies the structure containing the filenames of the dropped files.

iFile
Specifies the index of the file to query. If the value of the iFile parameter is
0xFFFFFFFF, DragQueryFile returns a count of the files dropped. If the value of
the iFile parameter is between zero and the total number of files dropped,
DragQueryFile copies the filename with the corresponding value to the buffer
pointed to by the lpszFile parameter.

lpszFile
Points to a buffer to receive the filename of a dropped file when the function
returns. This filename is a null-terminated string. If this parameter is NULL,
DragQueryFile returns the required size, in characters, of the buffer.

cch
Specifies the size, in characters, of the lpszFile buffer.

Return Values

When the function copies a filename to the buffer, the return value is a count
of the characters copied, not including the terminating null character.
If the index value is 0xFFFFFFFF, the return value is a count of the dropped
files.
If the index value is between zero and the total number of dropped files and the
lpszFile buffer address is NULL, the return value is the required size, in
characters, of the buffer, not including the terminating null character.
}

var
  hDrop : Integer;
  fName : ansistring;
  NFiles : Cardinal;
  FileCount : Cardinal;

begin
  if not assigned(ficheros_droppeados) then ficheros_droppeados:=Tstringlist.Create;
  ficheros_droppeados.Clear;

  hDrop := Msg.WParam;
  SetLength(fName, 255); // prepare buffer

  try
    // request number of files dropped
    NFiles := DragQueryFile(hDrop, $FFFFFFFF, PChar(fName), 254);
    for FileCount:=0 to pred(NFiles) do begin
      // request a filename at index FileCount
      DragQueryFile(hDrop, FileCount, PChar(fName), 254);
      // copy until NULL char is found
      ficheros_droppeados.Add(Copy(fName, 1, pred(Pos(#0, fName))));
    end;

    // report number of files dropped and their filenames
  finally
    // end drag
    DragFinish(hDrop);
  end;

end;

function get_fecha_fichero_modificado(const filename):tdatetime;
var fecha_creado,fecha_modificado,fecha_acceso:tdatetime;
begin
     GetFileTimes(pchar(filename), fecha_creado,fecha_acceso, fecha_modificado);
     result:=fecha_modificado;

end;


function GetFileTimes(const FileName: ansistring; var Created: TDateTime;
var Accessed: TDateTime; var Modified: TDateTime): Boolean;
var
  h: THandle;
  Info1, Info2, Info3: TFileTime;
  SysTimeStruct: SYSTEMTIME;
  TimeZoneInfo: TTimeZoneInformation;
 bias: Double;
begin
  Result := False;
  bias   := 0;
  h      := FileOpen(FileName, fmOpenRead or fmShareDenyNone);
  if h > 0 then
  begin
    try
      bias:=0;
      case GetTimeZoneInformation(TimeZoneInfo) of
    TIME_ZONE_ID_STANDARD:
      bias := (TimeZoneInfo.Bias / 60 / 24);
    TIME_ZONE_ID_DAYLIGHT:
      bias :=  ((TimeZoneInfo.Bias + TimeZoneInfo.DaylightBias) / 60 / 24);

      end;
      GetFileTime(h, @Info1, @Info2, @Info3);
      if FileTimeToSystemTime(Info1, SysTimeStruct) then
        Created := SystemTimeToDateTime(SysTimeStruct) - bias;
      if FileTimeToSystemTime(Info2, SysTimeStruct) then
        Accessed := SystemTimeToDateTime(SysTimeStruct) - bias;
      if FileTimeToSystemTime(Info3, SysTimeStruct) then
        Modified := SystemTimeToDateTime(SysTimeStruct) - bias;
      Result := True;
    finally
      FileClose(h);
    end;
  end;
end;

{
 function GetFileVersion(const FileName: TFileName;
     var Major, Minor, Release, Build: word): boolean;
  // Returns True on success and False on failure.
  var
    size, len: longword;
    handle: THandle;
    buffer: pchar;
    pinfo: ^VS_FIXEDFILEINFO;
    prueba:string;
  begin

    Result := False;
    size := GetFileVersionInfoSize(pwidechar(prueba),handle);
/7    (pwidechar(FileName),handle);
//    (pwidechar(FileName), handle);
    if size > 0 then begin
      GetMem(buffer, size);
      if GetFileVersionInfo(Pointer(FileName), 0, size, buffer)
      then
        if VerQueryValue(buffer, '\', pointer(pinfo), len) then begin
          Major   := HiWord(pinfo.dwFileVersionMS);
          Minor   := LoWord(pinfo.dwFileVersionMS);
          Release := HiWord(pinfo.dwFileVersionLS);
          Build   := LoWord(pinfo.dwFileVersionLS);
          Result  := True;
        end;
      FreeMem(buffer);
    end;
  end;
}

end.
