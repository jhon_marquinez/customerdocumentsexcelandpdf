unit TDocExcel_ftp;

interface

uses StdCtrls, adodb, db, Classes, SysUtils, windows, dialogs, Forms, DateUtils,
  frxDBSet, frxClass, frxExportPDF, frxExportXLS, regularexpressions, uitypes,UExcelAdapter, XLSAdapter, UCustomFlexCelReport,
  UFlexCelReport;

type
  Select = (s_pedidos_cab, s_pedidos_lin, s_facturas_cab, s_facturas_lin, s_albaranes_cab, s_albaranes_lin);
  updates = (u_doc_imp);
  sp = (sp_facturas_cab_temp_oo);

  arr_slt = array [Select] of widestring;
  arr_upd = array [updates] of widestring;
  arr_sp = array [sp] of widestring;

  Timp_docs = class(Tobject)
  private
    { Declaraciones privadas de la clase }
    sCliIni: string;
    sClifin: string;
    SDocIni: string;
    sDocFin: string;
    sFechaIni: string;
    sFechaFin: String;
    sReimp: string; // -1:Reimpresi�n  ; 0-sin reimpresi�n  ; 1- Todos
    sPathToSave: string;
    bcompleto:boolean;
    adoconnection_imp: Tadoconnection;
//    frxReport_imp: TfrxReport;
    FlexCelReport_Docs: TFlexCelReport;
    datasource_cab, datasource_lin: Tdatasource;
    datasource1, datasource2: Tdatasource;
    cons_select: arr_slt;
    // cons_upd:  arr_upd;
    cons_sp: arr_sp;
    procedure SetCliIni(valor: String);
    procedure SetCliFin(valor: String);
    procedure SetReimp(valor: String);

  public
    property CliIni: String read sCliIni write SetCliIni;
    property CliFin: String read sClifin write SetCliFin;
    property DocIni: String read SDocIni write SDocIni;
    property DocFin: String read sDocFin write sDocFin;
    property FechaIni: String read sFechaIni write sFechaIni;
    property FechaFin: String read sFechaFin write sFechaFin;
    property Reimp: String read sReimp write SetReimp;
    property PathToSave: String read sPathToSave write SPathToSave;
    property Conexion: Tadoconnection read adoconnection_imp
      write adoconnection_imp;
    property completo: boolean read bcompleto write bcompleto;

  public
    { Declaraciones p�blicas de la clase }
    constructor create; overload;
    constructor create(cli_ini, cli_fin, doc_ini, doc_fin, fecha_ini, fecha_fin: string; Reimp: string; sPathToSave: string); overload;
    procedure carga_consultas_array();
    procedure crea_directorios();
    procedure crear_excel_documento(cliente_id, nom_cli, documento, fecha_doc:string; var FlexCelReport_Docs:TFlexCelReport; adodts_consumo:Tadodataset; tipo_doc:string; adodataset_cab:tadodataset);


  private
    procedure FilenamePDF(var NomArchTemp: string);

  end;

type
  Timp_Factura_Excel = Class(Timp_docs)
  private
    sCicloIni: string;
    sCicloFin: string;
    sContabilizado: string;
    sIdent_TablaTemp: string;

  public
    property CicloIni: String read sCicloIni write sCicloIni;
    property CicloFin: String read sCicloFin write sCicloFin;
    property Contabilizado: String read sContabilizado write sContabilizado;
    property Ident_TablaTemp: String read sIdent_TablaTemp
      write sIdent_TablaTemp;

    constructor create(cli_ini, cli_fin, doc_ini, doc_fin, fecha_ini, fecha_fin,
      ciclo_ini, ciclo_fin: string; Reimp, contab: string); overload;

    function crea_tabla_CabTemp(var adocommand_temp: Tadocommand): boolean;
    function crea_cab(var adodataset_cab: Tadodataset; xnumdoc_id:string): boolean;
    function crea_lin(var adodts_consumo: Tadodataset; orden_lin: String): boolean;
    function crea_factura( orden: String; var FlexCelReport_Docs:TFlexCelReport; var XLSAdapter1:TXLSAdapter;var adodts_consumo: TADODataSet): boolean;

  private
    procedure Obtener_Indent(var NomArchTemp: string);

  end;

type
  Timp_pedido = class(Timp_docs)
  private
    sBis: boolean;
    sSc: boolean;

  public
    procedure setBis(valor: boolean);
    procedure setSC(valor: boolean);

    property bis: boolean read sBis write setBis;
    property sc: boolean read sSc write setSC;

    constructor create(cli_ini, cli_fin, doc_ini, doc_fin, fecha_ini, fecha_fin,
      Reimp: string; bis, sc: boolean); overload;

    function crea_pedido(copia_destino: string; var FlexCelReport_Docs:TFlexCelReport; var XLSAdapter1:TXLSAdapter;var adodts_consumo: TADODataSet): boolean;
   end;

type
  Timp_Albaran_Excel = class(Timp_docs)
  private

  public
    function crea_albaran(copia_destino: string; var FlexCelReport_Docs:TFlexCelReport; var XLSAdapter1:TXLSAdapter;var adodts_consumo: TADODataSet): boolean;

  end;

implementation

// uses base, bdalfa, general, encripter, email;
uses base, bdalfa, general, Hashes;

constructor Timp_docs.create;
begin
  sCliIni := '';
  sClifin := '';
  SDocIni := '';
  sDocFin := '';
  sFechaIni := '';
  sFechaFin := '';
  sReimp := '-1';
  sPathToSave:= '';

end;

constructor Timp_docs.create(cli_ini, cli_fin, doc_ini, doc_fin, fecha_ini,
  fecha_fin: string; Reimp: string; sPathToSave: string);
begin

  SetCliIni(cli_ini);
  SetCliFin(cli_fin);
  SDocIni := SDocIni;
  sDocFin := sDocFin;
  sFechaIni := (sFechaIni);
  sFechaFin := (sFechaFin);
  SetReimp(Reimp);
  SPathToSave := sPathToSave;

end;

constructor Timp_Factura_Excel.create(cli_ini, cli_fin, doc_ini, doc_fin, fecha_ini,
  fecha_fin, ciclo_ini, ciclo_fin: string; Reimp, contab: string);
begin
  SetCliIni(cli_ini);
  SetCliFin(cli_fin);
  SDocIni := (SDocIni);
  sDocFin := (sDocFin);
  sFechaIni := (sFechaIni);
  sFechaFin := (sFechaFin);
  sCicloIni := (ciclo_ini);
  sCicloFin := (ciclo_fin);
  SetReimp(Reimp);
  sContabilizado := (contab);

end;

Procedure Timp_Factura_Excel.Obtener_Indent(var NomArchTemp: string);
begin

  FilenamePDF(NomArchTemp);
  sIdent_TablaTemp := '_' + NomArchTemp;

end;





procedure Timp_docs.FilenamePDF(var NomArchTemp: string);
var
  HORA, MINUTS, SEG, MSEG: word;
begin

  DecodeTime(Now, HORA, MINUTS, SEG, MSEG);
  NomArchTemp := FORMAT('%.2D%.2D%.2D%.2D', [HORA, MINUTS, SEG, MSEG]);

end;

procedure Timp_docs.carga_consultas_array();
begin
  // Cargar las consultas en array.
  cons_select[s_pedidos_cab] := retorna_consulta_ini
    ('c:\Program Files\ccs\g733\class\timp_doc.ini', 'sql_select',
    's_pedidos_cab');
  cons_select[s_pedidos_lin] := retorna_consulta_ini
    ('c:\Program Files\ccs\g733\class\timp_doc.ini', 'sql_select',
    's_pedidos_lin');
  cons_select[s_albaranes_cab] := retorna_consulta_ini
    ('c:\Program Files\ccs\g733\class\timp_doc.ini', 'sql_select',
    's_albaranes_cab');
  cons_select[s_albaranes_lin] := retorna_consulta_ini
    ('c:\Program Files\ccs\g733\class\timp_doc.ini', 'sql_select',
    's_albaranes_lin');
  cons_select[s_facturas_cab] := retorna_consulta_ini
    ('c:\Program Files\ccs\g733\class\timp_doc.ini', 'sql_select',
    's_facturas_cab');
  cons_select[s_facturas_lin] := retorna_consulta_ini
    ('c:\Program Files\ccs\g733\class\timp_doc.ini', 'sql_select',
    's_facturas_lin');
  cons_sp[sp_facturas_cab_temp_oo] :=
    retorna_consulta_ini('c:\Program Files\ccs\g733\class\timp_doc.ini', 'sp',
    'sp_facturas_cab_temp_oo');
end;

procedure Timp_docs.crea_directorios();
var
  ruta_ficheros_local: ansistring;
begin

  if not DirectoryExists(PathToSave) then
      CreateDir(PathToSave);

  if not DirectoryExists(PathToSave + CliIni + '\1_Pedidos') then
      CreateDir(PathToSave + CliIni + '\1_Pedidos');

  if not DirectoryExists(PathToSave + CliIni + '\2_Albaranes') then
      CreateDir(PathToSave + CliIni + '\2_Albaranes');

  if not DirectoryExists(PathToSave + CliIni + '\3_Facturas') then
      CreateDir(PathToSave + CliIni + '\3_Facturas');

  if not DirectoryExists(PathToSave + CliIni + '\4_Pagos Pendientes') then
      CreateDir(PathToSave + CliIni + '\4_Pagos Pendientes');

  if  not DirectoryExists(PathToSave + CliIni + '\5_Articulos Comprados') then
      CreateDir(PathToSave + CliIni + '\5_Articulos Comprados');
end;

function Timp_Factura_Excel.crea_tabla_CabTemp(var adocommand_temp
  : Tadocommand): boolean;
begin

  adocommand_temp.CommandText := '';
  adocommand_temp.ParamCheck := true;
  adocommand_temp.CommandText := cons_sp[sp_facturas_cab_temp_oo];
  // adocommand_temp.CommandText:= retorna_consulta_ini('c:\Program Files\ccs\g733\class\timp_doc.ini','sp', 'sp_facturas_cab_temp_oo');
  adocommand_temp.Parameters.Refresh;

  with adocommand_temp do
  begin
    Parameters[0].Value := sIdent_TablaTemp;
    Parameters[1].Value := sFechaIni;
    Parameters[2].Value := sFechaFin;
    Parameters[3].Value := sCicloIni;
    //Parameters[4].Value := sCicloFin;
    if (SDocIni = '') then
      Parameters[4].Value := ('0')
    else
      Parameters[4].Value := (SDocIni);
    if (sDocFin = '') then
      Parameters[5].Value := ('9999999')
    else
      Parameters[5].Value := (sDocFin);
    Parameters[6].Value := (sCliIni);
    Parameters[7].Value := (sClifin);
    Parameters[8].Value := (sContabilizado);
    Parameters[9].Value := (sReimp);
    parameters[10].value:= '-1';
  end;

  try
    begin
      adocommand_temp.Execute;
      Result := true;
    end;
  except
    on E: Exception do
      Result := false;
  end;
end;

function Timp_Factura_Excel.crea_cab(var adodataset_cab: Tadodataset; xnumdoc_id:string ): boolean;
begin

  adodataset_cab.CommandText := '';
  adodataset_cab.ParamCheck := true;
  adodataset_cab.CommandText := cons_select[s_facturas_cab];
  with adodataset_cab do
  begin
    Parameters[0].Value := sIdent_TablaTemp;
    Parameters[1].Value := xnumdoc_id; // Doc
    Parameters[2].Value := '1'; // ident orden de linea.
  end;

  try
    begin
      adodataset_cab.active := true;
      Result := true;
    end;
  except
    on E: Exception do
      Result := false;
  end;

end;

function Timp_Factura_Excel.crea_lin(var adodts_consumo: Tadodataset; orden_lin: String): boolean;
begin
  if adodts_consumo.Active then adodts_consumo.Active:=false;

  adodts_consumo.CommandText := '';
  adodts_consumo.ParamCheck := true;
  adodts_consumo.CommandText := cons_select[s_facturas_lin];
  with adodts_consumo do
  begin
    Parameters[0].Value := (sIdent_TablaTemp);
    Parameters[1].Value := ('0');
    Parameters[2].Value := (orden_lin);
    /// pq solo queremos una copia de la factura aunque en la ficha de factura el cliente tenga informada varias
   // Parameters[3].Value := '1';
  end;

  try
    begin
      adodts_consumo.active := true;
      Result := true;
    end;
  except
    on E: Exception do
      Result := false;
  end;

end;

function Timp_Factura_Excel.crea_factura( orden: String; var FlexCelReport_Docs:TFlexCelReport; var XLSAdapter1:TXLSAdapter;var adodts_consumo: TADODataSet): boolean;
var
  adodataset_cab, adodataset_lin: Tadodataset;
  adocommand_temp: Tadocommand;
  ruta_ficheros_local, dir_fact, rpt_factura: ansistring;
  NomArchTemp: string;
  fecha_doc:string;
begin
  Result := true;
  Obtener_Indent(NomArchTemp);

  adocommand_temp := Tadocommand.create(nil);
  adocommand_temp.Connection := adoconnection_imp;

  adodataset_cab := Tadodataset.create(nil);
  adodataset_cab.Connection := adoconnection_imp;

  adodataset_lin := Tadodataset.create(nil);
  adodataset_lin.Connection := adoconnection_imp;

  datasource_cab := Tdatasource.create(nil);
  datasource_lin := Tdatasource.create(nil);

  datasource_cab.DataSet := adodataset_cab;
  datasource_lin.DataSet := adodataset_lin;

  adodataset_lin.DataSource := datasource_cab;

  adodataset_cab.MasterFields := 'xempresa_id;xnumdoc_id;ident;';
  adodataset_lin.MasterFields := 'xempresa_id;xnumdoc_id;ident;';
  adodataset_cab.IndexFieldNames := 'xempresa_id;xnumdoc_id;ident;';
  adodataset_lin.IndexFieldNames := 'xempresa_id;xnumdoc_id;ident;';

  carga_consultas_array();

  if crea_tabla_CabTemp(adocommand_temp) then
  begin
    if crea_cab(adodataset_cab, SDocIni ) then
    begin
      if crea_lin(adodts_consumo, orden) then
      begin
        //// lanzar fastreport
        If adodataset_cab.RecordCount > 0 then
        begin
          //lee_dato_ini(ejecutable_ini, 'rutas', 'ruta_pdf_ficheros', ruta_ficheros_local);
          lee_dato_ini(ejecutable_ini, 'rutas', 'dir_fac_local', dir_fact);
          lee_dato_ini(ejecutable_ini, 'Fichero_Report', 'rpt_factura', rpt_factura);

          crea_directorios;

          crear_excel_documento(CliIni, adodataset_cab.FieldByName('nombrecli_fact').asstring, DocIni, adodataset_cab.FieldByName('xfecha_doc').asstring,FlexCelReport_Docs, adodts_consumo , '23', adodataset_cab);

          adodataset_cab.First;

        end;
      end;
    end;
  end;
     datasource_cab.Free;
     datasource_lin.Free;
     adocommand_temp.Free;
     adodataset_cab.Free;


end;

function Timp_Albaran_Excel.crea_albaran(copia_destino: string; var FlexCelReport_Docs:TFlexCelReport; var XLSAdapter1:TXLSAdapter;var adodts_consumo: TADODataSet): boolean;
var
  i: integer;
  adodataset1, adodataset_aux: Tadodataset;
  ruta_ficheros_local,dir_alb,rpt_albaran: ansistring;
  fecha_doc:string;
begin
  if adodts_consumo.Active then adodts_consumo.Active:=false;

  Result := true;
  //lee_dato_ini(ejecutable_ini, 'rutas', 'ruta_pdf_ficheros',
   // ruta_ficheros_local);

  adodataset1 := Tadodataset.create(nil);
  adodataset1.Connection := adoconnection_imp;

  datasource1 := Tdatasource.create(nil);
  datasource2 := Tdatasource.create(nil);

  datasource1.DataSet := adodataset1;
  datasource2.DataSet := adodts_consumo;

  carga_consultas_array();

  adodataset1.MasterFields := 'xempresa_id;xnumdoc_id;xcliente_id;';
  adodts_consumo.MasterFields := 'xempresa_id;xnumdoc_id;xcliente_id;';
  adodataset1.IndexFieldNames := 'xempresa_id;xnumdoc_id;xcliente_id;';
  adodts_consumo.IndexFieldNames := 'xempresa_id;xnumdoc_id;xcliente_id;';

  adodts_consumo.DataSource := datasource1;

   adodataset_aux := Tadodataset.create(nil);
  adodataset_aux.Connection := adoconnection_imp;

  with adodataset_aux do
  begin
    CommandText := '';
    ParamCheck := true;
    CommandText := cons_select[s_albaranes_cab];
    Parameters[0].Value := sFechaIni;
    Parameters[1].Value := sFechaFin;
    if (SDocIni = '') then
      Parameters[2].Value := ('0')
    else
      Parameters[2].Value := (SDocIni);
    if (sDocFin = '') then
      Parameters[3].Value := ('9999999')
    else
      Parameters[3].Value := (sDocFin);
    Parameters[4].Value := (sCliIni);
    Parameters[5].Value := (sClifin);
    Parameters[6].Value := (sReimp);
    parameters[7].Value := '-1';
  end;
  try
    adodataset_aux.active := true;
  except
    on E: Exception do
     //Showmessage('Imposible Crear Cabeceras [s_albaranes_cab]' + E.Message);
     crea_albaran := false;
  end;

  If adodataset_aux.RecordCount > 0 then
  begin
    for i := 0 to adodataset_aux.RecordCount - 1 do
    begin
      with adodataset1 do
      begin
        CommandText := '';
        fecha_doc:='';
        ParamCheck := true;
        CommandText := cons_select[s_albaranes_cab];
        Parameters[0].Value := sFechaIni;
        Parameters[1].Value := sFechaFin;
        Parameters[2].Value := adodataset_aux.fieldbyname('xnumdoc_id')
          .asstring;
        Parameters[3].Value := adodataset_aux.fieldbyname('xnumdoc_id')
          .asstring;
        Parameters[4].Value := (sCliIni);
        Parameters[5].Value := (sClifin);
        Parameters[6].Value := (sReimp);
        parameters[7].Value := '-1';
      end;
      try
        adodataset1.active := true;
        crea_albaran := true;
      except
        on E: Exception do
        begin
          crea_albaran := false;
         //Showmessage('Imposible Crear Cabeceras [s_albaranes_cab]' + E.Message);
        end;
      end;

      with adodts_consumo do
      begin
        DataSource := datasource1;
        CommandText := '';
        ParamCheck := true;
        CommandText := cons_select[s_albaranes_lin];
        Parameters[0].Value := sFechaIni;
        Parameters[1].Value := sFechaFin;
        Parameters[2].Value := adodataset_aux.fieldbyname('xnumdoc_id').asstring;
        Parameters[3].Value := adodataset_aux.fieldbyname('xnumdoc_id').asstring;
        Parameters[4].Value := (sCliIni);
        Parameters[5].Value := (sClifin);
        Parameters[6].Value := (sReimp);
        Parameters[7].Value := copia_destino;
        parameters[8].Value := '-1';
      end;
      try
        adodts_consumo.active := true;
        crea_albaran := true;
      except
        on E: Exception do
        begin
          crea_albaran := false;
          //Showmessage('Imposible Crear Albaranes_Lin [s_albaranes_lin]' + E.Message);
        end;
      end;


      adodataset_aux.Next;
    end;

    try
      begin
        //if not DirectoryExists(ruta_ficheros_local + CliIni) then  CreateDir(ruta_ficheros_local + CliIni);
       crea_directorios;
       crear_excel_documento(CliIni, adodataset1.FieldByName('nom_cli').asstring, DocIni, adodataset1.FieldByName('xfecha_doc').asstring,FlexCelReport_Docs, adodts_consumo , '22', adodataset1);
      end;
    except
      on E: Exception do
      begin
        crea_albaran := false;
        //Showmessage(E.ClassName +' ha ocurrido un error, con el siguiente mensaje[ No se puede generar el fichero PDF -ALBARANES-] : '+ E.Message);
      end;
    end;

  end
  else
  begin
    // Showmessage('No existen ALBARANES a listar con esta selecci�n');
    crea_albaran := false;
  end;

  adodataset1.Free;
  adodts_consumo.Active:=false;
  adodataset_aux.Free;
  datasource1.Free;
  datasource2.Free;

end;

constructor Timp_pedido.create(cli_ini, cli_fin, doc_ini, doc_fin, fecha_ini,
  fecha_fin, Reimp: string; bis, sc: boolean);
begin
  SetCliIni(cli_ini);
  SetCliFin(cli_fin);
  SDocIni := (SDocIni);
  sDocFin := (sDocFin);
  sFechaIni := (sFechaIni);
  sFechaFin := (sFechaFin);
  SetReimp(Reimp);
  setBis(bis);
  setSC(sc);
end;

function Timp_pedido.crea_pedido( copia_destino: string; var FlexCelReport_Docs:TFlexCelReport; var XLSAdapter1:TXLSAdapter; var adodts_consumo: TADODataSet): boolean;
var
  i: integer;
  adodataset1, adodataset2, adodataset_aux: Tadodataset;
   ruta_ficheros_local, dir_ped, rpt_pedido_excel: ansistring;
begin


  lee_dato_ini(ejecutable_ini, 'rutas', 'ruta_pdf_ficheros', ruta_ficheros_local);
  Result := true;

  adodataset1 := Tadodataset.create(nil);
  adodataset_aux := Tadodataset.create(nil);

  datasource1 := Tdatasource.create(nil);
  datasource2 := Tdatasource.create(nil);

  adodataset1.Connection := adoconnection_imp;
  adodataset_aux.Connection := adoconnection_imp;

  adodataset1.MasterFields := 'xempresa_id;xnumdoc_id;xcliente_id;';
  adodts_consumo.MasterFields := 'xempresa_id;xnumdoc_id;xcliente_id;';
  adodataset1.IndexFieldNames := 'xempresa_id;xnumdoc_id;xcliente_id;';
  adodts_consumo.IndexFieldNames := 'xempresa_id;xnumdoc_id;xcliente_id;';

  datasource1.DataSet := adodataset1;
  datasource2.DataSet:= adodts_consumo;


  adodts_consumo.DataSource := datasource1;

  carga_consultas_array();

  with adodataset_aux do
  begin
    CommandText := '';
    ParamCheck := true;
    CommandText := cons_select[s_pedidos_cab];
    Parameters[0].Value := ('AL');
    Parameters[1].Value := sFechaIni;
    Parameters[2].Value := sFechaFin;
    if (SDocIni = '') then
      Parameters[3].Value := ('0000000')
    else
      Parameters[3].Value := (SDocIni);
    if (sDocFin = '') then
      Parameters[4].Value := ('9999999')
    else
      Parameters[4].Value := (sDocFin);
    Parameters[5].Value := (sCliIni);
    Parameters[6].Value := (sClifin);

    Parameters[7].Value := BoolToStr(sBis);

    Parameters[8].Value := (sReimp);
    parameters[9].Value := '-1';

  end;
  try
    adodataset_aux.active := true;
  except
    on E: Exception do
    begin
      Result := false;
      Showmessage('Imposible Crear Cabeceras [s_pedidos_cab]' + E.Message);
    end;
  end;

  If adodataset_aux.RecordCount > 0 then
  begin

    for i := 0 to adodataset_aux.RecordCount - 1 do
    begin
      with adodataset1 do
      begin
        CommandText := '';
        ParamCheck := true;
        ///execute g733_PedidosCab2_imp 'AL', '','','1310899', '1310899', '01005', '01005', 0,-1
        CommandText := cons_select[s_pedidos_cab];
        Parameters[0].Value := ('AL');
        Parameters[1].Value := sFechaIni;
        Parameters[2].Value := sFechaFin;
        Parameters[3].Value := adodataset_aux.fieldbyname('xnumdoc_id').asstring;
        Parameters[4].Value := adodataset_aux.fieldbyname('xnumdoc_id').asstring;
        Parameters[5].Value := (sCliIni);
        Parameters[6].Value := (sClifin);
        Parameters[7].Value := BoolToStr(sBis);
        Parameters[8].Value := (sReimp);
        parameters[9].Value := '-1';
      end;
      try
        adodataset1.active := true;
        crea_pedido := true;
      except
        on E: Exception do
        begin
          Result := false;
          Showmessage('Imposible Crear Cabeceras [s_pedidos_cab]' + E.Message);
        end;
      end;

      with adodts_consumo do
      begin
        DataSource := datasource1;
        CommandText := '';
        ParamCheck := true;
        CommandText := cons_select[s_pedidos_lin];
        ParamCheck := true;
        Parameters[0].Value := ('al');
        Parameters[1].Value := sFechaIni;
        Parameters[2].Value := sFechaFin;
        Parameters[3].Value := adodataset_aux.fieldbyname('xnumdoc_id')
          .asstring;
        Parameters[4].Value := adodataset_aux.fieldbyname('xnumdoc_id')
          .asstring;
        Parameters[5].Value := (sCliIni);
        Parameters[6].Value := (sClifin);
        Parameters[7].Value := BoolToStr(sBis);
        Parameters[8].Value := (sReimp);
        parameters[9].Value := '-1';
      end;
      try
        adodts_consumo.active := true;
      except
        on E: Exception do
        begin
          Result := false;
          Showmessage('Imposible Crear Pedidos_Lin [s_pedidos_lin]' +
            E.Message);
        end;


      end;
      try
      begin
        if not DirectoryExists(ruta_ficheros_local + CliIni) then
          CreateDir(ruta_ficheros_local + CliIni);
        crea_directorios;

         crear_excel_documento(CliIni, adodataset1.FieldByName('xnombre').asstring, DocIni, adodataset1.FieldByName('xfecha_doc').asstring,FlexCelReport_Docs, adodts_consumo , '21',  adodataset1);

        crea_pedido := true;
      end;
      except
       on E: Exception do
        Showmessage(E.ClassName +
          ' ha ocurrido un error, con el siguiente mensaje[ No se puede generar el fichero PDF -PEDIDOS-] : '
          + E.Message);
       end;


      adodataset1.active := false;
      adodts_consumo.active := false;
      adodataset_aux.Next;
    end;
  end
  else
    begin
      crea_pedido := false;
    end;

  adodataset1.Free;
  adodts_consumo.Active:=false;
  adodataset_aux.Free;
  datasource1.Free;
  datasource2.Free;

end;

procedure Timp_docs.crear_excel_documento(cliente_id , nom_cli, documento, fecha_doc:string; var FlexCelReport_Docs:TFlexCelReport; adodts_consumo:Tadodataset; tipo_doc:string; adodataset_cab:Tadodataset);
var
fichero_plantilla,  nombre_Fichero_doc_local:ansistring;
ruta_ppal_doc_local, ruta_ppal_ftp,  dir_doc_local:ansistring;
fichero_excel_local, fichero_excel_ftp:string;
begin
     fecha_doc:= stringreplace(fecha_doc, '/','',[rfReplaceAll]);
     fecha_doc:= stringreplace(fecha_doc, '-','',[rfReplaceAll]);

     //lee_dato_ini(ejecutable_ini, 'rutas', 'ruta_pdf_ficheros', ruta_ppal_doc_local);
     //lee_dato_ini(ejecutable_ini, 'rutas', 'dir_ppal_ftp', ruta_ppal_ftp);

     if tipo_doc='22' then lee_dato_ini(ejecutable_ini, 'rutas', 'dir_alb_local', dir_doc_local)
        else lee_dato_ini(ejecutable_ini, 'rutas', 'dir_fac_local', dir_doc_local);

     fichero_excel_local:= PathToSave + cliente_id + dir_doc_local;

     nombre_Fichero_doc_local:=cliente_id+'_'+documento+'_';

     if bcompleto then
     begin
        lee_dato_ini(ejecutable_ini, 'DocExcel', 'fichero_plantilla_completo',fichero_plantilla);
     end
     else
     begin
        lee_dato_ini(ejecutable_ini, 'DocExcel', 'fichero_plantilla',fichero_plantilla);
     end;

     FlexCelReport_Docs.Template:=fichero_plantilla;
     FlexCelReport_Docs.AutoClose:=true;
     FlexCelReport_Docs.FileName:=fichero_excel_local+nombre_Fichero_doc_local+fecha_doc+'.xls';
     if tipo_doc='22' then
     begin
          //FlexCelReport_Docs.FileName:=fichero_excel_local+nombre_Fichero_doc_local+fecha_doc+'.xls';
        FlexCelReport_Docs.Values['domicilio']:=adodataset_cab.FieldByName('xdomicilio').AsString;
        FlexCelReport_Docs.Values['poblacion']:=adodataset_cab.FieldByName('nom_poblacion').AsString;
        FlexCelReport_Docs.Values['provincia']:=adodataset_cab.FieldByName('nom_provincia').AsString;
        FlexCelReport_Docs.Values['xfpago']:=adodataset_cab.FieldByName('nom_fpago').AsString;
        //FlexCelReport_Docs.Values['xbase_imp']:=adodataset_cab.FieldByName('ximporte').AsString;
        FlexCelReport_Docs.Values['xnumdoc']:= 'ALBARAN : '+ documento;

     end
     else if tipo_doc='23' then
       begin
            //FlexCelReport_Docs.FileName:=fichero_excel_local+nombre_Fichero_doc_local+fecha_doc+'.xls';
            FlexCelReport_Docs.Values['domicilio']:=adodataset_cab.FieldByName('domiciliocli_fact').AsString;
            FlexCelReport_Docs.Values['poblacion']:=adodataset_cab.FieldByName('poblacioncli_fact').AsString;
            FlexCelReport_Docs.Values['provincia']:=adodataset_cab.FieldByName('provinciacli_fact').AsString;
            FlexCelReport_Docs.Values['xfpago']:=adodataset_cab.FieldByName('xdescripcion_fpago').AsString;
            //FlexCelReport_Docs.Values['xbase_imp']:=adodataset_cab.FieldByName('xbase_imp').AsString;
            FlexCelReport_Docs.Values['xnumdoc']:= 'FACTURA : '+ documento;

       end;

    FlexCelReport_Docs.Values['articulo']:= adodts_consumo.fieldbyname('xarticulo_id').AsString;
    FlexCelReport_Docs.Values['descripcion']:= adodts_consumo.fieldbyname('xdescripcion').AsString;
    FlexCelReport_Docs.Values['blister']:= adodts_consumo.fieldbyname('xporc_dto').AsString;
    FlexCelReport_Docs.Values['medida']:= adodts_consumo.fieldbyname('medida').AsString;
    FlexCelReport_Docs.Values['c_barras']:= adodts_consumo.fieldbyname('xcodalter_id').AsString;
    FlexCelReport_Docs.Values['pvp_actual']:= adodts_consumo.fieldbyname('xprec_venta').AsString;
    FlexCelReport_Docs.Values['xnombre']:= cliente_id+'-'+nom_cli;


     FlexCelReport_Docs.Run;
end;



procedure Timp_docs.SetCliIni(valor: String);
begin
  // a�adir al objeto los valores de los controles
  sCliIni := valor;
end;

procedure Timp_docs.SetCliFin(valor: String);
begin
  // a�adir al objeto los valores de los controles
  sClifin := valor;
end;

procedure Timp_docs.SetReimp(valor: string);
var
  RegExp_reimp: string; // '^(-1|1|0)$';
begin
  RegExp_reimp := '^(-1|1|0)$';
  try
    if TRegEx.IsMatch((valor), RegExp_reimp) then
      sReimp := valor
    else
      Showmessage('Valor de Reimpresi�n no v�lido !!!!!!!');
  except
    raise
  end;
end;

procedure Timp_pedido.setBis(valor: boolean);
var
  RegExp_bis: string; // '^(-1|1|0)$';
begin
  try
    RegExp_bis := '^(-1|0)$';
    if TRegEx.IsMatch(BoolToStr(valor), RegExp_bis) then
      sBis := valor
    else
      Showmessage('Valor Bis No V�lido');
  except
    on E: Exception do
  end;
end;

procedure Timp_pedido.setSC(valor: boolean);
var
  RegExp_bis: string; // '^(-1|1|0)$';
begin
  try
    RegExp_bis := '^(-1|0)$';
    if TRegEx.IsMatch(BoolToStr(valor), RegExp_bis) then
      sBis := valor
    else
      Showmessage('Valor Bis No V�lido');

  except
    on E: Exception do
  end;
end;

end.
