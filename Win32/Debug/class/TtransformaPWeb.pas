unit TtransformaPWeb;

interface

Uses  StdCtrls, adodb, db, Classes, SysUtils, regularexpressions, uitypes, Soap.SOAPHTTPClient,
StrUtils, comobj, windows ;


type
  Tpedidoweblin= class
    fichero_in:string;
    fichero_out:string;

    conexion:tadoconnection;

    procedure limpiapedidos(ruta_out:string);
    procedure transforma_carpeta_PWeb(  directorio_in, directorio_out :string);
    procedure RecorrerDirectorio( sRuta, ruta_out: String; bIncluirSubdirectorios: Boolean );
    private function obtener_num_repre(cod_cli:string):string;
  end;

implementation



procedure Tpedidoweblin.RecorrerDirectorio( sRuta, ruta_out: String; bIncluirSubdirectorios: Boolean);
var
  Directorio: TSearchRec;
  iResultado: Integer;
begin
  // Si la ruta no termina en contrabarra se la ponemos
  if sRuta[Length(sRuta)] <> '\' then
    sRuta := sRuta + '\';

   // Si la ruta no termina en contrabarra se la ponemos
  if ruta_out[Length(ruta_out)] <> '\' then
    ruta_out := ruta_out + '\';
  if ruta_out='' then ruta_out:=sRuta;

  iResultado := FindFirst( sRuta + '*.txt', FaAnyfile, Directorio );
  while iResultado = 0 do
  begin
    fichero_in:='';
    // �Es un directorio y hay que entrar en �l?
    if ( Directorio.Attr and faDirectory = faDirectory ) and bIncluirSubdirectorios then
    begin
      if ( Directorio.Name <> '.' ) and ( Directorio.Name <> '..' ) then
        RecorrerDirectorio( sRuta + Directorio.Name, ruta_out, True );
    end
    else
      // �No es el nombre de una unidad ni un directorio?
      if ( Directorio.Attr and faVolumeId <> faVolumeID ) then
         begin
          fichero_in:= sRuta + Directorio.Name;
          limpiapedidos(ruta_out);
          if not DirectoryExists('c:\pedidos\web\procesados') then  CreateDir('c:\pedidos\web\procesados');
          MoveFile(pchar(fichero_in),pchar('c:\pedidos\web\procesados\'+Directorio.Name));

         end;
    iResultado := FindNext( Directorio );
  end;
  SysUtils.FindClose( Directorio );
end;

function Tpedidoweblin.obtener_num_repre(cod_cli:string):string;
var adodset_rep:TADODataSet;
begin
  adodset_rep:= TADODataSet.Create(nil);
  adodset_rep.ParamCheck:=true;
  adodset_rep.Connection:= conexion;
  adodset_rep.CommandText:=' Select xrepresentante_id from pl_clientes where xempresa_id=''al'' and xcliente_id= :codcli';
  adodset_rep.Parameters[0].Value:=cod_cli;
  try
    begin
     adodset_rep.Active:=true;
     obtener_num_repre:= adodset_rep.FieldByName('xrepresentante_id').AsString;
     adodset_rep.Free;
    end
  finally

  end;
end;

procedure Tpedidoweblin.limpiapedidos(ruta_out:string);
var
  I: Integer;
  linea,cabeza, cola:string;
  codigo:string;
  sprecio_web:string;
  sprecio_ccs:string;
  cprecio_web:currency;
  cprecio_ccs:Currency;
  cprec_oferta_ccs:currency;
  letra:string;
  sdto:string;
  linped_in:tstrings;
  linped_out:tstrings;
  adodset1:tadodataset;
  snum_representante:string;
  sfecha:string;
begin

  adodset1:=TADODataSet.Create(nil);
  adodset1.Connection:=conexion;
  if not conexion.Connected then
  raise Exception.Create('Conexi�n a la bd no establecida');

  adodset1.CommandText:=' select left(g733_xdescart+''                                                                                                    '',40) as descripcion, '
      + 'case when g733_escalado_id=1 then 30 else 0 end AS dto, '
      + 'case when g733_escalado_id=1 then xprec_venta  else -1 end as precio, '
      + 'g733_ofertasn ofertasn, '
      + 'g733_preciooferta preciooferta '
      +'from g733_articulos where xempresa_id=''al'' and '
      +'xarticulo_id=:articulo';

  linped_in:=TStringList.Create;

  linped_out:=TStringList.Create;


  linped_out.Clear;
  linped_in.LoadFromFile(fichero_in);
  for I := 0 to linped_in.Count-1 do
  begin
      if copy(linped_in[i],1,2)='01' then
      begin
      linped_out.Add(copy(linped_in[i],1,length( linped_in[i])-4));
      snum_representante:=obtener_num_repre(copy(linped_in[i],3,5));
      sfecha:=(copy(linped_in[i],14,5));
      end
      else
      if copy(linped_in[i],1,2)='03' then
      linped_out.Add(linped_in[i])
      else
      if copy(linped_in[i],1,2)='02' then
        {$REGION 'tipo 02'}
      begin
      letra:='N';

            cabeza:=copy(linped_in[i],1,15);
            codigo:=copy(linped_in[i],3,5);
            sprecio_web:=copy( linped_in[i],length(linped_in[i])-15,13);
            //cprecio_web:=StrToCurr(sprecio_web);
             cprecio_web:=StrToCurr(ansireplacestr(sprecio_web, '.', FormatSettings.DecimalSeparator));

            adodset1.Active:=false;
            adodset1.Parameters[0].Value:=codigo;
            adodset1.Active:=true;

            cprecio_ccs:=adodset1.FieldByName('precio').AsCurrency ;
            {Miramos grupos de bombillas}
            if cprecio_ccs<>-1 then //es una bombilla
            begin
              sprecio_ccs:=FormatCurr('00000000.0000',cprecio_ccs);
              sdto:=formatcurr('00',adodset1.FieldByName('dto').ascurrency );
              letra:='N';
              //cola:=FormatCurr('00000000.0000',cprecio_ccs)+sdto+letra;
              cola:=stringreplace(FormatCurr('00000000.0000',cprecio_ccs)+sdto+letra,',','.',[rfReplaceAll]);
            end
            else
            {Miramos Ofertas}
            if adodset1.FieldByName('ofertasn').asinteger=-1  then
            begin
              cprec_oferta_ccs:= adodset1.FieldByName('preciooferta').ascurrency;
              if cprecio_web=cprec_oferta_ccs
                    then letra:='F';
               //cola:=FormatCurr('00000000.0000',cprecio_web)+'00'+letra;
                cola:=stringreplace(FormatCurr('00000000.0000',cprecio_web)+'00'+letra, ',','.',[rfReplaceAll]);
            end
            else

            //cola:=FormatCurr('00000000.0000',cprecio_web)+'00N';
             cola:=stringreplace(FormatCurr('00000000.0000',cprecio_web)+'00N',',','.',[rfReplaceAll]);

            linea:=cabeza+adodset1.FieldByName('descripcion').AsString + cola;
            linped_out.Add(linea)
          end;
    {$ENDREGION}
  end;
  fichero_out:=sfecha+ copy(fichero_in,length(fichero_in)-28,3)+'.A'+snum_representante;
  linped_out.SaveToFile(ruta_out+fichero_out);
end;


procedure Tpedidoweblin.transforma_carpeta_PWeb(directorio_in, directorio_out :string);
begin
   if not DirectoryExists(directorio_in) then
   begin
        raise Exception.Create('No existe el Directorio de entrada');
   end;

   if not DirectoryExists(directorio_out) then
   begin
        raise Exception.Create('No existe el Directorio de entrada');
   end;

   //recorrer directorio y realizar la transformaci�n
   RecorrerDirectorio(directorio_in, directorio_out, false);
  ///////////////////////////////////////////////////

end;

end.
