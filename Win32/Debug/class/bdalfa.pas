unit bdalfa;
interface


uses controls,variants,adodb,classes, dialogs,sysutils,forms;

function BDA_ObtenerConexion(ruta_fich:ansiString;var conexion:ansistring):boolean;
function bda_conectabd(var adoconn:tadoconnection):boolean;
function ejecuta_consulta_fich(fichero:string; parametros:tstrings):boolean;overload;
function ejecuta_consulta_fich(fichero:string):boolean;overload;
function ejecuta_consulta(consulta:string;parametros:tstrings):boolean;overload;
function ejecuta_comando(conexion:tadoconnection;comando_ini:string; parametros:tstrings):boolean;overload;
function carga_consultaini(seccion:string; clave:string):widestring;

function ejecuta_comando(conexion:tadoconnection;comando_ini:string):boolean;overload;
function ejecuta_comando_directo(conexion:tadoconnection;comando:ansistring; parametros:tstrings):boolean;overload;
function ejecuta_comando_directo(conexion:tadoconnection;comando:string ):boolean;overload;

function carga_registros(conexion:tadoconnection;consulta:widestring;parametros:tstrings; campo:string):tstrings;overload;
function carga_registros(conexion:tadoconnection;consulta:widestring;campo:string):tstrings;overload;

function retorna_valor(conexion:tadoconnection; campo_sel:string;variable:string; consulta:integer):string;overload;
function retorna_valor(conexion:tadoconnection; campo_sel:string;consulta:string):variant;overload;
function retorna_valor(conexion:tadoconnection; campo_sel:string;consulta:string;parametros:tstrings):variant;overload;
function retorna_valor(conexion:tadoconnection; campo_sel:string;consulta:string;parametro:string):variant;overload;
function retorna_valor(conexion:tadoconnection; campo_sel:string;fichero_ini:string;  seccion:string; clave:string;parametros_separados_por_comas:string):variant;overload;
function retorna_consulta_ini(fichero_ini:ansistring; seccion:ansistring; clave:ansistring):widestring;
function ejecuta_comando_directo(conexion:tadoconnection; fichero_ini:string;  seccion:string; clave:string;parametros_separados_por_comas:string):boolean;overload;

function devuelve_consultag733(conexion:tadoconnection; consulta_id:ansistring; formulario_id:ansistring; campo:ansistring):widestring;
function devuelve_registros_consultag733(conexion:tadoconnection; consulta_id:ansistring; formulario_id:ansistring; campo:ansistring):tstrings;overload;
function devuelve_registros_consultag733(conexion:tadoconnection; consulta_id:ansistring; formulario_id:ansistring; campo:ansistring;parametros:tstrings):tstrings;overload;
function devuelve_registros_consultag733(conexion:tadoconnection; consulta_id:ansistring; formulario_id:ansistring; parametros:tstrings):tstrings;overload;
function devuelve_registros_consultag733(conexion:tadoconnection; consulta_id:ansistring; formulario_id:ansistring; campo:ansistring; parametro:string):tstrings;overload;

function ejecuta_comandog733(conexion:tadoconnection; consulta:ansistring; formulario_id:ansistring):boolean;overload;
function ejecuta_comandog733(conexion:tadoconnection; consulta:ansistring; formulario_id:ansistring; parametro: variant):boolean;overload;
function ejecuta_comandog733(conexion:tadoconnection; consulta:ansistring; formulario_id:ansistring; parametros: tstrings):boolean;overload;
function ejecuta_comandog733(conexion:tadoconnection; consulta:ansistring; formulario_id:ansistring; timeout:integer):boolean;overload;



function carga_consulta_vector(conexion:tadoconnection;vector_consultas:array of widestring;consulta_id:ansistring;formulario_id:ansistring;num_consulta:integer):widestring;

implementation

uses general, encripter, base;


function carga_consulta_vector(conexion:tadoconnection;vector_consultas:array of widestring;consulta_id:ansistring;formulario_id:ansistring;num_consulta:integer):widestring;
var dset_aux1:tadodataset;
begin
     if vector_consultas[num_consulta]='' then
     begin
          dset_aux1:=tadodataset.create(nil);
          dset_aux1.Connection:=conexion;
          dset_aux1.CommandText:='select g733_sql from g733_consultas where g733_consulta_id='+quotedstr(consulta_id)+' and g733_formulario_id='+quotedstr(formulario_id);
          result:='';
          try
             dset_aux1.Active:=true;
             vector_consultas[num_consulta]:=dset_aux1.FieldByName('g733_sql').AsString;
          except
             showmessage('Error al cargar la consulta '+consulta_id+'-'+formulario_id);
             result:='';
          end;
    end;
    result:=vector_consultas[num_consulta];
end;


function carga_consultaini(seccion:string; clave:string):widestring;
var fich_consulta:ansistring;
begin
  if lee_dato_ini(ejecutable_ini,seccion,clave, fich_consulta) then
   result:=carga_ficherowide(fich_consulta,false)
  else
   result:='';
end;


function ejecuta_comandog733(conexion:tadoconnection; consulta:ansistring; formulario_id:ansistring):boolean;
begin
   result:=ejecuta_comandog733(conexion,consulta,formulario_id,0)
end;

function ejecuta_comandog733(conexion:tadoconnection; consulta:ansistring; formulario_id:ansistring; timeout:integer):boolean;
var
  dcomm_aux:tadocommand;
begin
     dcomm_aux:=tadocommand.create(nil);
     dcomm_aux.Connection:=conexion;

     if timeout>=0 then
       dcomm_aux.CommandTimeout:=timeout;



     dcomm_aux.commandtext:=devuelve_consultag733(conexion,consulta,formulario_id,'g733_sql');
     try

        dcomm_aux.execute;
        result:=true;
     except
        result:=false;
     end;
     dcomm_aux.free;
end;















function ejecuta_comandog733(conexion:tadoconnection; consulta:ansistring; formulario_id:ansistring; parametro: variant):boolean;overload;
var dset_aux:tadodataset;
  dcomm_aux:tadocommand;
begin
     dcomm_aux:=tadocommand.create(nil);
     dcomm_aux.Connection:=conexion;
     dcomm_aux.commandtext:=devuelve_consultag733(conexion,consulta,formulario_id,'g733_sql');
     try
        dcomm_aux.ParamCheck:=true;
        dcomm_aux.Parameters[0].value:=parametro;
        dcomm_aux.execute;
        result:=true;
     except
        result:=false;
     end;
     dcomm_aux.free;
end;






function devuelve_registros_consultag733(conexion:tadoconnection; consulta_id:ansistring; formulario_id:ansistring; campo:ansistring):tstrings;
var dset_aux, dset_aux2:tadodataset;
    strings_aux:tstrings;
    a:integer;
begin
     strings_aux:=tstringlist.create;
     strings_aux.Clear;
     dset_aux:=tadodataset.Create(nil);
     dset_aux2:=tadodataset.Create(nil);
     dset_aux.Connection:=conexion;
     dset_aux2.Connection:=conexion;
     dset_aux.CommandText:='select g733_sql from g733_consultas where g733_consulta_id='+quotedstr(consulta_id)+' and g733_formulario_id ='+quotedstr(formulario_id);
     dset_aux.active:=true;
     dset_aux2.commandtext:=dset_aux.FieldByName('g733_sql').AsString;
     dset_aux2.Active:=true;
     dset_aux2.First;
     for a:=0 to dset_aux2.RecordCount-1 do
     begin
          //if dset_aux.Fields.FieldByNumber(a).AsVariant
          strings_aux.Add(dset_aux2.FieldByname(campo).Asstring);
          dset_aux2.Next;
     end;
     devuelve_registros_consultag733:=strings_aux;
     dset_aux.Free;
     dset_aux2.Free;
end;

function devuelve_registros_consultag733(conexion:tadoconnection; consulta_id:ansistring; formulario_id:ansistring; campo:ansistring;parametros:tstrings):tstrings;
var dset_aux, dset_aux2:tadodataset;
    strings_aux:tstrings;
    a:integer;
begin
     strings_aux:=tstringlist.create;
     strings_aux.Clear;
     dset_aux:=tadodataset.Create(nil);
     dset_aux2:=tadodataset.Create(nil);
     dset_aux.Connection:=conexion;
     dset_aux2.Connection:=conexion;
     dset_aux.CommandText:='select g733_sql from g733_consultas where g733_consulta_id='+quotedstr(consulta_id)+' and g733_formulario_id ='+quotedstr(formulario_id);
     dset_aux.active:=true;

     dset_aux2.commandtext:=dset_aux.FieldByName('g733_sql').AsString;
     dset_aux.Active:=false;
     dset_aux2.Active:=true;
     dset_aux2.First;
     for a:=1 to dset_aux2.FieldCount do
       strings_aux.Add(dset_aux2.Fields.FieldByNumber(a).Asstring);
     devuelve_registros_consultag733:=strings_aux;
     dset_aux.Free;
end;

function devuelve_registros_consultag733(conexion:tadoconnection; consulta_id:ansistring; formulario_id:ansistring; parametros:tstrings):tstrings;
var dset_aux, dset_aux2:tadodataset;
    strings_aux:tstrings;
    a:integer;
    I: Integer;
begin
   strings_aux:=tstringlist.create;
   strings_aux.Clear;
   dset_aux:=tadodataset.Create(nil);
   dset_aux2:=tadodataset.Create(nil);
   dset_aux.Connection:=conexion;
   dset_aux2.Connection:=conexion;

   dset_aux.CommandText:='select g733_sql from g733_consultas where g733_consulta_id='+quotedstr(consulta_id)+' and g733_formulario_id ='+quotedstr(formulario_id);
   dset_aux.active:=true;

   dset_aux2.commandtext:=dset_aux.FieldByName('g733_sql').AsString;
   dset_aux2.ParamCheck:=true;
   if parametros<>nil then

   for I := 0 to parametros.Count-1 do
     dset_aux2.Parameters[i].Value:=parametros[i];

   dset_aux.Active:=false;
   dset_aux2.Active:=true;
   dset_aux2.First;
   if dset_aux2.RecordCount=0 then
     strings_aux.Clear
   else
   for a:=1 to dset_aux2.FieldCount do
     strings_aux.Add(dset_aux2.Fields.FieldByNumber(a).Asstring);

   devuelve_registros_consultag733:=strings_aux;

   dset_aux.Free;
end;

function devuelve_registros_consultag733(conexion:tadoconnection; consulta_id:ansistring; formulario_id:ansistring;campo:ansistring; parametro:string):tstrings;
var dset_aux, dset_aux2:tadodataset;
    strings_aux:tstrings;
    a:integer;
begin
     strings_aux:=tstringlist.create;
     strings_aux.Clear;
     dset_aux:=tadodataset.Create(nil);
     dset_aux2:=tadodataset.Create(nil);
     dset_aux.Connection:=conexion;
     dset_aux2.Connection:=conexion;
     dset_aux.CommandText:='select g733_sql from g733_consultas where g733_consulta_id='+quotedstr(consulta_id)+' and g733_formulario_id ='+quotedstr(formulario_id);
     dset_aux.active:=true;
     dset_aux2.commandtext:=dset_aux.FieldByName('g733_sql').AsString;
     if parametro<>'' then
       dset_aux2.Parameters[0].value:=parametro;
     dset_aux2.Active:=true;
     dset_aux2.First;
     for a:=0 to dset_aux2.RecordCount-1 do
     begin
          //if dset_aux.Fields.FieldByNumber(a).AsVariant
          strings_aux.Add(dset_aux2.FieldByname(campo).Asstring);
          dset_aux2.Next;
     end;
     devuelve_registros_consultag733:=strings_aux;
     dset_aux.Free;
       dset_aux2.Free;
end;



function devuelve_consultag733(conexion:tadoconnection; consulta_id:ansistring; formulario_id:ansistring; campo:ansistring):widestring;
var dset_aux:tadodataset;
begin
     dset_aux:=tadodataset.Create(nil);
     dset_aux.Connection:=conexion;
     dset_aux.CommandText:='select '+campo+' from imp.g733_consultas where g733_consulta_id='''+consulta_id+''' and g733_formulario_id ='''+formulario_id+'''';
     dset_aux.active:=true;
     devuelve_consultag733:=dset_aux.fieldbyname(campo).asstring;
     dset_aux.Free;
end;

function ejecuta_comandog733(conexion:tadoconnection; consulta:ansistring; formulario_id:ansistring; parametros: tstrings):boolean;overload;
var dset_aux:tadodataset;
    dcomm_aux:tadocommand;
  I: Integer;
begin
     dcomm_aux:=tadocommand.create(nil);
     dcomm_aux.Connection:=conexion;
     dcomm_aux.commandtext:=devuelve_consultag733(conexion,consulta,formulario_id,'g733_sql');
     result:=false;
     try
        dcomm_aux.ParamCheck:=true;
        for I := 0 to parametros.count-1 do
           dcomm_aux.Parameters[i].value:=parametros[i];



        dcomm_aux.execute;
        result:=true;
     except

       on e:exception do showmessage('Error al insertar el componente: '+e.message);


     end;
     dcomm_aux.free;
end;




function retorna_consulta_ini(fichero_ini:ansistring; seccion:ansistring; clave:ansistring):widestring;
var  cons1:ansistring;
begin
    cons1:='';
    lee_dato_ini(fichero_ini,seccion,clave,cons1);
    if cons1<>'' then
    begin
      if fileexists(cons1) then
      begin
           result:=carga_ficherowide(cons1,false)
      end
      else
          result:=cons1;
    end
    else
        result:='';
end;


function carga_registros(conexion:tadoconnection;consulta:widestring;parametros:tstrings; campo:string):tstrings;overload;
var dset_aux:tadodataset;
    tstrings_aux:tstrings;
    a:integer;
begin
     tstrings_aux:=tstringlist.Create;
     tstrings_aux.Clear;
     dset_aux:=tadodataset.create(nil);
     dset_aux.Connection:=conexion;
     dset_aux.CommandTimeout:=90;
     dset_aux.CommandText:=consulta;
     if parametros<>nil then

     if parametros.count>0 then
     for a:=0 to parametros.Count-1 do
     dset_aux.Parameters[a].value:=parametros[a];
     dset_aux.active:=true;
     dset_aux.First;
     while not(dset_aux.Eof) do
     begin
          tstrings_aux.add(dset_aux.fieldbyname(campo).asstring);
          dset_aux.Next;
     end;
     dset_aux.Free;
     carga_registros:=tstrings_aux;
end;

function carga_registros(conexion:tadoconnection;consulta:widestring; campo:string):tstrings;overload;
var dset_aux:tadodataset;
    tstrings_aux:tstrings;

begin
     tstrings_aux:=tstringlist.Create;
     tstrings_aux.Clear;
     dset_aux:=tadodataset.create(nil);
     dset_aux.Connection:=conexion;
     dset_aux.CommandTimeout:=90;
     dset_aux.CommandText:=consulta;
     dset_aux.active:=true;
     dset_aux.First;
     while not(dset_aux.Eof) do
     begin
          tstrings_aux.add(dset_aux.fieldbyname(campo).asstring);
          dset_aux.Next;
     end;
     dset_aux.Free;
     carga_registros:=tstrings_aux;
end;

function retorna_valor(conexion:tadoconnection; campo_sel:string;fichero_ini:string;  seccion:string; clave:string;parametros_separados_por_comas:string):variant;overload;
var adodataset_aux:tadodataset;
    a:integer;
    cons:ansistring;
    params:tstrings;
begin
     result:=-99;
     cons:='';
     params:=tstringlist.Create;
     params:=cambia_a_tstrings(parametros_separados_por_comas,',');
     adodataset_aux:=tadodataset.Create(adodataset_aux);
     adodataset_aux.Connection:=conexion;
     if (params.count=1) and (params[0]='') then params.clear;
     if lee_dato_ini(fichero_ini,seccion,clave,cons) then
     begin
          if fileexists(cons) then
            adodataset_aux.commandtext := carga_ficherowide(cons,false)
          else
            adodataset_aux.commandtext := cons;
    // adodataset_aux.Parameters.refresh;

          if adodataset_aux.commandtext<>'' then
          begin
                for a:=0 to params.count-1 do
                  adodataset_aux.Parameters[a].Value:=params[a];
                try
                   adodataset_aux.active:=true;
                   result:=adodataset_aux.fieldbyname(campo_sel).asvariant;
                   if varisnull(result) then result:=-99;

                   adodataset_aux.Active:=false;
                except
                on E: Exception do
                   ShowMessage ('Error: ' + E.Message);
                end;
          end;
     end;
     adodataset_aux.free;
{sss}
end;




function retorna_valor(conexion:tadoconnection; campo_sel:string;consulta:string;parametro:string):variant;overload;
var adodataset_aux:tadodataset;

begin
     result:=-99;
     adodataset_aux:=tadodataset.Create(nil);

     adodataset_aux.Connection:=conexion;

     adodataset_aux.commandtext := consulta;
    // adodataset_aux.Parameters.refresh;
     if adodataset_aux.commandtext<>'' then
     begin
           adodataset_aux.Parameters[0].Value:=parametro;
          try
            adodataset_aux.active:=true;
            result:=adodataset_aux.fieldbyname(campo_sel).asvariant;
            adodataset_aux.Active:=false;
          except
            on E: Exception do
            ShowMessage ('Error: ' + E.Message);
          end;
     end;
     adodataset_aux.free;
{sss}
end;


function retorna_valor(conexion:tadoconnection; campo_sel:string;consulta:string;parametros:tstrings):variant;overload;
var adodataset_aux:tadodataset;
    a:integer;
begin
     result:=-99;
     adodataset_aux:=tadodataset.Create(adodataset_aux);

     adodataset_aux.Connection:=conexion;

     adodataset_aux.commandtext := consulta;
    // adodataset_aux.Parameters.refresh;
     if adodataset_aux.commandtext<>'' then
     begin
          for a:=0 to parametros.count-1 do
           adodataset_aux.Parameters[a].Value:=parametros[a];
          try
            adodataset_aux.active:=true;
            result:=adodataset_aux.fieldbyname(campo_sel).asvariant;
            adodataset_aux.Active:=false;
          except
            on E: Exception do
            ShowMessage ('Error: ' + E.Message);
          end;
     end;
     adodataset_aux.free;
{sss}
end;

function retorna_valor(conexion:tadoconnection; campo_sel:string;consulta:string):variant;overload;
var adodataset_aux:tadodataset;
    sql1:string;

begin
     result:=-99;
     adodataset_aux:=tadodataset.Create(nil);
     sql1:='';
     adodataset_aux.Connection:=conexion;
     adodataset_aux.commandtext := consulta;
     if adodataset_aux.commandtext<>'' then
     begin
          try
            adodataset_aux.active:=true;
            if adodataset_aux.RecordCount>0 then

            result:=adodataset_aux.fieldbyname(campo_sel).asvariant
            else
            result:=-99;
           
            adodataset_aux.Active:=false;
          except
            on E: Exception do
            ShowMessage ('Error: ' + E.Message);
          end;
     end;

     adodataset_aux.free;
{sss}
end;


function retorna_valor(conexion:tadoconnection; campo_sel:string; variable:string;consulta:integer):string;
var adodataset_aux:tadodataset;
    sql1:ansistring;
begin
     result:='';
     adodataset_aux:=tadodataset.Create(adodataset_aux);
     sql1:='';
     adodataset_aux.Connection:=conexion;
     if not lee_dato_ini('c:\g733.ini','sql','f_retorna_valor_c'+inttostr(consulta), sql1) then showmessage('Error al cargar consulta f_retorna_valor');
     adodataset_aux.commandtext := carga_ficherowide(sql1,false);
     if adodataset_aux.commandtext<>'' then
     begin
          adodataset_aux.ParamCheck:=true;
          adodataset_aux.Parameters[0].value:=variable;
          try
            adodataset_aux.active:=true;
           result:=adodataset_aux.fieldbyname(campo_sel).asstring;
            adodataset_aux.Active:=false;
          except
            on E: Exception do
            ShowMessage ('Error: ' + E.Message);
          end;
     end;
     adodataset_aux.free;
{sss}
end;


function ejecuta_consulta_fich(fichero:string; parametros:tstrings):boolean;overload;
var a:integer;
begin
     a:=1;
end;

function ejecuta_consulta_fich(fichero:string):boolean;overload;
var a:integer;
begin
     a:=1;
end;
function ejecuta_consulta(consulta:string;parametros:tstrings):boolean;overload;
var a:integer;
begin
     a:=1;
end;

function ejecuta_comando(conexion:tadoconnection; comando_ini:string;parametros:tstrings):boolean;
var a:integer;
    sql1:ansistring;
    adocmd_aux:tadocommand;

begin
     result:=falsE;
     adocmd_aux:=tadocommand.Create(nil);
     adocmd_aux.Connection:=conexion;
     if lee_dato_ini(ChangeFileExt(application.exename,'.ini'),'sql',comando_ini,sql1)
      then
      begin
           adocmd_aux.commandtext:=carga_ficherowide(sql1,false);
           adocmd_aux.ParamCheck:=true;
           for a:=0 to parametros.Count-1 do
            adocmd_aux.Parameters[a].Value:=parametros[a];
           try
              adocmd_aux.Execute;

              result:=true;
           except
              result:=false;
           end;
      end
      else
       showmessage('Error al cargar consulta ' + comando_ini);
      adocmd_aux.Free;
end;
 {******************************************************************************}
function ejecuta_comando_directo(conexion:tadoconnection; fichero_ini:string;  seccion:string; clave:string;parametros_separados_por_comas:string):boolean;overload;
var a:integer;
    cons:ansistring;
    adocmd_aux:tadocommand;
    params:tstrings;
begin
     result:=false;
     cons:='';
     params:=tstringlist.Create;
     params:=cambia_a_tstrings(parametros_separados_por_comas,',');
     adocmd_aux:=tadocommand.Create(nil);
     adocmd_aux.Connection:=conexion;
     adocmd_aux.CommandTimeout:=600;
     adocmd_aux.ParamCheck:=true;

     if lee_dato_ini(fichero_ini,seccion,clave,cons) then
     begin
          if fileexists(cons) then
            adocmd_aux.commandtext := carga_ficherowide(cons,false)
          else
            adocmd_aux.commandtext := cons;
    // adodataset_aux.Parameters.refresh;

          if adocmd_aux.commandtext<>'' then
          begin
                for a:=0 to params.count-1 do
                  adocmd_aux.Parameters[a].Value:=params[a];
                try
                   adocmd_aux.execute;
                   result:=true;

                except
                {
                on E: Exception do
                   begin
                      ShowMessage ('Error: ' + E.Message);
                      result:=false;
                   end;
                 }
                end;
          end;
     end;
     adocmd_aux.free;
{sss}


end;

function ejecuta_comando_directo(conexion:tadoconnection; comando:ansistring;parametros:tstrings):boolean;
var a:integer;
//    sql1:ansistring;
    adocmd_aux:tadocommand;
begin
     result:=falsE;
     adocmd_aux:=tadocommand.Create(nil);
     adocmd_aux.Connection:=conexion;
     conexion.Connected:=true;
     adocmd_aux.CommandTimeout:=600;
     adocmd_aux.commandtext:=comando;
     adocmd_aux.ParamCheck:=true;
     for a:=0 to parametros.Count-1 do
         adocmd_aux.Parameters[a].Value:=parametros[a];
     try
        adocmd_aux.Execute;
        result:=true;
     except
    {    on E : Exception do
      begin
        ShowMessage(E.ClassName+' Error : '+E.Message); result:=false;  result:=false;
      end;

     }
     result:=false;
     end;
     adocmd_aux.Free;
end;


function ejecuta_comando_directo(conexion:tadoconnection; comando:string):boolean;
var a:integer;
    sql1:string;
    adocmd_aux:tadocommand;
begin
     result:=falsE;
     adocmd_aux:=tadocommand.Create(nil);
     adocmd_aux.Connection:=conexion;
     adocmd_aux.commandtext:=comando;
     adocmd_aux.CommandTimeout:=300;
     try
        adocmd_aux.Execute;
        result:=true;
     except
        result:=false;
     end;
     adocmd_aux.Free;
end;

function ejecuta_comando(conexion:tadoconnection; comando_ini:string):boolean;
var
    sql1:ansistring;
    adocmd_aux:tadocommand;
begin
     result:=falsE;
     adocmd_aux:=tadocommand.Create(nil);
     adocmd_aux.Connection:=conexion;

     if lee_dato_ini(ChangeFileExt(application.exename,'.ini'),'sql',comando_ini,sql1)
      then
      begin
           adocmd_aux.commandtext:=carga_ficherowide(sql1,false);
           try
              adocmd_aux.Execute;

              result:=true;
           except
              result:=false;
           end;
      end
      else
       showmessage('Error al cargar consulta' + comando_ini);
      adocmd_aux.Free;
end;



function bda_conectabd(var adoconn:tadoconnection):boolean;
var conex:ansistring;
begin
     if not adoconn.connected then
       begin
            adoconn.LoginPrompt:=false;
            if BDA_ObtenerConexion('c:\g733.ini',conex) then
               adoconn.ConnectionString:=conex;
            try
               adoconn.Connected:=true;
            except
               showmessage('Error al activar la conexi�n a la base de datos');
            end
       end;
     result:=adoconn.Connected;
end;


function BDA_ObtenerConexion(ruta_fich:ansistring; var conexion:ansistring):boolean;
//BDA_ObtenerConexion('C:\Archivos de programa\Borland\Delphi7\Projects\prova.ini',str_conexion)
var conex:ansistring;
    conexy:ansistring;
begin
//     result:= lee_dato_ini(ruta_fich,'BDALFA','conexion', conexion);
     result:=true;
     conexy:='';
     conex:='';
     if not lee_dato_ini(ejecutable_ini,'bdalfa','conexiony',conexy) then
        if not lee_dato_ini(ejecutable_ini,'bdalfa','conexion',conex)  then
           if not lee_dato_ini('c:\g733.ini','bdalfa','conexiony',conexy) then
            begin
                showmessage('No se ha encontrado la cadena de conexi�n a la base de datos');
                result:=false;
            end
            else
                begin
                     crea_dato_ini(ejecutable_ini,'conexion','bdalfa',conexy)
                end;
     if result=true then
     begin
           if (conexy='') and (conex<>'') then conexion:=conex;
           if (conexy<>'') and (conex='') then conexion:=desencripta(conexy,1499);
          
     end;
end;


end.
