unit encripter;

interface

uses classes;

function desencripta(const S: AnsiString; Key: Word): AnsiString; overload;
function encripta(const S: AnsiString; Key: Word): AnsiString; overload;
function desencripta(const testrings: tstrings; Key: Word): tstrings; overload;
function encripta(const testrings: tstrings; Key: Word): tstrings; overload;

implementation

const
  C1 = 52845;
  C2 = 22719;

function desencripta(const testrings: tstrings; Key: Word): tstrings;
var
  a: integer;
  testrings_desencriptado: tstrings;

begin
  testrings_desencriptado := Tstringlist.create;
  testrings_desencriptado.Clear;

  if testrings.count <> 0 then
  begin
    for a := 0 to testrings.count - 1 do
      testrings_desencriptado.add(desencripta(testrings[a], Key));
    result := testrings_desencriptado;
  end;
end;

function encripta(const testrings: tstrings; Key: Word): tstrings;
var
  a: integer;
  testrings_encriptado: tstrings;

begin

  testrings_encriptado := Tstringlist.create;
  testrings_encriptado.Clear;

  if testrings.count <> 0 then
  begin
    for a := 0 to testrings.count - 1 do
      testrings_encriptado.add(encripta(testrings[a], Key));
    result := testrings_encriptado;
  end;
end;

function Decode(const S: AnsiString): AnsiString;
const
  Map: array [ansichar] of byte = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 62, 0, 0, 0, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 0, 0, 0,
    0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,
    18, 19, 20, 21, 22, 23, 24, 25, 0, 0, 0, 0, 0, 0, 26, 27, 28, 29, 30, 31,
    32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
    51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0);
var
  I: LongInt;
begin
  case Length(S) of
    2:
      begin
        I := Map[S[1]] + (Map[S[2]] shl 6);
        SetLength(result, 1);
        Move(I, result[1], Length(result))
      end;
    3:
      begin
        I := Map[S[1]] + (Map[S[2]] shl 6) + (Map[S[3]] shl 12);
        SetLength(result, 2);
        Move(I, result[1], Length(result))
      end;
    4:
      begin
        I := Map[S[1]] + (Map[S[2]] shl 6) + (Map[S[3]] shl 12) +
          (Map[S[4]] shl 18);
        SetLength(result, 3);
        Move(I, result[1], Length(result))
      end
  end
end;

function PreProcess(const S: AnsiString): AnsiString;
var
  SS: AnsiString;
begin
  SS := S;
  result := '';
  while SS <> '' do
  begin
    result := result + Decode(Copy(SS, 1, 4));
    Delete(SS, 1, 4)
  end
end;

function InternalDecrypt(const S: AnsiString; Key: Word): AnsiString;
var
  I: Word;
  Seed: Word;
begin
  result := S;
  Seed := Key;
  for I := 1 to Length(result) do
  begin
    result[I] := ansichar(byte(result[I]) xor (Seed shr 8));
    Seed := (byte(S[I]) + Seed) * Word(C1) + Word(C2)
  end
end;

function desencripta(const S: AnsiString; Key: Word): AnsiString;
begin
  result := InternalDecrypt(PreProcess(S), Key)
end;

function Encode(const S: AnsiString): AnsiString;
const
  Map: array [0 .. 63] of Char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
    'abcdefghijklmnopqrstuvwxyz0123456789+/';
var
  I: LongInt;
begin
  I := 0;
  Move(S[1], I, Length(S));
  case Length(S) of
    1:
      result := Map[I mod 64] + Map[(I shr 6) mod 64];
    2:
      result := Map[I mod 64] + Map[(I shr 6) mod 64] + Map[(I shr 12) mod 64];
    3:
      result := Map[I mod 64] + Map[(I shr 6) mod 64] + Map[(I shr 12) mod 64] +
        Map[(I shr 18) mod 64]
  end
end;

function PostProcess(const S: AnsiString): AnsiString;
var
  SS: AnsiString;
begin
  SS := S;
  result := '';
  while SS <> '' do
  begin
    result := result + Encode(Copy(SS, 1, 3));
    Delete(SS, 1, 3)
  end
end;

function InternalEncrypt(const S: AnsiString; Key: Word): AnsiString;
var
  I: Word;
  Seed: Word;
begin
  result := S;
  Seed := Key;
  for I := 1 to Length(result) do
  begin
    result[I] := ansichar(byte(result[I]) xor (Seed shr 8));
    Seed := (byte(result[I]) + Seed) * Word(C1) + Word(C2)
  end
end;

function encripta(const S: AnsiString; Key: Word): AnsiString;
begin
  result := PostProcess(InternalEncrypt(S, Key))
end;

end.
