unit TCli_Web;

interface

uses StdCtrls, adodb, db, Classes, SysUtils, regularexpressions, uitypes, Soap.SOAPHTTPClient,
StrUtils, comobj, windows ;


type TGrupo_usuarios= class(Tobject)
strict private
          xempresa_id :string;
          xgrupo_usuario_id:string;
          xnombre:string;
          procedure SetEmpresa_id(empresa_id:string);
          procedure SetGrupo_Usuario_id(grupo_usuario_id:string);
          procedure SetNombre(nombre:string);
      public
          adoconnection_grupo_usuweb: Tadoconnection;
          property Empresa_id :string read xempresa_id write SetEmpresa_id;
          property Grupo_Usuario :string read xgrupo_usuario_id  write SetGrupo_Usuario_id;
          property Nombre:string read xnombre write SetNombre;

          constructor create(adoconnection:TADOConnection);
          function Inserta_Grupo(grupo_usuario,nombre:string):boolean;
          function Actualiza_grupo(grupo_usuario, nombre:String):boolean;
          function Carga_grupo(grupo_usuario:string):boolean;
          function devuelve_grupos_usuarios():Tstringlist;

end;

type TLocalEnv_Web =  class (Tobject)
  strict private
          xcod_postal:string;
          xdomicilio:string;
          xempresa_id:string;
          xlocal_id:String;
          xpais_id:string;
          xpoblacion:string;
          xprovincia_id:string;
          xusuario_id:String;
          xnombre_loc:string;
          xprovincia_desc:string;
          xpais_desc:string;

          procedure SetCod_postal(cod_postal: string);
          procedure SetDomicilio(domicilio: string);
          procedure SetEmpresa_id(empresa_id:string);
          procedure SetLocal_Envio_id(local_envio_id: string);
          procedure SetPais_id(pais_id: string);
          procedure SetPoblacion(poblacion: string);
          procedure SetProvincia_id(provincia_id: string);
          procedure SetUsuario_id(usuario_id:string);
          procedure SetNombre_loc(nombre_loc: string);
          procedure setProvincia_desc(provincia_desc:String);
          procedure setPais_desc(pais_desc:string);

  public
          adoconnection_local_env: Tadoconnection;
          property CodigoPostal: string read xcod_Postal write SetCod_postal;
          property Domicilio: string read xdomicilio write SetDomicilio;
          property Empresa_id :string read xempresa_id write SetEmpresa_id;
          property Local_envio_id:string read xlocal_id write SetLocal_Envio_id;
          property Pais_id: string read xpais_id write SetPais_id;
          property Poblacion: string read xpoblacion write SetPoblacion;
          property Provincia_id: string read xprovincia_id write SetProvincia_id;
          property Usuario_id :string read xusuario_id  write SetUsuario_id;
          property Nombre_loc: string read xnombre_loc write SetNombre_loc;
          property Pais_desc:String read xpais_desc write setPais_desc;
          property Provincia_desc:string read xprovincia_desc write setProvincia_desc;

          constructor create(Adoconnection:Tadoconnection);
          function Carga_Local_env(local_id, usuario_id:string):boolean;
          function Inserta_Local_env(usuario_id:string):boolean;
          function Actualiza_local_env(cod_postal, domicilio, local_id, pais_id, poblacion_id, provincia_id, usuario_id, nombre_loc:String):boolean;
          function getEmailLocalEnv(usuario, localizacion:String):String;
end;

type Tlocalizaciones =  class (Tobject)
      dataset:tadodataset;
      locs:array of TLocalEnv_Web;
end;

type TCliente_Web = class ( Tobject)
    strict private
          xactivo: boolean;
          xcliente_id: string;
          xemail: string;
          xempresa_id:String;
          xfpago_id:string;
          xfpago_desc:String;
          xgrupo_usuario_id: string;
          xlocal_id:String;
          xnombre:string;
          xtelefono:string;
          xusuario_id:string;
          xcod_agrup : String;
          xmodificado: boolean;
          xpublicado : boolean;
          xnombre_grupo_usuario :String;
          xpwd_cli:boolean;
          xemail_pwd:String;
          xpwd_enviado:string;
          xfecha_envio_ult_pwd:string;
          xusuario_envio_pwd:string;
          xemail_repre:String;
          xrepresentante_id:string;
          cooperativa:string;
          pedido_minimo:integer;

          procedure SetActivo(activo: boolean);
          procedure SetCliente_id(cliente_id: string);
          procedure SetEmail(email: string);
          procedure SetEmpresa_id(empresa_id: string);
          procedure SetFPago_id(fpago_id: string);
          procedure SetFPago_desc(fpago_desc: string);
          procedure Setgrupo_usuario_id(grupo_usuario_id: string);
          procedure SetLocal_Envio_id(local_envio_id: string);
          procedure SetNombre(nombre: string);
          procedure SetTelefono(telefono: string);
          procedure SetUsuario_id(usuario_id: string);
          procedure SetCod_agrup(cod_agrup:string);
          procedure SetModificado(modificado: boolean);
          procedure SetPublicado(publicado:boolean);
          procedure SetNombre_Grupo_Usuario(nombre_grupo:string);
          procedure SetPwd_cli(pwd_cli:boolean);

          procedure setEmailPwd(email:string);
          procedure setPwd_Enviado(pwd_enviado:string);
          procedure setFecha_Envio_pwd(fecha_envio_pwd:String);
          procedure setUsuario_Envio_pwd(usuario_envio_pwd:string);

          procedure setEmail_repre(email_repre:string);
          procedure setRepresentante_id(representante_id:string);
          procedure setCooperativa(fcooperativa:string);
          procedure setPedido_minimo(fpedidoMinimo:integer);


    public
          xlocalizaciones :Tlocalizaciones;
          adoconnection_cliweb: Tadoconnection;
          property Activo:boolean  read xactivo write SetActivo;
          property Cliente_id:string read xcliente_id write SetCliente_id;
          property Email:string read xemail write SetEmail;
          property Empresa:string read xempresa_id write SetEmpresa_id;
          property FPago:string read xfpago_id write SetFPago_id;
          property Grupo_Usuario_id :string read xgrupo_usuario_id write Setgrupo_usuario_id;
          property Local_envio_id:string read xlocal_id write SetLocal_Envio_id;
          property Nombre:string read xnombre write SetNombre;
          property Telefono:string read xtelefono write SetTelefono;
          property Usuario_id:string read xusuario_id write SetUsuario_id;
          property Cod_agrup:string read xcod_agrup write setCod_agrup;
          property Modificado:boolean read xmodificado write SetModificado;
          property Publicado:boolean read xpublicado write SetPublicado;
          property Nombre_Grupo_Usuario:String read xnombre_grupo_usuario  write SetNombre_Grupo_Usuario;
          property FPago_desc:String read xfpago_desc write SetFPago_desc;
          property Localizaciones:Tlocalizaciones read xlocalizaciones;
          property Pwd_Cli: boolean read xpwd_cli write SetPwd_cli;

          property email_pwd:String read xemail_pwd write setEmailPwd;
          property pwd_enviado:string read xpwd_enviado write setPwd_Enviado  ;
          property fecha_envio_ult_pwd:string  read xfecha_envio_ult_pwd write setFecha_Envio_pwd;
          property usuario_envio_pwd:string  read xusuario_envio_pwd write setUsuario_Envio_pwd;

          property email_repre:string  read xemail_repre write setEmail_repre;
          property representante_id:string read xrepresentante_id write setRepresentante_id;
          property fcooperativa:string read cooperativa write setCooperativa;
          property fpedidoMinimo:integer  read pedido_minimo write SetPedido_minimo;

          constructor create(Adoconnection:Tadoconnection);
          procedure insertar_cliweb(cliente_id,grupo_usuario_id,local_id:string);
          function actualiza_cliweb(activo:boolean; cliente_id, email, fpago_id, grupo_usuario_id, local_env_id, nombre, telefono, usuario_id, codigo_agrup :String; importe_pedido_minimo:integer; modificado:boolean):boolean;
          function verifica_grupo_cliente(grupo_usuario_id:string):boolean;
          function verifica_Local_env(local_env_id:string):boolean;
          procedure crear_usuarioweb(usuario_id,local_id, xactivo, grupo_usuario, email:string);
          function carga_combo_localenv():Tstringlist;
          function existe_usuario_web(usuario_id:String):boolean;
          function publicar():boolean;
          function guardar(u_web:TCliente_Web):boolean;
          function Cli_grupo_dto(cliente_id:string):string;
          procedure Crear_directorios(cliente_id:string);
          function getRepresentante(cliente_id:string):string;
          function envia_documento_directo_indy(nombre_cliente, usuario_web, pwd_usuario, email_destinatario, nombre_destinatario: string): boolean;
          procedure guarda_alta_datos_usuario(nombre_cliente, usuario_web, pwd_usuario, email_destinatario, importe_min_ped:string);
          function envia_documento_outlook(nombre_cliente, usuario_web, pwd_usuario, email_destinatario, nombre_destinatario: string): boolean;
end;


implementation

uses general,Magento, encripter, IdMessage, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdMessageClient, IdSMTP, Gauges, IdAttachmentFile,
  IdExplicitTLSClientServerBase, IdSMTPBase, IdIOHandler,
  IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL, ftp;

function GetloginName: string;
var
  buffer: array [0 .. 255] of Char;
  size: dword;
begin
  size := 256;
    if GetUserName(buffer, size) then
    Result := buffer
    else
    Result := ''
end;

procedure TCliente_Web.Crear_directorios(cliente_id:string);
var ruta_ppal_local, ruta_ppal_ftp, dir_ped_local, dir_alb_local, dir_fac_local, dir_ppte_local, dir_consumo_local:ansistring;
 dir_fac_ftp,dir_ped_ftp , dir_alb_ftp, dir_ppte_ftp, dir_consumo_ftp:ansistring;
 hosty, usery, pwdy : ansistring;
begin
  lee_dato_ini(ejecutable_ini, 'cuenta_ftp', 'hosty', hosty);
  lee_dato_ini(ejecutable_ini, 'cuenta_ftp', 'usernamey', usery);
  lee_dato_ini(ejecutable_ini, 'cuenta_ftp', 'passwordy', pwdy);

  lee_dato_ini(ejecutable_ini, 'rutas', 'ruta_ppal_local', ruta_ppal_local);
  lee_dato_ini(ejecutable_ini, 'rutas', 'dir_ppal_ftp', ruta_ppal_ftp);

  lee_dato_ini(ejecutable_ini, 'rutas', 'dir_ped_local', dir_ped_local);
  lee_dato_ini(ejecutable_ini, 'rutas', 'dir_alb_local', dir_alb_local);
  lee_dato_ini(ejecutable_ini, 'rutas', 'dir_fac_local', dir_fac_local);
  lee_dato_ini(ejecutable_ini, 'rutas', 'dir_ppte_local', dir_ppte_local);
  lee_dato_ini(ejecutable_ini, 'rutas', 'dir_consumo_local', dir_consumo_local);

  lee_dato_ini(ejecutable_ini, 'rutas', 'dir_fac_ftp', dir_fac_ftp);
  lee_dato_ini(ejecutable_ini, 'rutas', 'dir_ped_ftp', dir_ped_ftp);
  lee_dato_ini(ejecutable_ini, 'rutas', 'dir_alb_ftp', dir_alb_ftp);
  lee_dato_ini(ejecutable_ini, 'rutas', 'dir_ppte_ftp', dir_ppte_ftp);
  lee_dato_ini(ejecutable_ini, 'rutas', 'dir_consumo_ftp', dir_consumo_ftp);

  if not DirectoryExists(ruta_ppal_local+ cliente_id) then
          CreateDir(ruta_ppal_local + cliente_id);

  if not DirectoryExists(ruta_ppal_local + cliente_id + dir_ped_local ) then
          CreateDir(ruta_ppal_local + cliente_id +dir_ped_local);

  if not DirectoryExists(ruta_ppal_local + cliente_id + dir_alb_local) then
          CreateDir(ruta_ppal_local + cliente_id + dir_alb_local);

  if not DirectoryExists(ruta_ppal_local + cliente_id +dir_fac_local) then
          CreateDir(ruta_ppal_local + cliente_id + dir_fac_local);

  if not DirectoryExists(ruta_ppal_local + cliente_id +dir_ppte_local) then
          CreateDir(ruta_ppal_local + cliente_id + dir_ppte_local);

  if not DirectoryExists(ruta_ppal_local + cliente_id + dir_consumo_local) then
          CreateDir(ruta_ppal_local + cliente_id +dir_consumo_local);


  if ftp_conectado <> 1 then  ftp_conecta(hosty,usery,pwdy);
  if  ftp_conectado = 1 then
     begin
        if not (ftp_Existe_directorio(ruta_ppal_ftp+cliente_id, FTP1)) then
               ftp_crea_directorio(ruta_ppal_ftp+cliente_id);

        if not (ftp_Existe_directorio(ruta_ppal_ftp+cliente_id+dir_ped_ftp, FTP1)) then
             ftp_crea_directorio(ruta_ppal_ftp+cliente_id+dir_ped_ftp);

        if not (ftp_Existe_directorio(ruta_ppal_ftp+cliente_id+dir_alb_ftp, FTP1)) then
             ftp_crea_directorio(ruta_ppal_ftp+cliente_id+dir_alb_ftp);

        if not (ftp_Existe_directorio(ruta_ppal_ftp+cliente_id+dir_fac_ftp, FTP1)) then
             ftp_crea_directorio(ruta_ppal_ftp+cliente_id+dir_fac_ftp);

        if not (ftp_Existe_directorio(ruta_ppal_ftp+cliente_id+dir_ppte_ftp, FTP1)) then
             ftp_crea_directorio(ruta_ppal_ftp+cliente_id+dir_ppte_ftp);

        if not (ftp_Existe_directorio(ruta_ppal_ftp+cliente_id+dir_consumo_ftp, FTP1)) then
             ftp_crea_directorio(ruta_ppal_ftp+cliente_id+dir_consumo_ftp);
     end;
end;

procedure Tcliente_Web.guarda_alta_datos_usuario(nombre_cliente, usuario_web, pwd_usuario, email_destinatario, importe_min_ped:string);
var myfile:TextFile;
    fichero_log, upd_usuarios:ansistring;
    adocmd_upd:TADOCommand;

begin

  lee_dato_ini(ejecutable_ini,'rutas','ruta_y_fichero_log_pwd',fichero_log);
  lee_dato_ini(ejecutable_ini,'sql_tCliWeb','upd_usuarios',upd_usuarios);

  if FileExists(fichero_log) then
  begin
    AssignFile(myFile, fichero_log);
    append(myFile);
  end
  else
  begin
    AssignFile(myFile, fichero_log);
    rewrite(myFile);
    writeln(myfile, 'usuario_web' +char(9)+ 'nombre_cliente'+char(9)+'email_destinatario'+ char(9)+ 'pwd_usuario'+char(9)+'Fecha');
  end;
  writeln(myfile, usuario_web +char(9)+ nombre_cliente+char(9)+email_destinatario+ char(9)+ pwd_usuario+char(9)+DateTimeToStr(Date));
  CloseFile(myfile);

  ////////////////////////
  // Guarda registro de alta usuario
   try
     adocmd_upd:= TADOCommand.Create(nil);
     with adocmd_upd do
     begin
        Connection:=adoconnection_cliweb;
        ParamCheck:=true;
        CommandText:=upd_usuarios;
        Parameters[0].Value:= email_destinatario;
        Parameters[1].Value:= pwd_usuario;
        Parameters[2].Value:= GetloginName;
        Parameters[3].Value:= importe_min_ped;
        Parameters[4].Value:= usuario_web;

        Execute;
     end;
     adocmd_upd.Free;
   finally

   end;
  ////////////////////////
end;

function tcliente_web.envia_documento_outlook(nombre_cliente, usuario_web, pwd_usuario, email_destinatario, nombre_destinatario: string): boolean;
const
  olMailItem = 0;
var
  Outlook: OleVariant;
  vMailItem: variant;
  html_mail:widestring;
  fic_html_cli:ansistring;
begin

 lee_dato_ini(ejecutable_ini,'rutas_tCliWeb','ruta_fic_html_cli',fic_html_cli);
  try
    Outlook := GetActiveOleObject('Outlook.Application');
  except
    Outlook := CreateOleObject('Outlook.Application');
  end;
  html_mail:=carga_ficherowide(fic_html_cli, false);
  html_mail:= StringReplace(html_mail,':NOMBRE_CLIENTE', nombre_cliente,[rfReplaceAll]);
  html_mail:= StringReplace(html_mail,':USUARIO', usuario_web,[rfReplaceAll]);
  html_mail:= StringReplace(html_mail,':PWD_CLIENTE', pwd_usuario,[rfReplaceAll]);

  vMailItem := Outlook.CreateItem(olMailItem);
  vMailItem.Recipients.Add(email_destinatario);
  vMailItem.Subject := 'Web ALFA DYSER,S.L. ';
  vMailItem.HTMLBody := html_mail;

  //result := vMailItem.Send;
  //guarda_alta_datos_usuario(nombre_cliente, usuario_web, pwd_usuario, email_destinatario);

  VarClear(Outlook);

end;

//function envia_documento_indy(email: string; repre: String):boolean;
function TCliente_Web.envia_documento_directo_indy(nombre_cliente, usuario_web, pwd_usuario, email_destinatario, nombre_destinatario: string):boolean;
var
  USERNAMEy, PASSy, HOST1y, PORT1y: ansiString;
  PORT1: integer;
  IdMessage1:TIdMessage;
  IdSMTP1: TIdSMTP;
  html_mail:widestring;
  fic_html_cli:ansistring;
  EMAILORIGEN, CCo_mail:ansistring;

begin
  lee_dato_ini(ejecutable_ini,'rutas_tCliWeb','ruta_fic_html_cli',fic_html_cli);

  lee_dato_ini(ejecutable_ini, 'Cuenta_envio_correo', 'usernamey', USERNAMEy);
  lee_dato_ini(ejecutable_ini, 'Cuenta_envio_correo', 'passwordy', PASSy);
  lee_dato_ini(ejecutable_ini, 'Cuenta_envio_correo', 'hosty', HOST1y);
  lee_dato_ini(ejecutable_ini, 'Cuenta_envio_correo', 'porty', PORT1y);

  EMAILORIGEN := 'soporte_web@alfadyser.es';

  html_mail:=carga_ficherowide(fic_html_cli, false);
  html_mail:= StringReplace(html_mail,':NOMBRE_CLIENTE', nombre_cliente,[rfReplaceAll]);
  html_mail:= StringReplace(html_mail,':USUARIO', usuario_web,[rfReplaceAll]);
  html_mail:= StringReplace(html_mail,':PWD_CLIENTE', pwd_usuario,[rfReplaceAll]);

  IdMessage1:= TIdMessage.Create(nil);
  IdSMTP1:= TIdSMTP.Create(nil);
  IdMessage1.ContentType:='text/html';
  with IdMessage1 do
  begin
    Body.clear;
    Body.add(html_mail);
    From.Text := EMAILORIGEN;
    //Recipients.EMailAddresses := Desti.text+ ',' + CC.text+ ','+CCO.text;
    //Recipients.EMailAddresses := email_destinatario+','+'oscar@alfadyser.com';
    Recipients.EMailAddresses := email_destinatario;
    lee_dato_ini(ejecutable_ini, 'Envio_Mail', 'CCo_mail', CCo_mail);
    BccList.EMailAddresses :=CCo_mail;
    Subject := 'Web ALFA DYSER,S.L.';
    Priority := TidMessagePriority(mpHighest);
  end;

  with IdSMTP1 do
  begin
    // Configuro autentificaci�n.
    Username := desencripta(USERNAMEy, 1499);
    Password := desencripta(PASSy, 1499);
    Host := desencripta(HOST1y, 1499);
    Port := strtoint(desencripta(PORT1y, 1499));
    try
      Connect();
      authenticate;
      Send(IdMessage1);
      guarda_alta_datos_usuario(nombre_cliente, usuario_web, pwd_usuario, email_destinatario, inttostr(fpedidoMinimo));
    finally
      Disconnect;
    end;
  end;
  IdMessage1.clear;
  IdMessage1.Free;
  IdSMTP1.Free;
end;




function GenerarClave( iSilabas, iNumeros: Byte ): string;
const
  Consonante: array [0..19] of Char = ( 'b', 'c', 'd', 'f', 'g', 'h', 'j',
                                        'k', 'l', 'm', 'n', 'p', 'r', 's',
                                        't', 'v', 'w', 'x', 'y', 'z' );
  Vocal: array [0..4] of Char = ( 'a', 'e', 'i', 'o', 'u' );

  function Repetir( sCaracter: String; iVeces: Integer ): string;
  var
    i: Integer;
  begin
    Result := '';
    for i := 1 to iVeces do
      Result := Result + sCaracter;
  end;

var
  i: Integer;
  si, sf: Longint;
  n: string;
begin
  Result := '';
  Randomize;

  if iSilabas <> 0 then
    //for i := 1 to iSilabas do
    for i := 1 to 3 do
    begin
      //Result := Result + Consonante[Random(19)];
     // Result := Result + Vocal[Random(4)];
    end;

  if iNumeros = 1 then
    Result := Result + IntToStr(Random(9))
  else
    if iNumeros >= 2 then
    begin
      if iNumeros > 9 then
        iNumeros := 9;

      si     := StrToInt('1' + Repetir( '0', iNumeros - 1 ) );
      sf     := StrToInt( Repetir( '9', iNumeros ) );
      n      := FloatToStr( si + Random( sf ) );
      Result := Result + Copy( n, 0, iNumeros );
    end;
end;

function TCliente_Web.getRepresentante(cliente_id:string):string;
var adodataset_usuweb:TADODataSet;
i:integer;
strsql:AnsiString;
begin

    adodataset_usuweb := TADODataSet.Create(nil);
         with adodataset_usuweb do
         begin
            Active:=false;
            Connection:=adoconnection_cliweb;
            //CommandText:='select COUNT(xcliente_id) as grupo_dto from pl_cli_dtoart where xcliente_id=:CLI group by xcliente_id'
            ParamCheck:=true;
            lee_dato_ini(ejecutable_ini,'sql_tCliWeb','repre_cliente',strsql);
            CommandText:=strsql;
            Parameters[0].Value:= cliente_id;
            Active:=true;
            //Cli_grupo_dto:=adodataset_usuweb.FieldByName('xrepresentante_id').asstring;
            getRepresentante:= adodataset_usuweb.FieldByName('xrepresentante_id').asstring;
         end;
         adodataset_usuweb.Free;

end;


function TCliente_Web.Cli_grupo_dto(cliente_id:string):string;
var adodataset_usuweb:TADODataSet;
i:integer;
strsql:AnsiString;
begin
         adodataset_usuweb := TADODataSet.Create(nil);
         with adodataset_usuweb do
         begin
            Active:=false;
            Connection:=adoconnection_cliweb;
            //CommandText:='select COUNT(xcliente_id) as grupo_dto from pl_cli_dtoart where xcliente_id=:CLI group by xcliente_id'
            ParamCheck:=true;
            lee_dato_ini(ejecutable_ini,'sql_tCliWeb','cli_dto_art_grupo',strsql);
            CommandText:=strsql;
            Parameters[0].Value:= cliente_id;
            Active:=true;
            Cli_grupo_dto:=adodataset_usuweb.FieldByName('grupo_dto').asstring;
         end;
         adodataset_usuweb.Free;
end;

function TCliente_Web.guardar(u_web:TCliente_Web):boolean;
begin
//function actualiza_cliweb(activo:boolean; cliente_id, email, fpago_id, grupo_usuario_id, local_env_id, nombre,
//                          telefono, usuario_id, codigo_agrup :String; modificado:boolean):boolean;
  if existe_usuario_web(u_web.Usuario_id) then
  begin
      try
      begin
         actualiza_cliweb(u_web.xactivo, u_web.Cliente_id, AnsiLowerCase(u_web.Email), u_web.FPago, u_web.Grupo_Usuario_id, u_web.Local_envio_id, u_web.Nombre, u_web.Telefono, u_web.Usuario_id, u_web.Cod_agrup, u_web.pedido_minimo, true);
      end;
      except on E:Exception do raise  exception.create('Error al guardar usuario Web');
      end;
  end;
end;

function TLocalEnv_Web.getEmailLocalEnv(usuario, localizacion:String):String;
var adodataset_Localweb:Tadodataset;
var strsql:AnsiString;
begin

    adodataset_Localweb := TADODataSet.Create(nil);
    adodataset_Localweb.Connection:=adoconnection_local_env;
    with adodataset_Localweb do
      begin
         Active:=false;
         Connection:=adoconnection_local_env;
         //CommandText:= 'select coalesce(env.xemail, cli.xemail) as email , cli.xempresa_id, xlocal_id
         //               from pl_cli_env env, pl_clientes cli where cli.xempgen_id=env.xempresa_id
         //               and cli.xempresa_id= '+QuotedStr('AL')+ ' and cli.xcliente_id=env.xcliente_id and
         //               cli.xcliente_id= '+ QuotedStr(usuario)+' and env.xlocal_id= '+QuotedStr(localizacion);

         ParamCheck:=true;
         lee_dato_ini(ejecutable_ini, 'sql_tCliWeb', 'getEmail_localEnv', strsql);
         CommandText:=strsql;
         Parameters[0].Value:= 'AL';
         Parameters[1].Value:= usuario;
         parameters[2].Value:=localizacion;
         try
         begin
             Active:=true;
             getEmailLocalEnv:= fieldbyname('email').AsString;
         end
         except on E:Exception do Raise Exception.create('No existe conexi�n a la base de datos');

      end;
    end;
    adodataset_Localweb.Active:=false;
    adodataset_Localweb.Free;
end;


function TCliente_Web.publicar;
var adocommand_usuweb: TADOCommand;
  wf, sesion, password_cli: string;
  xml_cliente, xml_local_env: widestring;
  HTTPRIO_usuweb: THTTPRIO;
  i:integer;
  strsql:AnsiString;
  fic_xml_cli, fic_xml_local, envio_mail:Ansistring;
  WSDLLocationy, Servicey, Porty, Usernamey, pwdy, call_clientey, call_localizaciony :ansistring;
  WSDLLocation_alfa, Service_alfa, Port_alfa, Username_alfa, pwd_alfa, call_cliente, call_localizacion :ansistring;
begin
    publicar:=true;

    //HTTPRIO_usuweb:= THTTPRIO.Create(nil);
    //HTTPRIO_usuweb.WSDLLocation:='http://alfadyser.bendmas.com/api/soap/?wsdl';
    //HTTPRIO_usuweb.Service:='MagentoService';
    //HTTPRIO_usuweb.Port:='Mage_Api_Model_Server_HandlerPort';
    lee_dato_ini(ejecutable_ini, 'Envio_Mail', 'Envio_Mail',Envio_Mail);
    lee_dato_ini(ejecutable_ini, 'HTTPRIO_tCliWeb', 'WSDLLocationy',WSDLLocationy);
    lee_dato_ini(ejecutable_ini, 'HTTPRIO_tCliWeb', 'Servicey',Servicey);
    lee_dato_ini(ejecutable_ini, 'HTTPRIO_tCliWeb', 'Porty',Porty);
    lee_dato_ini(ejecutable_ini, 'HTTPRIO_tCliWeb', 'Usernamey',Usernamey);
    lee_dato_ini(ejecutable_ini, 'HTTPRIO_tCliWeb', 'pwdy',pwdy);
    lee_dato_ini(ejecutable_ini, 'HTTPRIO_tCliWeb', 'call_clientey',call_clientey);
    lee_dato_ini(ejecutable_ini, 'HTTPRIO_tCliWeb', 'call_localizaciony',call_localizaciony);

    WSDLLocation_alfa:= desencripta(WSDLLocationy, 1499);
    Service_alfa:= desencripta(Servicey, 1499);
    Port_alfa:=   desencripta(Porty, 1499);
    Username_alfa:=  desencripta(Usernamey, 1499);
    pwd_alfa:=  desencripta(pwdy, 1499);
    call_cliente:=  desencripta(call_clientey, 1499);
    call_localizacion:= desencripta(call_localizaciony,1499);

    HTTPRIO_usuweb:= THTTPRIO.Create(nil);
    HTTPRIO_usuweb.WSDLLocation:= WSDLLocation_alfa;
    HTTPRIO_usuweb.Service:=Service_alfa;
 //   HTTPRIO_usuweb.Port:=Port_alfa;
    HTTPRIO_usuweb.Port:='Port';


    lee_dato_ini(ejecutable_ini,'rutas_tCliWeb','ruta_fic_xml_cli',fic_xml_cli);
    lee_dato_ini(ejecutable_ini,'rutas_tCliWeb','ruta_fic_xml_local',fic_xml_local);

    xml_cliente:='';
    xml_cliente:=carga_ficherowide(fic_xml_cli, false);
    if activo = true then xml_cliente:= StringReplace(xml_cliente, '@xactivo', '1',[rfReplaceAll])
    else xml_cliente:= StringReplace(xml_cliente, '@xactivo', '0',[rfReplaceAll]);
    xml_cliente:= StringReplace(xml_cliente,'@xempresa', Empresa,[rfReplaceAll]);
    xml_cliente:= StringReplace(xml_cliente,'@xcliente_id', cliente_id,[rfReplaceAll]);
    xml_cliente:= StringReplace(xml_cliente,'@xnombre', ansireplacestr(nombre, '&',' '),[rfReplaceAll]);
    xml_cliente:= StringReplace(xml_cliente,'@xlocal_id', Local_envio_id,[rfReplaceAll]);
    xml_cliente:= StringReplace(xml_cliente,'@fpago_desc', FPago_desc,[rfReplaceAll]);
    xml_cliente:= StringReplace(xml_cliente,'@xgrupo_usuario_id', Grupo_Usuario_id,[rfReplaceAll]);
    xml_cliente:= StringReplace(xml_cliente,'@xgrupo_usuario_desc', Nombre_Grupo_Usuario,[rfReplaceAll]);
    xml_cliente:= StringReplace(xml_cliente,'@email', Email,[rfReplaceAll]);
    xml_cliente:= StringReplace(xml_cliente,'@telefono', Telefono,[rfReplaceAll]);
    xml_cliente:= StringReplace(xml_cliente,'@xusuario_id', Usuario_id,[rfReplaceAll]);
    xml_cliente:= StringReplace(xml_cliente,'@mail_repre', xemail_repre,[rfReplaceAll]);
    xml_cliente:= StringReplace(xml_cliente,'@cooperativa', cooperativa,[rfReplaceAll]);
    xml_cliente:= StringReplace(xml_cliente,'@pedido_minimo',inttostr(fpedidoMinimo) ,[rfReplaceAll]);

    // usuario nuevo, generar pwd desde magento
    if ((not Publicado) and (not Pwd_Cli)) then
    begin
        //xml_cliente:= StringReplace(xml_cliente,'@password', 'mage_auto_10',[rfReplaceAll]);
          password_cli:= GenerarClave(strtoint(copy(Usuario_id,1,5)),6);
          xml_cliente:= StringReplace(xml_cliente,'@password',password_cli,[rfReplaceAll]);
    end
    else if ((not Publicado) and (Pwd_Cli)) then
    begin
         password_cli:= GenerarClave(strtoint(copy(Usuario_id,1,5)), 6);
         xml_cliente:= StringReplace(xml_cliente,'@password',password_cli,[rfReplaceAll]);
    end
    else // usuario publicado, no es nuevo, pero queremos cambiar pwd
        if publicado and Pwd_Cli then
         begin
              password_cli:= GenerarClave(strtoint(copy(Usuario_id,1,5)), 6);
              xml_cliente:= StringReplace(xml_cliente,'@password',password_cli,[rfReplaceAll]);
         end
         else //usuario ya publicado y no se quieres cambiar pwd
            if Publicado and not Pwd_Cli then
             begin
                xml_cliente:= StringReplace(xml_cliente,'@password', '',[rfReplaceAll]);
             end;



    if (HTTPRIO_usuweb as  Mage_Api_Model_Server_HandlerPortType).endSession(sesion) then
    sesion := (HTTPRIO_usuweb as Mage_Api_Model_Server_HandlerPortType).login(Username_alfa,pwd_alfa);
    wf :=  (HTTPRIO_usuweb as Mage_Api_Model_Server_HandlerPortType).call(sesion, call_cliente, xml_cliente);

    if AnsiContainsText(wf,'correctamente') then
     begin
         //if publicado and
         if Pwd_Cli then
         begin
              if envio_mail='1' then envia_documento_directo_indy(ansireplacestr(nombre, '&',' '), Usuario_id, password_cli, AnsiLowerCase(Email), Nombre)
              else envia_documento_outlook(ansireplacestr(nombre, '&',' '), Usuario_id, password_cli, AnsiLowerCase(Email), Nombre);
         end;
         adocommand_usuweb:= TADOCommand.Create(nil);
         adocommand_usuweb.Connection:= adoconnection_cliweb;
         with adocommand_usuweb do
         begin
          //CommandText:='update web_usuarios set xmodificado= 0 , xpublicado=-1 where xusuario_id= '+
          //              QuotedStr(usuario_id);
           ParamCheck:=true;
           lee_dato_ini(ejecutable_ini,'sql_tCliWeb','upd_publicar_cli',strsql);
           CommandText:=strsql;
           Parameters[0].Value:= Usuario_id;
            try
               Execute;
             except
                begin
                  publicar:=false;
                  Raise Exception.create(wf);
                end;
             end;
         end;

         for I := 0 to xlocalizaciones.dataset.RecordCount-1 do
         begin
                xml_local_env:='';
                xml_local_env:=carga_ficherowide(fic_xml_local, false);
                xml_local_env:= StringReplace(xml_local_env, '@xusuario_id', Localizaciones.locs[i].Usuario_id,[rfReplaceAll]);
                xml_local_env:= StringReplace(xml_local_env, '@xlocal_env_id', Localizaciones.locs[i].Local_envio_id,[rfReplaceAll]);
                xml_local_env:= StringReplace(xml_local_env, '@xnombre_loc', ansireplacestr(Localizaciones.locs[i].Nombre_loc,'&', ' ') ,[rfReplaceAll]);
                xml_local_env:= StringReplace(xml_local_env, '@xpais_id', (Localizaciones.locs[i].Pais_desc),[rfReplaceAll]);
                xml_local_env:= StringReplace(xml_local_env, '@xprovincia_id', Localizaciones.locs[i].Provincia_desc,[rfReplaceAll]);
                xml_local_env:= StringReplace(xml_local_env, '@xpoblacion', Localizaciones.locs[i].Poblacion,[rfReplaceAll]);
                xml_local_env:= StringReplace(xml_local_env, '@xdomicilio', Localizaciones.locs[i].Domicilio,[rfReplaceAll]);
                xml_local_env:= StringReplace(xml_local_env, '@xcod_postal', Localizaciones.locs[i].CodigoPostal,[rfReplaceAll]);
                if i=0 then  xml_local_env:= StringReplace(xml_local_env, '@shipping','shipping',[rfReplaceAll])
                else  xml_local_env:= StringReplace(xml_local_env, '@shipping', '',[rfReplaceAll]);

                wf:='';
                if (HTTPRIO_usuweb as Mage_Api_Model_Server_HandlerPortType).endSession(sesion) then
                sesion := (HTTPRIO_usuweb as Mage_Api_Model_Server_HandlerPortType).login(Username_alfa,pwd_alfa);
                wf :=  (HTTPRIO_usuweb as Mage_Api_Model_Server_HandlerPortType).call(sesion, call_localizacion, xml_local_env);

                adocommand_usuweb.CommandText:='';
                if AnsiContainsText(wf,'correctamente') then
                begin
                     with adocommand_usuweb do
                     begin
                        //CommandText:='update web_local_env set xmodificado= 0 where xusuario_id= '+
                        //              QuotedStr(Usuario_id)+ ' and xlocal_id= '+
                        //              QuotedStr(Localizaciones.locs[i].Local_envio_id);
                         ParamCheck:=true;
                         lee_dato_ini(ejecutable_ini,'sql_tCliWeb','upd_publicar_local',strsql);
                         CommandText:=strsql;
                         Parameters[0].Value:= Usuario_id;
                         parameters[1].Value:= Localizaciones.locs[i].Local_envio_id;
                         try
                           Execute;
                         except
                             begin
                               publicar:=false;
                               Raise Exception.create(wf);
                             end;
                        end;
                     end;
                end
                else
                begin
                  publicar:=false;
                  Raise Exception.create(wf);
                end
         end;
         //Crear_directorios(copy(Usuario_id,1,5));
         adocommand_usuweb.Free;
     end
     else
     begin
         publicar:=false;
         Raise Exception.create(wf);
     end;
end;


function TCliente_Web.carga_combo_localenv():Tstringlist;
var adodataset_localEnv:TADODataSet;
lista_Localizaciones : Tstringlist;
i:integer;
strsql:AnsiString;
begin
    adodataset_localEnv := TADODataSet.Create(nil);
    lista_Localizaciones:= TStringList.Create();
    adodataset_localEnv.ParamCheck:=true;
    with adodataset_localEnv do
    begin
       Active:=false;
       Connection:=adoconnection_cliweb;
       //CommandText:= ' Select xlocal_id, xnombre_loc from web_local_env where xempresa_id='+
       //                 QuotedStr(xempresa_id)+' and xusuario_id='+QuotedStr(Usuario_id);
       lee_dato_ini(ejecutable_ini,'sql_tCliWeb','carga_cbo_localenv',strsql);
       CommandText:=strsql;
       Parameters[0].Value:= xempresa_id;
       Parameters[1].Value:= Usuario_id;
       Active:=true;
       if RecordCount>0 then
          First;
          for i:=0 to RecordCount-1 do
          begin
            lista_Localizaciones.Add(adodataset_localEnv.FieldByName('xlocal_id').AsString +'-'+adodataset_localEnv.FieldByName('xnombre_loc').AsString);
            next;
          end;
    end;
    adodataset_localEnv.Active:=false;
    adodataset_localEnv.Free;
    carga_combo_localenv:=lista_Localizaciones;
end;


function TCliente_Web.existe_usuario_web(usuario_id:String):boolean;
var adodataset_usuweb:TADODataSet;
i:integer;
strsql:AnsiString;
begin
         adodataset_usuweb := TADODataSet.Create(nil);
         with adodataset_usuweb do
         begin
            Active:=false;
            Connection:=adoconnection_cliweb;
            //CommandText:=' select xusuario_id from web_usuarios where xusuario_id='+ QuotedStr(usuario_id);
            ParamCheck:=true;
            lee_dato_ini(ejecutable_ini,'sql_tCliWeb','existe_usuario_web',strsql);
            CommandText:=strsql;
            Parameters[0].Value:= Usuario_id;
            Active:=true;
            if RecordCount>0 then  existe_usuario_web:=true
            else existe_usuario_web:=false;
         end;
         adodataset_usuweb.Free;
end;

function TGrupo_usuarios.devuelve_grupos_usuarios():Tstringlist;
var adodataset_grupoweb:TADODataSet;
lista_grupo_usuarios_id : Tstringlist;
i:integer;
strsql:AnsiString;
begin
    adodataset_grupoweb := TADODataSet.Create(nil);
    lista_grupo_usuarios_id:= TStringList.Create();
    with adodataset_grupoweb do
    begin
       Active:=false;
       Connection:=adoconnection_grupo_usuweb;
       //CommandText:= ' Select xnombre,  xgrupo_usuario_id from web_grupo_usuarios where xempresa_id='+
       //                QuotedStr(xempresa_id);
       ParamCheck:=true;
       lee_dato_ini(ejecutable_ini,'sql_tCliWeb','devuelve_grupos_usuarios',strsql);
       CommandText:=strsql;
       Parameters[0].Value:= xempresa_id;
       Active:=true;
       if RecordCount>0 then
          First;
          for i:=0 to RecordCount-1 do
          begin
            lista_grupo_usuarios_id.Add(adodataset_grupoweb.FieldByName('xgrupo_usuario_id').AsString +'-'+adodataset_grupoweb.FieldByName('xnombre').AsString);
            next;
          end;
    end;
    adodataset_grupoweb.Active:=false;
    adodataset_grupoweb.Free;
    devuelve_grupos_usuarios:=lista_grupo_usuarios_id;

end;

function TLocalEnv_Web.Carga_Local_env(local_id, usuario_id:string):boolean;
var adodataset_Localweb:TADODataSet;
strsql:AnsiString;
begin
    Carga_Local_env:=true;
    adodataset_Localweb := TADODataSet.Create(nil);
    if adoconnection_local_env.Connected=false then
    begin
      Carga_Local_env:=false;
      Raise Exception.create('No existe conexi�n a la base de datos')
    end
    else
    begin
      with adodataset_Localweb do
      begin
         Active:=false;
         Connection:=adoconnection_local_env;
         //CommandText:= ' exec Web_Carga_Local_env ' + QuotedStr(local_id)+ ',' +QuotedStr(usuario_id);
         ParamCheck:=true;
         lee_dato_ini(ejecutable_ini,'sql_tCliWeb','Carga_Local_env',strsql);
         CommandText:=strsql;
         Parameters[0].Value:= local_id;
         Parameters[1].Value:= usuario_id;
         try
             Active:=true;
             if RecordCount>0 then
             begin
                  SetCod_postal(fieldbyname('xcod_postal').AsString);
                  SetDomicilio(AnsiReplaceStr((fieldbyname('xdomicilio').AsString),'"',''''));
                  SetEmpresa_id(fieldbyname('xempresa_id').AsString);
                  SetLocal_Envio_id(fieldbyname('xlocal_id').AsString);
                  SetPais_id(fieldbyname('xpais_id').AsString);
                  SetPoblacion(ansireplacestr((fieldbyname('xpoblacion').AsString),'"',''''));
                  SetProvincia_id(fieldbyname('xprovincia_id').AsString);
                  SetUsuario_id(fieldbyname('xusuario_id').AsString);
                  SetNombre_loc(ansireplacestr((fieldbyname('xnombre_loc').AsString),'"',''''));
                  setPais_desc(fieldbyname('xpais_desc').asstring);
                  setProvincia_desc(fieldbyname('xprovincia_desc').AsString);
                  Carga_Local_env:=true;
             end
             else Carga_Local_env:=false;
             except on E:Exception do Carga_Local_env:=false;
             end;
      end;
    end;
    adodataset_Localweb.Active:=false;
    adodataset_Localweb.Free;
end;

function TLocalEnv_Web.Inserta_Local_env(usuario_id:string):boolean;
var Adocommand_Local_Env: TADOCommand;
strsql:AnsiString;
begin

    Inserta_Local_env:=true;
    Adocommand_Local_Env :=  TADOCommand.Create(nil);
    begin
        with Adocommand_Local_Env do
        begin
            Connection:= adoconnection_local_env;
            //CommandText:= 'exec Web_Insert_Local_Env '+QuotedStr(xempresa_id)+','+QuotedStr(usuario_id);
            ParamCheck:=true;
            lee_dato_ini(ejecutable_ini,'sql_tCliWeb','Inserta_Local_env',strsql);
            CommandText:=strsql;
            Parameters[0].Value:= xempresa_id;
            Parameters[1].Value:= usuario_id;
            try
                Execute;
            except
              on E:Exception do Inserta_Local_env:=false;
            end;
         end;
    end;
     Adocommand_Local_Env.Free;
end;

function TLocalEnv_Web.Actualiza_local_env(cod_postal, domicilio, local_id, pais_id, poblacion_id, provincia_id, usuario_id, nombre_loc:String):boolean;
var Adocommand_Local_Env: TADOCommand;
strsql:AnsiString;
begin
    Actualiza_local_env:=true;
    Adocommand_Local_Env :=  TADOCommand.Create(nil);
    with Adocommand_Local_Env do
    begin
        Connection:= adoconnection_local_env;
        //CommandText:= ' exec Web_Upd_Local_env '+QuotedStr(cod_postal)+','+ QuotedStr(domicilio)+','+
        //                QuotedStr(xempresa_id) +','+QuotedStr(local_id)+','+ QuotedStr(pais_id)+','+
        //                QuotedStr(poblacion_id)+','+ QuotedStr(provincia_id)+','+ QuotedStr(usuario_id)+','+
        //                QuotedStr(nombre_loc);
        ParamCheck:=true;
        lee_dato_ini(ejecutable_ini,'sql_tCliWeb','Actualiza_local_env',strsql);
        CommandText:=strsql;
        Parameters[0].Value:= cod_postal;
        Parameters[1].Value:= domicilio;
        Parameters[2].Value:= xempresa_id;
        Parameters[3].Value:= local_id;
        Parameters[4].Value:= pais_id;
        Parameters[5].Value:= poblacion_id;
        Parameters[6].Value:= provincia_id;
        Parameters[7].Value:= usuario_id;
        Parameters[8].Value:= nombre_loc;

        try
          Execute;
        except
          on E:Exception do Actualiza_local_env:=false;
        end;
    end;
    Adocommand_Local_Env.Free
end;

function TGrupo_usuarios.Carga_grupo(grupo_usuario:string):boolean;
var adodataset_grupoweb:TADODataSet;
strsql:AnsiString;
begin
    Carga_grupo:=true;
    adodataset_grupoweb := TADODataSet.Create(nil);
    with adodataset_grupoweb do
    begin
       Active:=false;
       Connection:=adoconnection_grupo_usuweb;
       //CommandText:= ' Select * from web_grupo_usuarios where xempresa_id='+QuotedStr(xempresa_id)+'
       //               and xgrupo_usuario_id = '+QuotedStr(grupo_usuario);
       ParamCheck:=true;
       lee_dato_ini(ejecutable_ini,'sql_tCliWeb','Carga_grupo',strsql);
       CommandText:=strsql;
       Parameters[0].Value:= xempresa_id;
       Parameters[1].Value:= grupo_usuario;
       try
           Active:=true;
           if RecordCount>0 then SetNombre(fieldbyname('xnombre').AsString)
           else Carga_grupo:=false;
       except on E:Exception do Carga_grupo:=false;
       end;
    end;
    adodataset_grupoweb.Active:=false;
    adodataset_grupoweb.Free;
end;


function TGrupo_usuarios.Actualiza_grupo(grupo_usuario, nombre:String):boolean;
var Adocommand_grupo_usuweb: TADOCommand;
strsql:AnsiString;
begin
    Actualiza_grupo:=true;
    Adocommand_grupo_usuweb :=  TADOCommand.Create(nil);
    with Adocommand_grupo_usuweb do
    begin
        Connection:= adoconnection_grupo_usuweb;
        //CommandText:= 'exec Web_Upd_Grupo_Usuarios '+QuotedStr(xempresa_id)+','+ QuotedStr(grupo_usuario)+','+
        //                QuotedStr(nombre);
        ParamCheck:=true;
        lee_dato_ini(ejecutable_ini,'sql_tCliWeb','Actualiza_grupo',strsql);
        CommandText:=strsql;
        Parameters[0].Value:= xempresa_id;
        Parameters[1].Value:= grupo_usuario;
        parameters[2].Value:=nombre;
        try
          Execute;
        except
          on E:Exception do Actualiza_grupo:=false;
        end;
    end;
    Adocommand_grupo_usuweb.Free;
end;

function TGrupo_usuarios.Inserta_Grupo(grupo_usuario,nombre:string):boolean;
var Adocommand_grupo_usuweb: TADOCommand;
strsql:AnsiString;
begin
    Inserta_Grupo:=true;

    Adocommand_grupo_usuweb :=  TADOCommand.Create(nil);
    with Adocommand_grupo_usuweb do
    begin
        Connection:= adoconnection_grupo_usuweb;
        //CommandText:='exec Web_Insert_Grupo_Usuarios '+QuotedStr(xempresa_id)+','+ QuotedStr(grupo_usuario)+','+
        //            QuotedStr(nombre);
        ParamCheck:=true;
        lee_dato_ini(ejecutable_ini,'sql_tCliWeb','Inserta_Grupo',strsql);
        CommandText:=strsql;
        Parameters[0].Value:= xempresa_id;
        Parameters[1].Value:= grupo_usuario;
        parameters[2].Value:=nombre;
        try
            Execute;
        except
          on E:Exception do Inserta_Grupo:=false;
        end;
     end;
     Adocommand_grupo_usuweb.Free;
end;

function TCliente_Web.verifica_Local_env(local_env_id:string):boolean;
var Adodataset_grupo_usuweb: TADODataSet;
strsql:AnsiString;
begin
     verifica_Local_env:=true;
     Adodataset_grupo_usuweb:= TADODataSet.Create(nil);
     with Adodataset_grupo_usuweb do
     begin
        Connection:=adoconnection_cliweb;
        //CommandText := ' Select count(*) existe from web_local_env where xempresa_id = ' + QuotedStr(Empresa)+
        //               ' and xlocal_id= '+QuotedStr(local_env_id);
        ParamCheck:=true;
        lee_dato_ini(ejecutable_ini,'sql_tCliWeb','verifica_Local_env',strsql);
        CommandText:=strsql;
        Parameters[0].Value:= xempresa_id;
        Parameters[1].Value:= local_env_id;
         try
           Adodataset_grupo_usuweb.Active:=true;
         except
          on E:Exception do verifica_Local_env:=false;
        end;
     end;
       if (Adodataset_grupo_usuweb.fieldbyname('existe').Asinteger<1)then
          Raise Exception.create('No existe la localizaci�n de env�o indicada');
     Adodataset_grupo_usuweb.Free;
end;

function TCliente_Web.verifica_grupo_cliente(grupo_usuario_id:String):boolean;
var Adodataset_grupo_usuweb: TADODataSet;
strsql:AnsiString;
begin
     verifica_grupo_cliente:=true;
     Adodataset_grupo_usuweb:= TADODataSet.Create(nil);
     with Adodataset_grupo_usuweb do
     begin
         Connection:=adoconnection_cliweb;
        //CommandText := 'Select count(*) existe from web_grupo_usuarios where xempresa_id = ' + QuotedStr(Empresa)+ '
        //                and xgrupo_usuario_id= '+QuotedStr(grupo_usuario_id);
        ParamCheck:=true;
        lee_dato_ini(ejecutable_ini,'sql_tCliWeb','verifica_grupo_cliente',strsql);
        CommandText:=strsql;
        Parameters[0].Value:= xempresa_id;
        Parameters[1].Value:= grupo_usuario_id;
         try
           Adodataset_grupo_usuweb.Active:=true;
         except
          on E:Exception do verifica_grupo_cliente:=false;
        end;
     end;
       if (Adodataset_grupo_usuweb.fieldbyname('existe').Asinteger<1)then
          Raise Exception.create('No existe el grupo de usuarios indicado');
     Adodataset_grupo_usuweb.Free;
end;

constructor TCliente_Web.create(adoconnection:Tadoconnection);
begin
    if adoconnection.Connected=false then
      Raise Exception.create('No existe conexi�n a la base de datos')
    else
    begin
      adoconnection_cliweb:=Adoconnection;
      xempresa_id:='al';
    end;
end;

constructor TLocalEnv_Web.create( adoconnection:Tadoconnection);
begin
    if adoconnection.Connected=false then
      Raise Exception.create('No existe conexi�n a la base de datos')
    else
    begin
        adoconnection_local_env:=Adoconnection;
        xempresa_id:='al';
    end;
end;

constructor TGrupo_usuarios.create(adoconnection:TADOConnection);
begin
   if adoconnection.Connected=false then
      Raise Exception.create('No existe conexi�n a la base de datos')
    else
    begin
       adoconnection_grupo_usuweb:=adoconnection;
       xempresa_id:='al';
    end;
end;

function TCliente_Web.actualiza_cliweb(activo:boolean; cliente_id, email, fpago_id, grupo_usuario_id, local_env_id, nombre, telefono, usuario_id, codigo_agrup :String; importe_pedido_minimo:integer; modificado:boolean):boolean;
var Adocommand_upd_usuarios: TADOCommand;
strsql:AnsiString;
begin
    if verifica_grupo_cliente(grupo_usuario_id) and verifica_Local_env(local_env_id) then
    begin
        actualiza_cliweb:=true;
        Adocommand_upd_usuarios :=  TADOCommand.Create(nil);
        with Adocommand_upd_usuarios do
        begin
           Connection:= adoconnection_cliweb;
{
             CommandText:= 'exec Web_Upd_Usuarios '+QuotedStr(BoolToStr(activo))+','+ QuotedStr(cliente_id)+','+
             QuotedStr(email)+','+ QuotedStr(Empresa)+','+ QuotedStr(fpago_id)+','+ QuotedStr(grupo_usuario_id)+','+
             QuotedStr(local_env_id)+','+ QuotedStr(nombre)+','+ QuotedStr(telefono)+','+ QuotedStr(usuario_id)+','+
             QuotedStr(codigo_agrup)+','+ QuotedStr(BoolToStr(modificado));
}
            ParamCheck:=true;
            lee_dato_ini(ejecutable_ini,'sql_tCliWeb','actualiza_cliweb',strsql);
            CommandText:=strsql;
            if activo then  Parameters[0].Value:=( '-1') else Parameters[0].Value:= ( '0');
            Parameters[1].Value:= copy(cliente_id,1,5);
            Parameters[2].Value:= (AnsiLowerCase(email));
            parameters[3].Value:= (Empresa);
            Parameters[4].Value:= (fpago_id);
            Parameters[5].Value:= (grupo_usuario_id);
            Parameters[6].Value:= (local_env_id);
            Parameters[7].Value:= (nombre);
            Parameters[8].Value:= (telefono);
            Parameters[9].Value:= (usuario_id);
            Parameters[10].Value:= codigo_agrup;
            Parameters[11].Value:= (importe_pedido_minimo);

            if modificado then Parameters[12].Value:= ( '-1') else Parameters[12].Value:= ('0');

            try
              Execute;
            except
              on E:Exception do actualiza_cliweb:=false;
            end;
        end;
        Adocommand_upd_usuarios.Free;
    end;
end;

procedure TCliente_Web.insertar_cliweb(cliente_id,grupo_usuario_id,local_id:string);
var Adocommand_cliweb: TADOCommand;
strsql:AnsiString;
cliente:string;
begin
    Adocommand_cliweb :=  TADOCommand.Create(nil);
    with Adocommand_cliweb do
    begin
        Connection:= adoconnection_cliweb;
        //CommandText:= 'exec Web_Usuario '+QuotedStr(cliente_id)+','+QuotedStr(xempresa_id)+','+
        //                QuotedStr(grupo_usuario_id)+','+ QuotedStr(local_id);
        ParamCheck:=true;
        lee_dato_ini(ejecutable_ini,'sql_tCliWeb','insertar_cliweb',strsql);
        CommandText:=strsql;
        Parameters[0].Value:= cliente_id;
        Parameters[1].Value:= xempresa_id;

        cliente:=Copy( cliente_id, 0, 5);
        //// DETERMINA SI EL CLIENTE PERTENECE A UN GRUPO CON DTO
        Parameters[2].Value:= cli_grupo_dto(cliente_id);
        //Parameters[2].Value:= grupo_usuario_id;
        parameters[3].Value:= local_id;
        Execute;
    end;
    Adocommand_cliweb.free;
end;

procedure TCliente_Web.crear_usuarioweb(usuario_id,local_id, xactivo, grupo_usuario, email:string);
var Adocommand_usuweb: TADOCommand;
strsql:AnsiString;
begin
//falta Comprobar que existe la localizaci�n de envio y el grupo de usuario.
    Adocommand_usuweb :=  TADOCommand.Create(nil);
    with Adocommand_usuweb do
    begin
        Connection:= adoconnection_cliweb;
        try
         begin
            CommandText:='';
            //CommandText:= 'exec Web_insert_UsuLocalEnv '+QuotedStr('al')+','+QuotedStr(usuario_id)+','+
            //               QuotedStr(local_id)+','+QuotedStr(xactivo)+','+QuotedStr(grupo_usuario);
            ParamCheck:=true;
            lee_dato_ini(ejecutable_ini,'sql_tCliWeb','crear_usuarioweb',strsql);
            CommandText:=strsql;
            Parameters[0].Value:= xempresa_id;
            Parameters[1].Value:= usuario_id;
            Parameters[2].Value:= local_id;
            parameters[3].Value:= xactivo;
            parameters[4].Value:= grupo_usuario;
            Execute;
         end
        except on E:Exception do Raise Exception.create(' Error al Crear USUARIO del cliente.');
        end;
    end;
    Adocommand_usuweb.free;
end;

procedure TCliente_Web.SetActivo(activo: boolean);
begin
  // a�adir al objeto los valores de los controles
  xactivo := activo;
end;


procedure TCliente_Web.setPedido_minimo(fpedidoMinimo:integer);
begin
  // a�adir al objeto los valores de los controles
  pedido_minimo := fpedidoMinimo;
end;

procedure TCliente_Web.SetCliente_id(cliente_id: string);
begin
  if not TRegEx.IsMatch(cliente_id, '^\d\d\d\d\d$') then
      Raise Exception.create('C�digo de Cliente no permitido')
  else
  xcliente_id := cliente_id;
end;

procedure TCliente_Web.SetEmail(email: string);
var regexp_email:AnsiString;
begin
///   \ ^ [A-Z0-9._ %+-]+@[ A-Z0-9 .-] + \. [AZ] {2,4} \ $
   //regexp_email:= '[\w\d\-\.]+@[\w\d\-]+(\.[\w\d\-]+)+';

   lee_dato_ini(ejecutable_ini,'regexp_email', 'regexp_email', regexp_email );
   email:=AnsiLowerCase(email);
   if Length(email)>80 then
    Raise Exception.create('El Email no debe superar los 80 caracteres')
   else
    if not  TRegEx.IsMatch(email, regexp_email) then
    begin
     Raise Exception.create('El Email no es correcto - '+ email);
    end
    else  xemail := email;
end;

procedure TCliente_Web.SetEmpresa_id(empresa_id: string);
begin
  xempresa_id := empresa_id;
end;

procedure TCliente_Web.SetFPago_id(fpago_id: string);
begin
   if Length(fpago_id)>4 then
    Raise Exception.create('El valor del Identificador de Forma de Pago no debe superar los 4 caracteres')
   else xfpago_id := fpago_id;
end;

procedure TCliente_Web.SetFPago_desc(fpago_desc: string);
begin
   xfpago_desc := fpago_desc;
end;

procedure TCliente_Web.Setgrupo_usuario_id(grupo_usuario_id: string);
begin
   if Length(grupo_usuario_id)>4 then
    Raise Exception.create('El valor del Identificador de Grupo Usuario no debe superar los 4 caracteres')
   else xgrupo_usuario_id := grupo_usuario_id;
end;

procedure TCliente_Web.SetLocal_Envio_id(local_envio_id: string);
begin
   if Length(local_envio_id)>4 then
    Raise Exception.create('El valor del Identificador de Localizaci�n Env�o no debe superar los 4 caracteres')
   else xlocal_id := local_envio_id;
end;

procedure TCliente_Web.SetNombre(nombre: string);
begin
   if Length(nombre)>80 then
    Raise Exception.create('El valor del Nombre no debe superar los 80 caracteres')
   //else  xnombre := ansireplacestr(nombre, '&',' ');
   else  xnombre := nombre;
end;

procedure TCliente_Web.SetTelefono(telefono: string);
begin
   if Length(telefono)>40 then
    Raise Exception.create('El valor del Tel�fono no debe superar los 40 caracteres')
   else  xtelefono := telefono;
end;

procedure TCliente_Web.SetUsuario_id(usuario_id: string);
var adodataset_cliweb:TADODataSet;
strsql, str_sql_loc:AnsiString;
i:integer;
begin
  // a�adir al objeto los valores de los controles
  if usuario_id=''  then xusuario_id:=xcliente_id
  else xusuario_id := usuario_id;

   adodataset_cliweb := TADODataSet.Create(nil);
    with adodataset_cliweb do
    begin
       Active:=false;
       Connection:=adoconnection_cliweb;

       //CommandText:='SetUsuario_id= exec Web_Carga_Usuario '+ QuotedStr(usuario_id);
       ParamCheck:=true;
       lee_dato_ini(ejecutable_ini,'sql_tCliWeb','SetUsuario_id',strsql);
       CommandText:=strsql;
       Parameters[0].Value:= usuario_id;
       try
          try
           Active:=true;
           if RecordCount>0 then
              begin
                case fieldbyname('xactivo').AsInteger of
                  -1 : xactivo:=true;
                   0 : xactivo:=false;
                end;
                SetCliente_id(fieldbyname('xcliente_id').AsString);
                SetFPago_id(fieldbyname('xfpago_id').AsString);
                setFpago_desc(fieldbyname('xfpago_desc').AsString);
                Setgrupo_usuario_id(fieldbyname('xgrupo_usuario_id').AsString);
                SetLocal_Envio_id(fieldbyname('xlocal_id').AsString);
                SetNombre(ansireplacestr((fieldbyname('xnombre').AsString),'"',''''));
                SetTelefono(fieldbyname('xtelefono').AsString);
                //SetUsuario_id(fieldbyname('xusuario_id').AsString);
                case fieldbyname('xmodificado').AsInteger of
                  -1 : xmodificado:=true;
                   0 : xmodificado:=false;
                end;
                case fieldbyname('xpublicado').AsInteger of
                  -1 : xpublicado :=true;
                   0 : xpublicado:=false;
                end;
                SetNombre_Grupo_Usuario(fieldbyname('xnombre_grupo_usuario').asstring);
                SetEmail(fieldbyname('xemail').AsString);

                SetEmailPwd(fieldbyname('xemail_pwd').AsString);
                SetPwd_Enviado(fieldbyname('xpwd_enviado').AsString);
                SetFecha_Envio_pwd(fieldbyname('xfecha_envio_ult_pwd').AsString);
                SetUsuario_Envio_pwd(fieldbyname('xusuario_envio_pwd').AsString);

                setEmail_repre(fieldbyname('xemail_repre').asstring);
                setRepresentante_id(fieldbyname('xrepresentante_id').asstring);
                setCooperativa(fieldbyname('cooperativa').asstring);
                setPedido_minimo(FieldByName('importe_ped_min').asinteger);
              end
          except Raise Exception.create('Usuario con datos incorrectos:'+ '  '+adodataset_cliweb.fieldbyname('xcliente_id').AsString + '-'+ adodataset_cliweb.fieldbyname('xnombre').AsString)

          end;
       finally
       //creamos las localizaciones de envio
        xlocalizaciones:= Tlocalizaciones.Create();
        xlocalizaciones.dataset:= TADODataSet.Create(nil);
        if xlocalizaciones.dataset.Active then xlocalizaciones.dataset.Active:=false;
        xlocalizaciones.dataset.Connection:=adoconnection_cliweb;

        //xlocalizaciones.dataset.CommandText:= 'exec Web_Carga_LocalEnv ' + QuotedStr(usuario_id);
        localizaciones.dataset.ParamCheck:=true;
        lee_dato_ini(ejecutable_ini,'sql_tCliWeb','SetUsuario_id_localizaciones',str_sql_loc);
        localizaciones.dataset.CommandText:=str_sql_loc;
        localizaciones.dataset.Parameters[0].Value:= usuario_id;
        xlocalizaciones.dataset.Active:=true;

        if xlocalizaciones.dataset.RecordCount>0 then
         begin
           SetLength(xlocalizaciones.locs, xlocalizaciones.dataset.RecordCount);
           xlocalizaciones.dataset.First;
           for i:=0 to xlocalizaciones.dataset.RecordCount-1 do
           begin
              xlocalizaciones.locs[i]:= TLocalEnv_Web.create(adoconnection_cliweb);
              with  xlocalizaciones.locs[i] do
              begin
                 CodigoPostal:=ansireplacestr(xlocalizaciones.dataset.FieldByName('xcod_postal').AsString,'"','''');
                 domicilio:=ansireplacestr(xlocalizaciones.dataset.FieldByName('xdomicilio').asstring,'"','''');
                 Empresa_id:=xlocalizaciones.dataset.FieldByName('xempresa_id').asstring;
                 Local_envio_id:=xlocalizaciones.dataset.FieldByName('xlocal_id').asstring;
                 Pais_id:= xlocalizaciones.dataset.FieldByName('xpais_id').asstring;
                 Poblacion:=ansireplacestr(xlocalizaciones.dataset.FieldByName('xpoblacion').asstring,'"','''');
                 Provincia_id:= xlocalizaciones.dataset.FieldByName('xprovincia_id').asstring;
                 Provincia_desc:= xlocalizaciones.dataset.FieldByName('xprovincia_desc').asstring;
                 Usuario_id:=  xlocalizaciones.dataset.FieldByName('xusuario_id').asstring;
                 Nombre_loc:= ansireplacestr(xlocalizaciones.dataset.FieldByName('xnombre_loc').asstring,'"','''');
                 Pais_desc:= xlocalizaciones.dataset.FieldByName('xpais_desc').asstring;
                 Provincia_desc:= xlocalizaciones.dataset.FieldByName('xprovincia_desc').asstring;
                 xlocalizaciones.dataset.Next;
              end;
           end;
         end;
        end;
    end;
    adodataset_cliweb.Active:=false;
    adodataset_cliweb.Free;
end;


procedure TCliente_Web.setEmail_repre(email_repre:string);
begin
    xemail_repre:=email_repre;
end;

procedure TCliente_Web.setRepresentante_id(representante_id:string);
begin
    xrepresentante_id:=representante_id;
end;

procedure TCliente_Web.setCooperativa(fcooperativa:string);
begin
    cooperativa:=fcooperativa;
end;

procedure TCliente_Web.setEmailPwd(email:string);
begin
    xemail_pwd:=email;
end;

procedure TCliente_Web.setPwd_Enviado(pwd_enviado:string);
begin
    xpwd_enviado:=pwd_enviado;
end;

procedure TCliente_Web.setFecha_Envio_pwd(fecha_envio_pwd:String);
begin
    xfecha_envio_ult_pwd:=fecha_envio_pwd;
end;

procedure TCliente_Web.setUsuario_Envio_pwd(usuario_envio_pwd:string);
begin
   xusuario_envio_pwd:=usuario_envio_pwd;
end;

procedure TCliente_Web.SetCod_agrup(cod_agrup:string);
begin
  xcod_agrup:= cod_agrup;
end;

procedure TCliente_Web.SetModificado(modificado: boolean);
begin
  // a�adir al objeto los valores de los controles
  xmodificado := modificado;
end;

procedure TCliente_Web.SetPublicado(publicado:boolean);
begin
  xpublicado:=publicado;
end;

procedure TCliente_Web.SetPwd_Cli(pwd_cli:boolean) ;
begin
  xpwd_cli:=Pwd_Cli;
end;

procedure TCliente_Web.SetNombre_Grupo_Usuario(nombre_grupo:String);
begin
   xnombre_grupo_usuario:=nombre_grupo;
end;

procedure TLocalEnv_Web.SetCod_postal(cod_postal: string);
begin
   if Length(cod_postal)>9 then
    Raise Exception.create('El valor del Identificador de C�digo Postal no debe superar los 9 caracteres')
  else xcod_postal := cod_postal;
end;

procedure TLocalEnv_Web.SetDomicilio(domicilio: string);
begin
   if Length(domicilio)>250 then
    Raise Exception.create('El valor del Domicilio no debe superar los 250 caracteres')
  else xdomicilio := domicilio;
end;

procedure TLocalEnv_Web.SetEmpresa_id(empresa_id: string);
begin
   if Length(empresa_id)>4 then
    Raise Exception.create('El valor del Identificador de Empresa no debe superar los 4 caracteres')
  else  xempresa_id := empresa_id;
end;

procedure TLocalEnv_Web.SetLocal_Envio_id(local_envio_id: string);
begin
   if Length(local_envio_id)>4 then
    Raise Exception.create('El valor del Identificador de Localizaci�n Env�o no debe superar los 4 caracteres')
  else xlocal_id := local_envio_id;
end;

procedure TLocalEnv_Web.SetPais_id(pais_id: string);
begin
   if Length(pais_id)>4 then
    Raise Exception.create('El valor del Identificador de Pa�s no debe superar los 4 caracteres')
  else xpais_id := pais_id;
end;

procedure TLocalEnv_Web.SetPoblacion(poblacion: string);
begin
   if Length(poblacion)>40 then
    Raise Exception.create('El valor del Identificador de la Poblaci�n no debe superar los 40 caracteres')
  else xpoblacion := poblacion;
end;

procedure TLocalEnv_Web.SetProvincia_id(provincia_id: string);
begin
   if Length(provincia_id)>4 then
    Raise Exception.create('El valor del Identificador de Provincia no debe superar los 4 caracteres')
  else xprovincia_id := provincia_id;
end;

procedure TLocalEnv_Web.SetNombre_loc(nombre_loc: string);
begin
   if Length(nombre_loc)>40 then
    Raise Exception.create('El valor del Nombre de la Localizaci�n no debe superar los 40 caracteres')
 //else xnombre_loc := ansireplacestr(nombre_loc,'&','"&"');
   else xnombre_loc := nombre_loc;
end;

procedure TLocalEnv_Web.SetUsuario_id(usuario_id:string);
begin
   if Length(usuario_id)>10 then
    Raise Exception.create('El valor del Identificador de Usuario no debe superar los 10 caracteres')
  else
  xusuario_id := usuario_id;
end;

procedure  TLocalEnv_Web.setProvincia_desc(provincia_desc:string);
begin
    xprovincia_desc:=provincia_desc;
end;

procedure TLocalEnv_Web.setPais_desc(pais_desc:string);
begin
    xpais_desc:= pais_desc[1]+AnsiLowerCase(Copy(pais_desc,2,length(pais_desc)));
end;

procedure TGrupo_usuarios.SetEmpresa_id(empresa_id:string);
begin
  // a�adir al objeto los valores de los controles
  xempresa_id := empresa_id;
end;

procedure TGrupo_usuarios.SetGrupo_Usuario_id(grupo_usuario_id:string);
begin
   if Length(grupo_usuario_id)>4 then
    Raise Exception.create('El valor del Identificador de Grupo no debe superar los 4 caracteres')
  else  xgrupo_usuario_id := grupo_usuario_id;
end;

procedure TGrupo_usuarios.SetNombre(nombre:string);
begin
 /// restricci�n de magento comentada con Eduard Falla
 if Length(nombre)>31 then
    Raise Exception.create('El valor del Nombre de Grupo no debe superar los 32 caracteres')
  else xnombre := nombre;

end;

end.
