 {

}
unit base;

interface

uses forms,windows,sysutils, dialogs, ADODB, Db, classes,filectrl, shellapi;

function devuelve_fecha_larga(separador:char):string;
function Devuelve_odbc_path():string;
function Devuelve_path_sql(nombre_fichero:string):string;
function Devuelve_odbc_string(ruta:string;nombre_fichero_ini:string):string;
function Captura_linea_a_partir_del_signo_igual(cadena:string):string;
function Carga_Cbo_Repres():Tstringlist; overload;
function Carga_Cbo_Repres(adoconection: TADOConnection): Tstringlist; overload;
function Carga_Cbo_Empresas(var empresas:tstrings):boolean;
function carga_cbo_ficheros(unidad:char;directorio:string;mascara:string;formulario:tform):tstringlist;


function GetFileVersion(const FileName: TFileName;
         var Major, Minor, Release, Build: word): boolean;
function carga_consulta(nombre_consulta:string):widestring;
function carga_consulta_ruta(nombre_consulta:string):widestring;
function getloginname:string;
function carga_combo_ini(fichero_ini:string; seccion:string;clave:string ):tstrings;
function codifica_fecha:string;
function ejecutayespera(aplicacion:string; parametros:string):boolean;

implementation

uses bdalfa, general,  encripter;





function ejecutayespera(aplicacion:string; parametros:string):boolean;
var

   SEInfo: TShellExecuteInfo;
   ExitCode: DWORD;
   ExecuteFile: string;
begin
     ExecuteFile:=aplicacion;

     FillChar(SEInfo, SizeOf(SEInfo), 0) ;
     SEInfo.cbSize := SizeOf(TShellExecuteInfo) ;
     with SEInfo do begin
      fMask := SEE_MASK_NOCLOSEPROCESS;
      Wnd := application.Handle;
      lpFile := PChar(ExecuteFile) ;
     {
ParamString can contain the
application parameters.
}
      lpParameters := PChar(parametros) ;
{
StartInString specifies the
name of the working directory.
If ommited, the current directory is used.
}
// lpDirectory := PChar(StartInString) ;
     nShow := SW_HIDE;
   end;
   try
        if ShellExecuteEx(@SEInfo) then
        begin
             repeat
               application.ProcessMessages;
               GetExitCodeProcess(SEInfo.hProcess, ExitCode) ;
             until (ExitCode <> STILL_ACTIVE) or
             Application.Terminated;
             result:=true;
        end
   except
         result:=false;
   end
end;






function carga_combo_ini(fichero_ini:string; seccion:string;clave:string ):tstrings;
var  valores:ansistring;


     itemscombo:tstrings;
begin
     itemscombo:=tstringlist.Create;
     if lee_dato_ini(fichero_ini, seccion, clave, valores) then
     begin
          itemscombo.Delimiter:=';';
          itemscombo.DelimitedText:=valores;
          result:=itemscombo;
     end
     else
     result.add('N/A');
end;

function GetLoginName: string;
var
  buffer: array[0..255] of char;
  size: dword;
begin
  size := 256;
  if GetUserName(buffer, size) then
    Result := buffer
  else
    Result := ''
end;
function codifica_fecha:string;
var
    Year, Month, Day: Word;
    codigo:integer;
    codigo_str:string;

begin
     DecodeDate(Now, Year, Month, Day);
     codigo:=month*31+Day*7;
     if codigo<10 then codigo_str:='00'+inttostr(codigo) else
     if codigo<100 then codigo_str:='0'+inttostr(codigo) else
      codigo_str:=inttostr(codigo);
     result:=codigo_str;
end;




function carga_consulta_ruta(nombre_consulta:string):widestring;
var f:TextFile ;
    consulta:widestring;
    linea:string;
begin
     result:='NULL';
     consulta:='';
     nombre_consulta:=nombre_consulta;
     if fileexists(nombre_consulta) then
     begin
          assignfile(f,nombre_consulta);
          reset(f);
          while not eof(f) do
          begin
               readln(f,linea);
               consulta:=consulta+' '+linea;
          end;
          result:=consulta
     end;
     closefile(f);
end;

function carga_consulta(nombre_consulta:string):widestring;
var f:TextFile ;
    consulta:widestring;
    linea:string;
begin
   result:='NULL';
   consulta:='';
   nombre_consulta:='c:\archivos de programa\ccs\g733\sql\'+nombre_consulta;
   if fileexists(nombre_consulta) then
   begin
        assignfile(f,nombre_consulta);
        reset(f);
        while not eof(f) do
        begin
             readln(f,linea);
             consulta:=consulta+' '+linea;
        end;
        result:=consulta
   end;
   closefile(f);
end;
function GetFileVersion(const FileName: TFileName;
     var Major, Minor, Release, Build: word): boolean;
  // Returns True on success and False on failure.
  var
    size, len: longword;
//    handle: THandle;
    buffer: pchar;
    pinfo: ^VS_FIXEDFILEINFO;
  begin
    Result := False;
 //   size := GetFileVersionInfoSize(Pointer(FileName), handle);
    if size > 0 then begin
      GetMem(buffer, size);
      if GetFileVersionInfo(Pointer(FileName), 0, size, buffer)
      then
        if VerQueryValue(buffer, '\', pointer(pinfo), len) then begin
          Major   := HiWord(pinfo.dwFileVersionMS);
          Minor   := LoWord(pinfo.dwFileVersionMS);
          Release := HiWord(pinfo.dwFileVersionLS);
          Build   := LoWord(pinfo.dwFileVersionLS);
          Result  := True;
        end;
      FreeMem(buffer);
    end;
  end;

function carga_cbo_ficheros(unidad:char;directorio:string;mascara:string;formulario:tform):tstringlist;
var  a:integer;
     fichlist:tfilelistbox;
     resultado:tstringlist;
begin
     resultado:=tstringlist.Create;
     fichlist:=tfilelistbox.Create(nil);
     fichlist.Parent:=formulario;
     fichlist.Mask:=mascara;
     fichlist.Drive:=unidad;
     fichlist.Directory:=directorio;
     fichlist.Update;
     if fichlist.Items.Count>0 then
     for a:=0 to fichlist.items.count-1 do
         resultado.Add(fichlist.Items[a]);
     result:=resultado;
     fichlist.Free;

end;

function Carga_Cbo_Repres():Tstringlist;
const comilla=chr(39);
var g733_dataset: TADODataSet;
    repres:tstringlist;
begin
    g733_dataset:=TADODataSet.Create(g733_dataset);
    repres:=tstringlist.Create;
    repres.Clear;
    g733_dataset.active:=false;
    g733_dataset.ConnectionString:='';
    g733_dataset.ConnectionString:='Provider=SQLOLEDB.1;Password=ccsccs;Persist Security Info=True;User ID=imp;Initial Catalog=G733_DATA;Data Source=10.67.33.01';
    g733_dataset.CommandText:='select xrepresentante_id+'+quotedstr('_')+'+xnombre as xnombre from pl_representantes where xempresa_id='+comilla+'AL'+comilla+' and  xrepresentante_id in (select g733_representante from g733_repres_opc where g733_empresa='+comilla+'AL'+comilla+' and g733_activo='+comilla+'-1'+comilla+')';
    g733_dataset.active:=true;
    g733_dataset.First;
    while not g733_dataset.Eof do
     begin
        repres.Add(g733_dataset.fieldbyname('xnombre').Asstring);
        g733_dataset.Next;
     end;
     Carga_Cbo_Repres:=repres;
     g733_dataset.Free;
end;


function Carga_Cbo_Repres(adoconection: TADOConnection): Tstringlist;
const
  comilla = chr(39);
var
  g733_dataset: TADODataSet;
  repres: Tstringlist;
begin
  g733_dataset := TADODataSet.Create(nil);
  repres := Tstringlist.Create;
  repres.Clear;
  g733_dataset.active := false;
  g733_dataset.Connection := adoconection;
  g733_dataset.CommandText := 'select xrepresentante_id+' + quotedstr('_') +
    '+xnombre as xnombre from pl_representantes where xempresa_id=' + comilla +
    'AL' + comilla +
    ' and  xrepresentante_id in (select g733_representante from g733_repres_opc where g733_empresa='
    + comilla + 'AL' + comilla + ' and g733_activo=' + comilla + '-1' +
    comilla + ')';
  g733_dataset.active := true;
  g733_dataset.First;
  while not g733_dataset.Eof do
  begin
    repres.Add(g733_dataset.fieldbyname('xnombre').AsString);
    g733_dataset.Next;
  end;
  Carga_Cbo_Repres := repres;
  g733_dataset.Free;
end;


function Carga_Cbo_Empresas(var empresas:tstrings):boolean;
var f:textfile;
    linea:string;
begin
       Carga_Cbo_Empresas:=true;
       AssignFile(F,'c:\archivos de programa\ccs\g733\datos_comunes\empresas.txt');
       reset(F);
       try
          empresas.clear
       except
         Carga_Cbo_Empresas:=false;
       end;{try}
       while not eof(f) do
       begin
            readln(f,linea);
            empresas.Add(linea);
       end;
       closefile(f);
end;

function devuelve_fecha_larga(separador:char):string;
var
    Year, Month, Day: Word;
    dia,mes:string;

begin
      DecodeDate(Now, Year, Month, Day);
      if day<10 then dia:='0'+inttostr(day)
      else dia:=inttostr(day);
      if month<10 then mes:='0'+inttostr(month)
      else
      mes:=inttostr(month);
      devuelve_fecha_larga:=dia+separador+mes+separador+inttostr(year);
end;

function devuelve_odbc_path():string;
var f:textfile;
    ruta:string;
begin
     assign(f,'c:\g733.ini');
     reset(f);
     readln(f);
     readln(f,ruta);
     closefile(f);
     result:=ruta;
end;

function captura_linea_a_partir_del_signo_igual(cadena:string):string;
var a:integer;
begin
     a:=0;
     repeat
     a:=a+1
     until (cadena[a]='=') or (a=length(cadena));
     if cadena[a]='=' then
     result:=copy(cadena,a+1,length(cadena));
end;

function devuelve_path_sql(nombre_fichero:string):string;{debe devolver la 5� l�nea del fichero SQL}
var f:textfile;
    ruta:string;
begin
     assign(f,nombre_fichero);
     reset(f);
     readln(f,ruta);
     readln(f,ruta);
     readln(f,ruta);
     readln(f,ruta);
     readln(f,ruta);
     closefile(f);
     result:=ruta;
end;

function devuelve_odbc_string(ruta:string;nombre_fichero_ini:string):string;
var f:textfile;
    marca:string; {caracter de directorio " \ "}
    cadena:string;
//  odbc_string:string;
begin
     if ruta[length(ruta)]<>'\' then marca:='\' else marca:='';
     if fileexists(ruta+marca+nombre_fichero_ini) then
     begin
        assign(f,ruta+marca+nombre_fichero_ini);
        reset(f);
        readln(f,cadena);
        result:=captura_linea_a_partir_del_signo_igual(cadena);
        closefile(f);
     end
     else
     showmessage('Error (2): No se encuentra el fichero '+ruta+marca+nombre_fichero_ini);
end;


end.
