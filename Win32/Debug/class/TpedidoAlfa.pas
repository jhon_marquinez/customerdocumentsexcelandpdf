unit TpedidoAlfa;

interface
uses StdCtrls, adodb, db, Classes, SysUtils, regularexpressions, uitypes, StrUtils, comobj, windows ;


type TLocalizaciones =  class (Tobject)
  strict private
          xcod_postal:string;
          xdomicilio:string;
          xempresa_id:string;
          xlocal_id:String;
          xpais_id:string;
          xpoblacion:string;
          xprovincia_id:string;
          xusuario_id:String;
          xnombre_loc:string;
          xprovincia_desc:string;
          xpais_desc:string;
  public
          adoconnection_local_env: Tadoconnection;
      {    procedure SetCod_postal(cod_postal: string);
          procedure SetDomicilio(domicilio: string);
          procedure SetEmpresa_id(empresa_id:string);
          procedure SetLocal_Envio_id(local_envio_id: string);
          procedure SetPais_id(pais_id: string);
          procedure SetPoblacion(poblacion: string);
          procedure SetProvincia_id(provincia_id: string);
          procedure SetUsuario_id(usuario_id:string);
          procedure SetNombre_loc(nombre_loc: string);
          procedure setProvincia_desc(provincia_desc:String);
          procedure setPais_desc(pais_desc:string);

  public
          adoconnection_local_env: Tadoconnection;
          property CodigoPostal: string read xcod_Postal write SetCod_postal;
          property Domicilio: string read xdomicilio write SetDomicilio;
          property Empresa_id :string read xempresa_id write SetEmpresa_id;
          property Local_envio_id:string read xlocal_id write SetLocal_Envio_id;
          property Pais_id: string read xpais_id write SetPais_id;
          property Poblacion: string read xpoblacion write SetPoblacion;
          property Provincia_id: string read xprovincia_id write SetProvincia_id;
          property Usuario_id :string read xusuario_id  write SetUsuario_id;
          property Nombre_loc: string read xnombre_loc write SetNombre_loc;
          property Pais_desc:String read xpais_desc write setPais_desc;
          property Provincia_desc:string read xprovincia_desc write setProvincia_desc;    }

          constructor create(ConexionBD:Tadoconnection);
         { function Carga_Local_env(local_id, usuario_id:string):boolean;
          function Inserta_Local_env(usuario_id:string):boolean;
          function Actualiza_local_env(cod_postal, domicilio, local_id, pais_id, poblacion_id, provincia_id, usuario_id, nombre_loc:String):boolean;
          function getEmailLocalEnv(usuario, localizacion:String):String; }
end;


type TLocalizacionesCli =  class (Tobject)
      dataset:TAdodataset;
      locs:array of TLocalizaciones;
end;

type TCliente = class ( Tobject)
    strict private
          xactivo_web: boolean;
          xcliente_id: string;
          xemail: string;
          xempresa_id:String;
          xfpago_id:string;
          xfpago_desc:String;
          xlocal_id:String;
          xnombre:string;
          xtelefono:string;
          adoconnectionCliente:Tadoconnection;
        {  procedure SetCliente_id(cliente_id: string);
          procedure SetEmail(email: string);
          procedure SetEmpresa_id(empresa_id: string);
          procedure SetFPago_id(fpago_id: string);
          procedure SetFPago_desc(fpago_desc: string);
          procedure SetLocal_Envio_id(local_envio_id: string);
          procedure SetNombre(nombre: string);
          procedure SetTelefono(telefono: string);  }

    public
          xlocalizaciones :TlocalizacionesCli;
          { property Activo_web:boolean  read xactivo_web write SetActivo;
          property Cliente_id:string read xcliente_id write SetCliente_id;
          property Email:string read xemail write SetEmail;
          property Empresa:string read xempresa_id write SetEmpresa_id;
          property FPago:string read xfpago_id write SetFPago_id;
          property Local_envio_id:string read xlocal_id write SetLocal_Envio_id;
          property Nombre:string read xnombre write SetNombre;
          property Telefono:string read xtelefono write SetTelefono;
          property FPago_desc:String read xfpago_desc write SetFPago_desc;
          property Localizaciones:TlocalizacionesCli read xlocalizaciones;  }

          constructor create(ConexionBD:Tadoconnection);

end;

type Trepresentante =class(Tobject)
  strict private

      representanteId:String;
      empresaId:string;
      porcentajeComision:currency;
      telefono:string;
      nif:string;
      nombre:string;
      paisId:string;
      paisNombre:String;
      poblacion:string;
      provinciaId:string;
      provinciaNombre:string;
      codigoPostal:string;
      domicilio:string;
      email:string;

end;



type Tpedido = class(Tobject)
        ConexionBD:TAdoconnection;
        clienteId:TCliente;
        localEnv:TLocalizacionesCli;
        origenEntrada:string;
        representanteID:Trepresentante;
        importeTotal:currency;
        //fechaEntrada:


end;


implementation

uses  general, encripter, email, bdalfa, base;



constructor TCliente.create(ConexionBD:Tadoconnection);
begin
    if ConexionBD.Connected=false then
      Raise Exception.create('No existe conexi�n a la base de datos')
    else
    begin
      adoconnectionCliente:=ConexionBD;
      xempresa_id:='al';
    end;
end;

constructor TLocalizaciones.create( ConexionBD:Tadoconnection);
begin
    if ConexionBD.Connected=false then
      Raise Exception.create('No existe conexi�n a la base de datos')
    else
    begin
        adoconnection_local_env:=ConexionBD;
        xempresa_id:='al';
    end;
end;





end.
